package in.billiontags.bluemoney;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import in.billiontags.bluemoney.adapter.BusStationAdapter;
import in.billiontags.bluemoney.adapter.BusTypeAdapter;
import in.billiontags.bluemoney.adapter.TicketAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.callback.BusTypeCallback;
import in.billiontags.bluemoney.callback.TicketCallback;
import in.billiontags.bluemoney.model.BusStations;
import in.billiontags.bluemoney.model.BusType;
import in.billiontags.bluemoney.model.Ticket;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launchWithBundle;

public class BusSearchActivity extends AppCompatActivity implements TicketCallback, BusTypeCallback, View.OnClickListener {

    private static final String KEY_TRAVELDATE = "traveldate";
    private static final String KEY_SOURCEPLACE = "source";
    private static final String KEY_DESTINATIONPLACE = "destination";
    private static final String KEY_SOURCENAME = "sourcename";
    private static final String KEY_DESTINATIONNAME = "destinationname";
    private static final String KEY_NOOFTICKETS = "nooftickets";
    AutoCompleteTextView mTextViewInputFrom, mTextViewInputTo;
    int count = 1;
    ArrayList<BusStations> mBusStationslist;
    BusStationAdapter adapter;
    int i = 0;
    int mTotalTickets;
    int j = 0;
    private Context mContext;
    private RecyclerView mViewTicket, mViewBusType;
    private List<Ticket> mTicketList;
    private List<BusType> mBusTypeList;
    private TicketAdapter mTicketAdapter;
    private BusTypeAdapter mBusTypeAdapter;
    private CardView mCardOnWardDate, mCardReturnDate;
    private TextView mTextViewOnWardsDate, mTextViewReturnDate;
    private DateFormat dateFormatter;
    private ImageView mImageBusTypeRight, mImageBusTypeLeft, mImageBack, mImageTicketRight, mImageTicketLeft;
    private Calendar newCalendar;
    private Button mButtonProceed;
    private String mSourcePlace, mDestinationPlace;
    private int mNoOfTickets = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_search);
        initObjects();
        initCallbacks();
        initRecyclerView();
        populateData();
        mgetStations();
        mSetAutoComplete();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mCardOnWardDate.setOnClickListener(this);
        mCardReturnDate.setOnClickListener(this);
        mImageTicketRight.setOnClickListener(this);
        mImageTicketLeft.setOnClickListener(this);
        mImageBusTypeLeft.setOnClickListener(this);
        mImageBusTypeRight.setOnClickListener(this);
        mButtonProceed.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
    }

    @Override
    public void onTicketSelect(int position) {
        Ticket ticket = mTicketList.get(position);
        mTotalTickets = position + 1;
        if (i == 0) {
            ticket.setSelected(true);
            mTicketAdapter.notifyDataSetChanged();
            mNoOfTickets = ticket.getNo();
            i++;
        } else {
            ticket.setSelected(false);
            mTicketAdapter.notifyDataSetChanged();
            i--;
        }

    }

    @Override
    public void onBusTypeClick(int position) {
        BusType busType = mBusTypeList.get(position);
        mTotalTickets = position;
        if (j == 0) {
            busType.setSelected(true);
            mBusTypeAdapter.notifyDataSetChanged();
            Log.e("position", "" + busType);
            j++;
        } else {
            busType.setSelected(false);
            mBusTypeAdapter.notifyDataSetChanged();
            j--;
        }


    }

    private void initObjects() {
        mViewTicket = findViewById(R.id.ticket);
        mViewBusType = findViewById(R.id.bus_type);
        mTextViewInputFrom = findViewById(R.id.input_from);
        mTextViewInputTo = findViewById(R.id.input_to);
        mCardOnWardDate = findViewById(R.id.onward);
        mCardReturnDate = findViewById(R.id.card_return);
        mTextViewOnWardsDate = findViewById(R.id.txt_onwardate);
        mTextViewReturnDate = findViewById(R.id.txt_returndate);
        mImageTicketRight = findViewById(R.id.img_ticket_right);
        mImageTicketLeft = findViewById(R.id.img_ticket_left);
        mImageBusTypeRight = findViewById(R.id.img_type_right);
        mImageBusTypeLeft = findViewById(R.id.img_type_left);
        mImageBack = findViewById(R.id.img_back);
        mButtonProceed = findViewById(R.id.btn_proceed);
        mContext = this;
        mTicketList = new ArrayList<>();
        mBusTypeList = new ArrayList<>();
        mBusStationslist = new ArrayList<>();
        mTicketAdapter = new TicketAdapter(mContext, mTicketList, this);
        mBusTypeAdapter = new BusTypeAdapter(mContext, mBusTypeList, this);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        newCalendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(newCalendar.getTime());
        mTextViewOnWardsDate.setText(formattedDate);

    }

    @Override
    public void onClick(View v) {
        if (v == mCardOnWardDate) {
            mFromdatePicker();
        } else if (v == mCardReturnDate) {
            mtoDatePicker();
        } else if (v == mImageTicketRight) {
            mImageRightclick();
        } else if (v == mImageTicketLeft) {
            mImageleftClick();
        } else if (v == mImageBusTypeRight) {
            populateData();

        } else if (v == mImageBack) {
            onBackPressed();

        } else if (v == mImageBusTypeLeft) {
            populateData();

        } else if (v == mButtonProceed) {
            processData();
        }
    }

    private void mImageRightclick() {
        if (mTotalTickets < 6) {
            if (mTotalTickets == 0) {
                Ticket ticket = mTicketList.get(0);
                ticket.setSelected(true);
                mNoOfTickets = ticket.getNo();
                mTotalTickets++;
            } else if (mTotalTickets == 1) {
                Ticket ticket = mTicketList.get(0);
                Ticket ticket1 = mTicketList.get(1);
                ticket1.setSelected(true);
                mNoOfTickets = ticket1.getNo();
                ticket.setSelected(false);
                mTotalTickets++;
            } else if (mTotalTickets == 2) {
                Ticket ticket = mTicketList.get(0);
                Ticket ticket1 = mTicketList.get(1);
                Ticket ticket2 = mTicketList.get(2);
                ticket2.setSelected(true);
                mNoOfTickets = ticket2.getNo();
                ticket1.setSelected(false);
                ticket.setSelected(false);
                mTotalTickets++;
            } else if (mTotalTickets == 3) {
                Ticket ticket = mTicketList.get(0);
                Ticket ticket1 = mTicketList.get(1);
                Ticket ticket2 = mTicketList.get(2);
                Ticket ticket3 = mTicketList.get(3);
                ticket3.setSelected(true);
                mNoOfTickets = ticket3.getNo();
                ticket2.setSelected(false);
                ticket1.setSelected(false);
                ticket.setSelected(false);
                mTotalTickets++;
            } else if (mTotalTickets == 4) {
                Ticket ticket = mTicketList.get(0);
                Ticket ticket1 = mTicketList.get(1);
                Ticket ticket2 = mTicketList.get(2);
                Ticket ticket3 = mTicketList.get(3);
                Ticket ticket4 = mTicketList.get(4);
                mNoOfTickets = ticket4.getNo();
                ticket4.setSelected(true);
                ticket3.setSelected(false);
                ticket2.setSelected(false);
                ticket1.setSelected(false);
                ticket.setSelected(false);
                mTotalTickets++;
            } else if (mTotalTickets == 5) {
                Ticket ticket = mTicketList.get(0);
                Ticket ticket1 = mTicketList.get(1);
                Ticket ticket2 = mTicketList.get(2);
                Ticket ticket3 = mTicketList.get(3);
                Ticket ticket4 = mTicketList.get(4);
                Ticket ticket5 = mTicketList.get(5);
                mNoOfTickets = ticket5.getNo();
                ticket5.setSelected(true);
                ticket4.setSelected(false);
                ticket3.setSelected(false);
                ticket2.setSelected(false);
                ticket1.setSelected(false);
                ticket.setSelected(false);
                mTotalTickets++;
            }

        } else {
            ToastBuilder.build(mContext, "Max 6 allowed");

        }
        mTicketAdapter.notifyDataSetChanged();
    }


    private void mImageleftClick() {
        if (mTotalTickets > 0 && mTotalTickets <= 6) {
            mTotalTickets--;
            Ticket ticket = mTicketList.get(mTotalTickets);
            ticket.setSelected(false);
            if (mTotalTickets == 0) {
                ticket.setSelected(true);
            }
            mTicketAdapter.notifyDataSetChanged();
        } else {
            ToastBuilder.build(mContext, "Minimum select 1 seat");

        }
    }
    private void processData() {

        if (mNoOfTickets == 0) {
            Ticket ticket = mTicketList.get(0);
            mNoOfTickets = ticket.getNo();
        }
        Date date = new Date(mTextViewOnWardsDate.getText().toString());
        String Traveldate = dateFormatter.format(date);
        String source = mTextViewInputFrom.getText().toString();
        String destination = mTextViewInputTo.getText().toString();
        int Tickets = mNoOfTickets;
        if (validateInput(mSourcePlace, mDestinationPlace)) {
            Bundle bundle = new Bundle();
            bundle.putString(KEY_TRAVELDATE, Traveldate);
            bundle.putString(KEY_SOURCEPLACE, mSourcePlace);
            bundle.putString(KEY_DESTINATIONPLACE, mDestinationPlace);
            bundle.putString(KEY_DESTINATIONNAME, destination);
            bundle.putString(KEY_SOURCENAME, source);
            bundle.putInt(KEY_NOOFTICKETS, Tickets);
            launchWithBundle(mContext, BusSearchDetailActivity.class, bundle);
        }
    }

    private boolean validateInput(String source, String destination) {
        if (TextUtils.isEmpty(source)) {
            mTextViewInputFrom.requestFocus();
            mTextViewInputFrom.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "Boarding Location"));
            return false;
        } else if (TextUtils.isEmpty(destination)) {
            mTextViewInputTo.requestFocus();
            mTextViewInputTo.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Dropping Location"));
            return false;
        }
        return true;
    }

    private void mFromdatePicker() {
        android.app.DatePickerDialog fromDatePickerDialog = new android.app.DatePickerDialog(BusSearchActivity.this, new android.app.DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                mTextViewOnWardsDate.setText(df.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        fromDatePickerDialog.getDatePicker().setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        fromDatePickerDialog.show();

    }

    private void mtoDatePicker() {
        android.app.DatePickerDialog ReturnDatePickerDialog = new android.app.DatePickerDialog(BusSearchActivity.this,new android.app.DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                mTextViewReturnDate.setText(df.format(newDate.getTime()));
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        ReturnDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        ReturnDatePickerDialog.getDatePicker().setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        ReturnDatePickerDialog.show();

    }
    private void initRecyclerView() {
        mViewTicket.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mViewTicket.setHasFixedSize(true);
        mViewTicket.setAdapter(mTicketAdapter);

        mViewBusType.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mViewBusType.setHasFixedSize(true);
        mViewBusType.setAdapter(mBusTypeAdapter);
    }

    private void populateData() {
        for (int i = 1; i <= 6; i++) {
            mTicketList.add(new Ticket(i, i == 1));
        }
        mTicketAdapter.notifyDataSetChanged();
        mBusTypeList.add(new BusType("AC", false));
        mBusTypeList.add(new BusType("Non AC", false));
        mBusTypeList.add(new BusType("Sleeper", false));
        mBusTypeList.add(new BusType("Semi Sleeper", false));
        mBusTypeAdapter.notifyDataSetChanged();
    }

    private void mSetAutoComplete() {
        mTextViewInputFrom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSourcePlace = mBusStationslist.get(position).getmStationCode();


            }
        });
        mTextViewInputTo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDestinationPlace = mBusStationslist.get(position).getmStationCode();
            }
        });

    }

    private void mgetStations() {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<ArrayList<BusStations>> call = authService.getStations();
        call.enqueue(new Callback<ArrayList<BusStations>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<BusStations>> call, @NonNull Response<ArrayList<BusStations>> response) {
                ArrayList<BusStations> mbusStationList = response.body();
                if (response.isSuccessful() && mbusStationList != null) {
                    mBusStationslist.addAll(mbusStationList);
                    adapter = new BusStationAdapter(mContext, R.layout.item_busstations, R.id.txt_stations, mBusStationslist);
                    mTextViewInputFrom.setThreshold(1);
                    mTextViewInputFrom.showDropDown();
                    mTextViewInputFrom.setAdapter(adapter);
                    mTextViewInputTo.setThreshold(1);
                    mTextViewInputTo.showDropDown();
                    mTextViewInputTo.setAdapter(adapter);
                    mTextViewInputFrom.setTextColor(Color.BLACK);
                    mTextViewInputTo.setTextColor(Color.BLACK);

                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<BusStations>> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }


}
