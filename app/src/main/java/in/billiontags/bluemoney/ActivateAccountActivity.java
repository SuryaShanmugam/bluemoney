package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.AuthService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.ActivateAccount;
import in.billiontags.bluemoney.model.User;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launchClearStack;
import static in.billiontags.bluemoney.utils.Utils.getMacAddress;

public class ActivateAccountActivity extends AppCompatActivity implements View.OnClickListener {

    public Context mContext;
    protected View mRootView;
    private EditText medit_otp;

    private Button verifyotp;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_account);

        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        medit_otp = findViewById(R.id.edit_otp);
        verifyotp = findViewById(R.id.verifyotp_button);
        mImageBack = findViewById(R.id.img_back);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);

    }

    @Override
    public void onClick(View view) {
        if (view == verifyotp) {
            processOtp();
        } else if (view == mImageBack) {
            onBackPressed();
        }

    }

    private void initCallbacks() {
        verifyotp.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
    }


    private void processOtp() {
        String otp = medit_otp.getText().toString().trim();
        if (validateInput(otp)) {
            showProgressDialog("Verify Otp..");
            verifyOtp(new ActivateAccount(mPreference.getPhone(), otp, getMacAddress(), "3"));
        }
    }

    private boolean validateInput(String otp) {
        if (otp.isEmpty()) {
            medit_otp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1), "OTP"));
            medit_otp.requestFocus();
            return false;
        } else if (otp.length() < 6) {
            medit_otp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "OTP", 6, "digits"));
            medit_otp.requestFocus();
            return false;
        }
        return true;
    }

    private void verifyOtp(ActivateAccount activateAccount) {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<User> call = authService.activateAccount(activateAccount);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    hideProgressDialog();

                    mPreference.setPhone(userResponse.getmUsername());
                    mPreference.setEmail(userResponse.getmEmail());
                    mPreference.setToken(userResponse.getmToken());
                    mPreference.setUserId(String.valueOf(userResponse.getmId()));
                    launchClearStack(mContext, HomeActivity.class);
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
