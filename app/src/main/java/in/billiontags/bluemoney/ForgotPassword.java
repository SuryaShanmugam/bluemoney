package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.AuthService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.ForgotPass;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launch;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {
    private Context mContext;
    private ImageView mImageViewBack;
    private EditText mEditTextPhone;
    private Button mButtonResetPass;
    private TextView login_redirect;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__password);

        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mImageViewBack = findViewById(R.id.img_back);
        mEditTextPhone = findViewById(R.id.edit_phone);
        mButtonResetPass = findViewById(R.id.btn_reset_pass);
        login_redirect = findViewById(R.id.login_redirect);
        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mImageViewBack.setOnClickListener(this);
        mButtonResetPass.setOnClickListener(this);
        login_redirect.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mImageViewBack) {
            onBackPressed();
        } else if (view == mButtonResetPass) {
            processForgotPass();
        } else if (view == login_redirect) {
            finish();
        }
    }


    private void processForgotPass() {
        String phone = mEditTextPhone.getText().toString().trim();
        if (validateInput(phone)) {
            showProgressDialog("Verifying MobileNumber..");
            getforgotpass(phone);
        }

    }

    private boolean validateInput(String phone) {
        if (phone.isEmpty()) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Mobile Number"));
            return false;
        }
        return true;
    }

    private void getforgotpass(String mMobileNumber) {
        Log.e("mMobileNumber", "" + mMobileNumber);
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<ForgotPass> call = authService.forgotPass(new ForgotPass(mMobileNumber, "3"));
        call.enqueue(new Callback<ForgotPass>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPass> call, @NonNull Response<ForgotPass> response) {
                ForgotPass forgotPass = response.body();
                hideProgressDialog();
                if (response.isSuccessful() && forgotPass != null) {
                    ToastBuilder.build(mContext, "OTP Sent Successfully ");

                    mPreference.setUserId(forgotPass.getmId());
                    mPreference.setPhone(forgotPass.getmUserName());
                    mPreference.setEmail(forgotPass.getmEmail());
                    Log.e("hhjjhj", "" + forgotPass.getmUserName());
                    launch(mContext, ResetPassActivity.class);

                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());

                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPass> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }
}



