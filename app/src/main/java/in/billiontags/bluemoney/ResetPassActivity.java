package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.AuthService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.ForgotPass;
import in.billiontags.bluemoney.model.ResendPojo;
import in.billiontags.bluemoney.model.ResetPass;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launchClearStack;
import static in.billiontags.bluemoney.utils.Utils.getMacAddress;

public class ResetPassActivity extends AppCompatActivity implements View.OnClickListener {


    public Context mContext;
    protected View mRootView;
    String mUrl;
    private EditText mEditTextOtp, mEditTextNewPass, mEditTextMobile;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private TextView mTextViewResendOtp;
    private Button mResetPass;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);

        initObjects();
        initCallbacks();
    }


    private void initObjects() {
        mContext = this;
        mEditTextOtp = findViewById(R.id.edit_otp);
        mEditTextNewPass = findViewById(R.id.edit_pass);
        mEditTextMobile = findViewById(R.id.edit_phone);
        mTextViewResendOtp = findViewById(R.id.resend_otp);
        mResetPass = findViewById(R.id.btn_reset_pass);
        mImageBack = findViewById(R.id.img_back);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);

    }

    private void initCallbacks() {
        mResetPass.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mTextViewResendOtp.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        if (view == mResetPass) {
            processSetPass();
        } else if (view == mTextViewResendOtp) {
            setUrl();
        } else if (view == mImageBack) {
            onBackPressed();
        }
    }

    private void processSetPass() {
        String otp = mEditTextOtp.getText().toString().trim();
        String pass = mEditTextNewPass.getText().toString().trim();
        String mobile = mEditTextMobile.getText().toString().trim();
        if (validateInput(otp, mobile, pass)) {
            showProgressDialog("Processing...");
            resetPass(new ResetPass(mobile, pass, otp, getMacAddress(), "3"));
        }
    }

    private boolean validateInput(String otp, String mobile, String pass) {
        if (otp.isEmpty()) {
            mEditTextOtp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "OTP"));
            mEditTextOtp.requestFocus();
            return false;
        } else if (otp.length() < 6) {
            mEditTextOtp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "OTP", 6, "digits"));
            mEditTextOtp.requestFocus();
            return false;
        } else if (mobile.isEmpty()) {
            mEditTextMobile.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "Mobile Number"));
            mEditTextMobile.requestFocus();
        } else if (pass.isEmpty()) {
            mEditTextOtp.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "Password"));
            mEditTextNewPass.requestFocus();
        } else if (pass.length() < 6) {
            mEditTextOtp.setError(getString(R.string.error_pass_length));
            mEditTextNewPass.requestFocus();
            return false;
        }
        return true;
    }

    private void resetPass(final ResetPass resetPass) {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<ResetPass> call = authService.resetPass(resetPass);
        call.enqueue(new Callback<ResetPass>() {
            @Override
            public void onResponse(@NonNull Call<ResetPass> call, @NonNull Response<ResetPass> response) {
                ResetPass userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    hideProgressDialog();
                    ToastBuilder.build(mContext, "Reset Password Successfully");
                    launchClearStack(mContext, Login.class);
                } else {
                    hideProgressDialog();

                    //     ToastBuilder.build(mContext, "Invalid OTP for this user.");
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());

                }
            }

            @Override
            public void onFailure(@NonNull Call<ResetPass> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    private void setUrl() {
        mUrl = ApiClient.BASE_URL + "oauth/change-phone/" + mPreference.getUserId() + "/";

        Log.e("djkhfdf", "" + mUrl);
        showProgressDialog("Sending Otp....");
        resendOtp();
    }

    private void resendOtp() {
        AuthService authService = ApiClient.getClient().create(AuthService.class);

        Call<ForgotPass> call = authService.mResendOtp(mUrl, new ResendPojo(mPreference.getPhone()));
        call.enqueue(new Callback<ForgotPass>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPass> call, @NonNull Response<ForgotPass> response) {
                Log.e("djkhfdf", "" + response.toString());
                hideProgressDialog();
                if (response.isSuccessful()) {
                    ToastBuilder.build(mContext, "OTP sent Successfully");
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPass> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
