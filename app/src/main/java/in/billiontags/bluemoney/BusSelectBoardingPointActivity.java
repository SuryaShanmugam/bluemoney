package in.billiontags.bluemoney;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import in.billiontags.bluemoney.adapter.BusBoardingLocationAdapter;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.callback.BusBoardingLocationCallback;
import in.billiontags.bluemoney.model.BusBoardingPointLocation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.app.Constant.KEY_ARRAY_BOARDINGPPOINT;
import static in.billiontags.bluemoney.app.Constant.KEY_BUSTYPE;
import static in.billiontags.bluemoney.app.Constant.KEY_OPERATORCODE;
import static in.billiontags.bluemoney.app.Constant.KEY_SCHEDULECODE;
import static in.billiontags.bluemoney.app.Constant.KEY_SEATFARE;
import static in.billiontags.bluemoney.app.Constant.KEY_SEATNO;
import static in.billiontags.bluemoney.app.Constant.KEY_SOURCENAME;
import static in.billiontags.bluemoney.app.Constant.KEY_TOTALFARE;
import static in.billiontags.bluemoney.app.Constant.KEY_TRAVELDATE;
import static in.billiontags.bluemoney.app.Constant.KEY_TRAVELSNAME;

public class BusSelectBoardingPointActivity extends AppCompatActivity implements BusBoardingLocationCallback, View.OnClickListener {
    private static final String KEY_ARRAY_DROPPINGPPOINT = "droppingpoints";
    private static final String KEY_SELECTEDBOARDINGLOCATION = "selectedlocation";
    private static final String KEY_SELECTEDBOARDINGCODE = "selectedcode";
    private static final String KEY_DESTINATIONNAME = "destinationname";
    public ArrayList<BusBoardingPointLocation> mMyordersItemList;
    ArrayList<String> mSeatNo = new ArrayList<>();
    ArrayList<String> mTotalSeatFare = new ArrayList<>();
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<BusBoardingPointLocation> mBoardinpointLocation;
    private ArrayList<BusBoardingPointLocation> mDroppingPointLocation;
    private BusBoardingLocationAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String mSourceName, mDestinationName, mSechedulecode, mOperatorcode,
            mTravelername, mBusType, mTravelDate, mTotalFare;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_select_boarding_point);
        processBundle();
        initObjects();
        initCallbacks();
        initRecyclerView();

    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void processBundle() {
        Intent i = getIntent();
        mMyordersItemList = new ArrayList<>();
        mBoardinpointLocation = new ArrayList<>();
        mDroppingPointLocation = new ArrayList<>();
        mMyordersItemList = i.getParcelableArrayListExtra(KEY_ARRAY_BOARDINGPPOINT);
        mDroppingPointLocation = i.getParcelableArrayListExtra(KEY_ARRAY_DROPPINGPPOINT);
        mSourceName = i.getStringExtra(KEY_SOURCENAME);
        mDestinationName = i.getStringExtra(KEY_DESTINATIONNAME);
        mSechedulecode = i.getStringExtra(KEY_SCHEDULECODE);
        mOperatorcode = i.getStringExtra(KEY_OPERATORCODE);
        mTravelername = i.getStringExtra(Constant.KEY_TRAVELSNAME);
        mSeatNo = i.getStringArrayListExtra(Constant.KEY_SEATNO);
        mTotalSeatFare = i.getStringArrayListExtra(KEY_SEATFARE);
        mBusType = i.getStringExtra(Constant.KEY_BUSTYPE);
        mTravelDate = i.getStringExtra(Constant.KEY_TRAVELDATE);
        mTotalFare = i.getStringExtra(Constant.KEY_TOTALFARE);

        if (mMyordersItemList != null) {
            mBoardinpointLocation.addAll(mMyordersItemList);
        } else {
            mBoardinpointLocation.add(new BusBoardingPointLocation(mSourceName));
        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initObjects() {
        mContext = this;
        mRecyclerView = findViewById(R.id.busseatboardingpoints);
        mImageBack = findViewById(R.id.img_back);

        mAdapter = new BusBoardingLocationAdapter(mContext, mBoardinpointLocation, this);
        mLayoutManager = new LinearLayoutManager(mContext);
    }

    @Override
    public void mOnItemClick(int pos) {
        BusBoardingPointLocation busBoardingPointLocation = mBoardinpointLocation.get(pos);
        Intent i = new Intent(BusSelectBoardingPointActivity.this, BusDroppingPointLocationActivity.class);
        i.putParcelableArrayListExtra(KEY_ARRAY_DROPPINGPPOINT, mDroppingPointLocation);
        i.putExtra(KEY_SELECTEDBOARDINGLOCATION, busBoardingPointLocation.getmName());
        i.putExtra(KEY_SELECTEDBOARDINGCODE, busBoardingPointLocation.getmCode());
        i.putExtra(KEY_OPERATORCODE, mOperatorcode);
        i.putExtra(KEY_SCHEDULECODE, mSechedulecode);
        i.putExtra(KEY_SOURCENAME, mSourceName);
        i.putExtra(KEY_TRAVELSNAME, mTravelername);
        i.putExtra(KEY_DESTINATIONNAME, mDestinationName);
        i.putExtra(KEY_BUSTYPE, mBusType);
        i.putExtra(KEY_TRAVELDATE, mTravelDate);
        i.putExtra(KEY_TOTALFARE, mTotalFare);
        i.putStringArrayListExtra(KEY_SEATNO, mSeatNo);
        i.putStringArrayListExtra(KEY_SEATFARE, mTotalSeatFare);
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }
    }
}
