package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.AuthService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.LoginPojo;
import in.billiontags.bluemoney.model.User;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launch;
import static in.billiontags.bluemoney.utils.Activity.launchClearStack;
import static in.billiontags.bluemoney.utils.Utils.getMacAddress;

public class Login extends AppCompatActivity implements View.OnClickListener {

    TextView msignupRedirect, forgot_password;
    String phone;
    String pass;
    String device_type;
    Button signIn;
    private Context mContext;
    private EditText mEditTextPhone, mEditTextPass;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();
        initCallbacks();

    }

    private void initCallbacks() {
        msignupRedirect.setOnClickListener(this);
        forgot_password.setOnClickListener(this);
        signIn.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void initObjects() {
        msignupRedirect = findViewById(R.id.account);
        forgot_password = findViewById(R.id.forgot_password);
        mEditTextPhone = findViewById(R.id.edit_phone);
        mEditTextPass = findViewById(R.id.edit_pass);
        signIn = findViewById(R.id.signIn);
        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);


    }

    @Override
    public void onClick(View v) {

        if (v == msignupRedirect) {
            launch(mContext, SignUp.class);
        } else if (v == forgot_password) {
            launch(mContext, ForgotPassword.class);
        } else if (v == signIn) {
            processSignIn();
        }

    }

    private void processSignIn() {
        phone = mEditTextPhone.getText().toString().trim();
        pass = mEditTextPass.getText().toString().trim();
        device_type = "ANDROID";
        if (validateInput(phone, pass)) {
            mPreference.setPhone(phone);
            showProgressDialog("Signing in..");
            getlogin(new LoginPojo(phone, pass, getMacAddress(), "3"));
        }
    }

    private boolean validateInput(String phone, String pass) {
        if (TextUtils.isEmpty(phone)) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "Mobile Number or Email Id"));
            return false;
        } else if (TextUtils.isEmpty(pass)) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "Password"));
            return false;
        } else if (pass.length() < 6) {
            mEditTextPass.requestFocus();
            mEditTextPass.setError(getString(R.string.error_pass_length));
            return false;
        }
        return true;
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void getlogin(LoginPojo loginPojo) {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<User> call = authService.loginUser(loginPojo);

        call.enqueue(new retrofit2.Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                User user = response.body();
                if (response.isSuccessful() && user != null) {
                    hideProgressDialog();
                    mPreference.setPhone(user.getmUsername());
                    mPreference.setEmail(user.getmEmail());
                    mPreference.setToken(user.getmToken());
                    mPreference.setProfilePic(user.getMuserprofile().getmProfilepic());
                    mPreference.setUserId(String.valueOf(user.getmId()));
                    launchClearStack(mContext, HomeActivity.class);

                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());

                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });


    }

    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();
    }


}
