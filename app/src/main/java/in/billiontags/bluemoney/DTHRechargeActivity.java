package in.billiontags.bluemoney;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.adapter.MyPagerAdapter;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.fragments.DthOffersFragment;
import in.billiontags.bluemoney.fragments.DthRechargeFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.WalletAmount.getWalletAmount;

public class DTHRechargeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, Runnable, View.OnClickListener {


    public int mId;
    public ImageView img_back;
    public TextView mWalletAmount;
    public Context mContext;
    MyPreference myPreference;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;
    private int[] tabIcons = {
            R.drawable.ic_satellite_dish,
            R.drawable.ic_dth_offers_new
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dth__recharge);
        initObjects();
        initCallbacks();
        initTabs();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void run() {

    }

    private void initObjects() {
        mContext = this;
        mTabLayout = findViewById(R.id.tab);
        mViewPager = findViewById(R.id.pager_profile);
        mWalletAmount = findViewById(R.id.amount_bar);
        img_back = findViewById(R.id.img_back);

        myPreference = new MyPreference(mContext);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);

        getWalletAmount(mContext, mWalletAmount);


    }

    private void initCallbacks() {
        img_back.setOnClickListener(this);
        mTabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == img_back) {
            onBackPressed();
        }

    }


    @SuppressWarnings("ConstantConditions")
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
        int posotion = tab.getPosition();
        if (posotion == 0) {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_satellite_dish);
            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));
        } else {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_dth_offers);

            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

        int posotion = tab.getPosition();
        if (posotion == 0) {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_satellite_dish_new);

        } else {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_dth_offers);

        }
    }

    @SuppressWarnings("ConstantConditions")

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        int posotion = tab.getPosition();
        if (posotion == 0)

        {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_satellite_dish);
        } else {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_dth_offers);
        }
    }


    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("DTH Recharge"));
        mFragmentList.add(new DthRechargeFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("DTH Offers"));
        mFragmentList.add(DthOffersFragment.newInstance(mId));
        mPagerAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        setupTabIcons();

    }

    @SuppressWarnings("ConstantConditions")
    private void setupTabIcons() {
        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }


}
