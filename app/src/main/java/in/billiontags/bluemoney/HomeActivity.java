package in.billiontags.bluemoney;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.AuthService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.FragmentCallback;
import in.billiontags.bluemoney.fragments.FragmentHome;
import in.billiontags.bluemoney.fragments.MyOrdersFragment;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launch;
import static in.billiontags.bluemoney.utils.Activity.launchClearStack;
import static in.billiontags.bluemoney.utils.WalletAmount.getWalletAmount;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentCallback {


    public int mId;
    TextView mWalletAmount, Title_name;
    MyPreference myPreference;
    Context mContext;
    LinearLayout home_layout;
    LinearLayout Myorders_list_layout;
    LinearLayout Logout_layout;
    AlertDialog dailog;
    AlertDialog.Builder build;
    private ImageView mImageProfile;

    private ImageView mImageAddMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;
        build = new AlertDialog.Builder(mContext);
        myPreference = new MyPreference(mContext);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        drawer.setScrimColor(Color.TRANSPARENT);

        NavigationView navigationView = findViewById(R.id.nav_view);
        final View hView = navigationView.inflateHeaderView(R.layout.home_nav_header);
        navigationView.setNavigationItemSelectedListener(this);
        home_layout = hView.findViewById(R.id.home_layout);
        Myorders_list_layout = hView.findViewById(R.id.Myorders_list_layout);
        Logout_layout = hView.findViewById(R.id.Logout_layout);
        mWalletAmount = findViewById(R.id.amount_bar);
        Title_name = findViewById(R.id.Title_name);
        mImageAddMoney = findViewById(R.id.Add_Wallet);
        mImageProfile = findViewById(R.id.profilePic);

        getWalletAmount(mContext, mWalletAmount);

        if (myPreference.getProfilePic() != null) {
            Glide.with(mContext).load(myPreference.getProfilePic())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_man).error(R.drawable.ic_man))
                    .into(mImageProfile);
        }

        Log.e("token", "" + myPreference.getToken());

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_home, new FragmentHome())
                .commit();

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                        INPUT_METHOD_SERVICE);
                if (imm != null) imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        });

        home_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_home, new FragmentHome())
                        .commit();
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

            }
        });

        mImageAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launch(mContext, AddWalletMoneyActivity.class);
            }
        });

        Myorders_list_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.content_home, new MyOrdersFragment())
                        .commit();
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        Logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Logout_dialog();
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void Logout_dialog() {
        build.setMessage("Are you sure want to logout now?");
        build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });
        build.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // TODO Auto-generated method stub
                dailog.dismiss();
            }
        });
        dailog = build.create();
        dailog.show();

    }

    private void logout() {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<Void> call = authService.logoutUser("Token " + myPreference.getToken());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    myPreference.clearUser();
                    launchClearStack(mContext, WelcomeActivity.class);
                    finish();

                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());

                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myPreference.getProfilePic() != null) {
            Glide.with(mContext).load(myPreference.getProfilePic())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_man).error(R.drawable.ic_man))
                    .into(mImageProfile);
        }

    }


    @Override
    public void launchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_home, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void headerSelected(boolean select) {

    }

    @Override
    public void setHeader(boolean sign, String value) {

    }
}
