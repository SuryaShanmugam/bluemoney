package in.billiontags.bluemoney.presenter;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.model.ServiceOperatorList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LandlineoperatorApi implements Operatorpresenter {

    private Operatorrequestview yTriprequestview;


    private LandlineoperatorApi(Operatorrequestview yyTriprequestview) {
        this.yTriprequestview = yyTriprequestview;
    }

    public static LandlineoperatorApi newInstance(Operatorrequestview yyTriprequestview) {
        return new LandlineoperatorApi(yyTriprequestview);
    }

    @Override
    public void Getting_trailer_list() {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<ArrayList<ServiceOperatorList>> call = authService.getOperators();
        call.enqueue(new Callback<ArrayList<ServiceOperatorList>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<ServiceOperatorList>> call, @NonNull Response<ArrayList<ServiceOperatorList>> response) {
                List<ServiceOperatorList> serviceoperatorlist = response.body();
                if (response.isSuccessful() && serviceoperatorlist != null) {
                    yTriprequestview.mOperatorlist(serviceoperatorlist.get(3).getOperators());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<ServiceOperatorList>> call, @NonNull Throwable t) {


            }
        });
    }


}