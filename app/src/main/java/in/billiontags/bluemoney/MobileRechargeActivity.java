package in.billiontags.bluemoney;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.adapter.MyPagerAdapter;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.fragments.PostpaidFragment;
import in.billiontags.bluemoney.fragments.PrepaidFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.WalletAmount.getWalletAmount;

@SuppressWarnings("ConstantConditions")
public class MobileRechargeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, Runnable {


    public int mId;
    public ImageView img_back;
    public TextView mWalletAmount;
    public Context mContext;
    MyPreference myPreference;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;
    private int[] tabIcons = {
            R.drawable.ic_icon_home,
            R.drawable.ic_postpaid_new
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile__recharge);
        initObjects();
        initCallbacks();
        initTabs();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
        int posotion = tab.getPosition();
        if (posotion == 0) {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_icon_home);
            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));
        } else {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_postpaid);

            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));
        }


    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

        int posotion = tab.getPosition();
        if (posotion == 0) {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_icon_home_new);

        } else {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_postpaid_new);

        }

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        int posotion = tab.getPosition();
        if (posotion == 0)

        {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_icon_home);
        } else {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_postpaid);
        }
    }

    @Override
    public void run() {

    }

    private void initObjects() {
        mContext = this;
        mTabLayout = findViewById(R.id.tab);
        mViewPager = findViewById(R.id.pager_profile);
        mWalletAmount = findViewById(R.id.amount_bar);
        img_back = findViewById(R.id.img_back);
        myPreference = new MyPreference(mContext);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);
        getWalletAmount(mContext, mWalletAmount);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Prepaid"));
        mFragmentList.add(new PrepaidFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("PostPaid"));
        mFragmentList.add(new PostpaidFragment());
        mPagerAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        setupTabIcons();

    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);

    }


    private void setupTabIcons() {
        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
