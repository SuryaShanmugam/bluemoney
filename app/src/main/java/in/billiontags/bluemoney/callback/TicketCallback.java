package in.billiontags.bluemoney.callback;

/**
 * Created by Bobby on 2/2/2018
 */

public interface TicketCallback {
    void onTicketSelect(int position);
}
