package in.billiontags.bluemoney.callback;

/**
 * Created by Bobby on 2/2/2018
 */

public interface BusTypeCallback {
    void onBusTypeClick(int position);
}
