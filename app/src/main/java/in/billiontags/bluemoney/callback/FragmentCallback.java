package in.billiontags.bluemoney.callback;

import android.support.v4.app.Fragment;

/**
 * Created by Bobby on 13/07/17
 */

public interface FragmentCallback {
    void launchFragment(Fragment fragment, boolean addToBackStack);

    void headerSelected(boolean select);

    void setHeader(boolean sign, String value);
}
