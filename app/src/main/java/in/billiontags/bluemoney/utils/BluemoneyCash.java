package in.billiontags.bluemoney.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.TextView;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.model.BluemoneyCashAmount;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BluemoneyCash {

    public static void getBluemoneycash(String mUrl, final Context context, final TextView bluemoneycashamount,
                                        final int mRechargeAmount, final TextView payableamount) {

        RechargesService rechargeService = ApiClient.getClient().create(RechargesService.class);
        Call<BluemoneyCashAmount> call = rechargeService.getBluemoneycash(mUrl);
        call.enqueue(new Callback<BluemoneyCashAmount>() {
            @Override
            public void onResponse(@NonNull Call<BluemoneyCashAmount> call, @NonNull Response<BluemoneyCashAmount> response) {
                BluemoneyCashAmount bluemoneycash = response.body();
                if (response.isSuccessful() && bluemoneycash != null) {

                    int percentage = (mRechargeAmount * 2 / 100);
                    Log.e("dkjfhdf", "" + percentage);
                    Log.e("dkjfhdf", "" + percentage);

                    if (percentage < bluemoneycash.getmMaxAmount()) {
                        bluemoneycashamount.setText(String.format("₹  %s", String.valueOf(percentage)));
                        int amount = mRechargeAmount - percentage;
                        payableamount.setText(String.format("₹  %s", String.valueOf(amount)));
                    } else {
                        bluemoneycashamount.setText(String.format("₹  %s", String.valueOf(bluemoneycash.getmMaxAmount())));
                        int amount = mRechargeAmount - bluemoneycash.getmMaxAmount();
                        payableamount.setText(String.format("₹  %s", String.valueOf(amount)));
                    }


                } else {
                    ErrorHandler.processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BluemoneyCashAmount> call, @NonNull Throwable t) {
                ToastBuilder.build(context, t.getMessage());

            }
        });


    }
}
