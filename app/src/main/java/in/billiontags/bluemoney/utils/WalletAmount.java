package in.billiontags.bluemoney.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletAmount {


    public static void getWalletAmount(final Context context, final TextView textView) {

        MyPreference myPreference = new MyPreference(context);
        RechargesService rechargeService = ApiClient.getClient().create(RechargesService.class);
        Call<in.billiontags.bluemoney.model.WalletAmount> call = rechargeService.getWalletAmount("Token " + myPreference.getToken());
        call.enqueue(new Callback<in.billiontags.bluemoney.model.WalletAmount>() {


            @Override
            public void onResponse(@NonNull Call<in.billiontags.bluemoney.model.WalletAmount> call, @NonNull Response<in.billiontags.bluemoney.model.WalletAmount> response) {
                in.billiontags.bluemoney.model.WalletAmount walletAmount = response.body();
                if (response.isSuccessful() && walletAmount != null) {
                    int mTotalCash = walletAmount.getmBluemoneyCash() + walletAmount.getmUserCash();
                    if (walletAmount.getmBluemoneyCash() > 0) {
                        if (mTotalCash > 0) {
                            textView.setText(String.format("₹ %s", String.valueOf(mTotalCash)));
                        } else {
                            textView.setText(String.format("₹ %s", String.valueOf(walletAmount.getmUserCash())));
                        }
                    } else {
                        textView.setText(String.format("₹ %s", String.valueOf("0")));

                    }
                } else {
                    ErrorHandler.processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<in.billiontags.bluemoney.model.WalletAmount> call, @NonNull Throwable t) {
                ToastBuilder.build(context, t.getMessage());

            }
        });


    }

}
