package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.AddWalletMoney;
import in.billiontags.bluemoney.model.Data;
import in.billiontags.bluemoney.model.PaymentSuccess;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.WalletAmount.getWalletAmount;

public class AddWalletMoneyActivity extends AppCompatActivity implements View.OnClickListener, PaymentResultWithDataListener {


    int mAmount;
    int total = 0;
    private Context mContext;
    private TextInputLayout mTextLayoutAmount;
    private EditText mEditTextAmount;
    private ProgressDialog mProgressDialog;
    private MyPreference mPreference;
    private Button mButtonContinue;
    private TextView mTextviewThousand, mTextViewFivehundred, mTextViewHundred, mTextViewFifty, mTextviewWalletAmount;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wallet_money);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mButtonContinue.setOnClickListener(this);
        mTextviewThousand.setOnClickListener(this);
        mTextViewFivehundred.setOnClickListener(this);
        mTextViewHundred.setOnClickListener(this);
        mTextViewFifty.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
    }

    private void initObjects() {

        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mPreference = new MyPreference(mContext);
        mTextLayoutAmount = findViewById(R.id.layout_Amount);
        mEditTextAmount = findViewById(R.id.Amount);
        mButtonContinue = findViewById(R.id.btn_Continue);
        mTextviewThousand = findViewById(R.id.txt_thousand);
        mTextViewFivehundred = findViewById(R.id.txt_FiveHundred);
        mTextViewHundred = findViewById(R.id.txt_Hundred);
        mTextViewFifty = findViewById(R.id.txt_fifty);
        mImageBack = findViewById(R.id.img_back);
        mTextviewWalletAmount = findViewById(R.id.txt_walletbalance);

        getWalletAmount(mContext, mTextviewWalletAmount);
    }

    @Override
    public void onClick(View v) {
        if (v == mTextviewThousand) {
            mAmount = 1000;
            addMethod(mAmount);
        } else if (v == mTextViewFivehundred) {
            mAmount = 500;
            addMethod(mAmount);
        } else if (v == mTextViewHundred) {
            mAmount = 100;
            addMethod(mAmount);
        } else if (v == mTextViewFifty) {
            mAmount = 50;
            addMethod(mAmount);
        } else if (v == mButtonContinue) {
            processdata();
        } else if (v == mImageBack) {
            onBackPressed();
        }
    }

    private void addMethod(int amount) {
        total += amount;
        mEditTextAmount.setText(String.valueOf(total));

    }

    private void processdata() {
        String mWalletAmount = mEditTextAmount.getText().toString();
        Log.e("mWalletAmount", "" + mWalletAmount);

        if (validateInput(mWalletAmount)) {
            showProgressDialog("Processsing....");
            mAddMoney(new AddWalletMoney(mWalletAmount));
        }
    }


    private boolean validateInput(String walletAmount) {
        if (TextUtils.isEmpty(walletAmount)) {
            mEditTextAmount.requestFocus();
            mTextLayoutAmount.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Amount"));
            return false;
        }
        return true;
    }

    private void mAddMoney(AddWalletMoney addWalletMoney) {

        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<AddWalletMoney> call = authService.mAddWalletMoney("Token " + mPreference.getToken(), addWalletMoney);
        call.enqueue(new Callback<AddWalletMoney>() {

            @Override
            public void onResponse(@NonNull Call<AddWalletMoney> call, @NonNull Response<AddWalletMoney> response) {
                AddWalletMoney addWalletMoney = response.body();
                hideProgressDialog();

                if (response.isSuccessful() && addWalletMoney != null) {
                    hideProgressDialog();
                    int amount = Integer.valueOf(addWalletMoney.getmAmount());
                    String mOrderId = addWalletMoney.getmOrderId();
                    processPayment(amount, mOrderId);
                } else {
                    hideProgressDialog();
                    try {
                        Log.e("response", "" + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddWalletMoney> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
                hideProgressDialog();
            }
        });


    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void processPayment(int amount, String orderId) {
        Checkout checkout = new Checkout();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getString(R.string.app_name));
            jsonObject.put("description", "Pay Now");
            jsonObject.put("currency", "INR");
            jsonObject.put("amount", amount);
            JSONObject themeObject = new JSONObject();
            themeObject.put("color", "#01ABF2");
            themeObject.put("emi_mode", true);
            jsonObject.put("theme", themeObject);
            JSONObject prefillObject = new JSONObject();
            prefillObject.put("name", mPreference.getName());
            prefillObject.put("contact", mPreference.getPhone());
            prefillObject.put("email", mPreference.getEmail());
            jsonObject.put("prefill", prefillObject);
            jsonObject.put("order_id", orderId);
        } catch (JSONException e) {
            Toast.makeText(this, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        checkout.open(this, jsonObject);
    }


    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Log.e("paymentsuccess", "" + paymentData.getOrderId());
        Log.e("paymentsuccess", "" + paymentData.getPaymentId());
        Log.e("paymentsuccess", "" + paymentData.getSignature());
        paymentSuccess(new PaymentSuccess(paymentData.getOrderId(), paymentData.getPaymentId(), paymentData.getSignature(), "1"));
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        ToastBuilder.build(mContext, s);

    }

    private void paymentSuccess(PaymentSuccess paymentSuccess) {

        showProgressDialog("Processing....");
        RechargesService checkoutService = ApiClient.getClient().create(RechargesService.class);
        Call<Data> call = checkoutService.paymentSuccess("Token " + mPreference.getToken(),
                paymentSuccess);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                hideProgressDialog();
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    ToastBuilder.build(mContext, dataResponse.getData());
                } else {
                    Log.e("error", "" + response.errorBody());
                    ToastBuilder.build(mContext, response.message());
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

}
