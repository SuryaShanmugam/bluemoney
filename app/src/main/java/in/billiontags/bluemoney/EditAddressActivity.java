package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.AddNewAddress;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launchClearStack;

public class EditAddressActivity extends AppCompatActivity implements View.OnClickListener {

    public Context mContext;
    public String mobile, street, area, city, state, country, zipcode, mUrl;
    public int mId;
    protected View mRootView;
    EditText mEditTextmMobile, mEditTextmStreet, mmEditTextmArea, mEditTextmCity,
            mEditTextmState, mEditTextmCountry, mEditTextmZipCode;
    private TextInputLayout mTextLayoutmMobile, mTextLayoutStreet, mTextLayoutmArea, mTextLayoutmCity,
            mTextLayoutmState, mTextLayoutmCountry, mTextLayoutmZipCode;
    private Button mSave;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        initObjects();
        processBundle();
        initCallbacks();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void processBundle() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mId = bundle.getInt(Constant.ADDR_ID);
            mobile = bundle.getString(Constant.ADDR_MOBILE);
            street = bundle.getString(Constant.ADDR_STREET);
            area = bundle.getString(Constant.ADDR_AREA);
            city = bundle.getString(Constant.ADDR_CITY);
            state = bundle.getString(Constant.ADDR_STATE);
            country = bundle.getString(Constant.ADDR_COUNTRY);
            zipcode = bundle.getString(Constant.ADDR_ZIPCODE);
            mEditTextmMobile.setText(mobile);
            mEditTextmStreet.setText(street);
            mmEditTextmArea.setText(area);
            mEditTextmCity.setText(city);
            mEditTextmState.setText(state);
            mEditTextmCountry.setText(country);
            mEditTextmZipCode.setText(zipcode);
        }

    }

    private void initCallbacks() {
        mSave.setOnClickListener(this);
        mImageBack.setOnClickListener(this);

    }

    private void initObjects() {
        mContext = this;
        mTextLayoutmMobile = findViewById(R.id.layout_Mobilenumber);
        mTextLayoutStreet = findViewById(R.id.layout_street);
        mTextLayoutmArea = findViewById(R.id.layout_Area);
        mTextLayoutmCity = findViewById(R.id.layout_City);
        mTextLayoutmState = findViewById(R.id.layout_State);
        mTextLayoutmCountry = findViewById(R.id.layout_ZipCode);
        mTextLayoutmZipCode = findViewById(R.id.layout_Country);

        mEditTextmMobile = findViewById(R.id.edit_mobile);
        mEditTextmStreet = findViewById(R.id.edit_Street);
        mmEditTextmArea = findViewById(R.id.edit_area);
        mEditTextmCity = findViewById(R.id.edit_city);
        mEditTextmState = findViewById(R.id.edit_State);
        mEditTextmCountry = findViewById(R.id.edit_Country);
        mEditTextmZipCode = findViewById(R.id.edit_zipcode);
        mSave = findViewById(R.id.btn_save);
        mSave.setText("Update");
        mImageBack = findViewById(R.id.img_back);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);

    }

    @Override
    public void onClick(View v) {
        if (v == mSave) {
            processAddress();
        } else if (v == mImageBack) {
            onBackPressed();
        }

    }

    private void processAddress() {
        String mobile = mEditTextmMobile.getText().toString().trim();
        String street = mEditTextmStreet.getText().toString().trim();
        String area = mmEditTextmArea.getText().toString().trim();
        String city = mEditTextmCity.getText().toString().trim();
        String state = mEditTextmState.getText().toString().trim();
        String country = mEditTextmCountry.getText().toString().trim();
        String zipcode = mEditTextmZipCode.getText().toString().trim();
        if (validateInput(mobile, street, area, city, state, zipcode, country)) {
            showProgressDialog("Processing....");
            mUrl = ApiClient.BASE_URL + "users/update/address/" + mId + "/";
            VerifyAddress(new AddNewAddress(mobile, street, area, city, state, zipcode, country, true), mUrl);
        }
    }

    private boolean validateInput(String mobile, String street, String area, String city, String state, String zipcode, String country) {
        if (TextUtils.isEmpty(mobile)) {
            mEditTextmMobile.requestFocus();
            mTextLayoutmMobile.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Mobile"));
            return false;
        } else if (mobile.length() < 10) {
            mTextLayoutmMobile.setError(String.format(Locale.getDefault(), getString(R.string.error_length),
                    "Mobile Number", 10, "digits"));
            mEditTextmMobile.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(street)) {
            mEditTextmStreet.requestFocus();
            mTextLayoutStreet.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Street"));
            return false;
        } else if (TextUtils.isEmpty(area)) {
            mmEditTextmArea.requestFocus();
            mTextLayoutmArea.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Area"));
            return false;
        } else if (TextUtils.isEmpty(city)) {
            mEditTextmCity.requestFocus();
            mTextLayoutmCity.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "City"));
            return false;
        } else if (TextUtils.isEmpty(state)) {
            mEditTextmState.requestFocus();
            mTextLayoutmState.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "State"));
            return false;
        } else if (TextUtils.isEmpty(zipcode)) {
            mEditTextmZipCode.requestFocus();
            mTextLayoutmZipCode.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Zipocode"));
            return false;
        } else if (TextUtils.isEmpty(country)) {
            mEditTextmCountry.requestFocus();
            mTextLayoutmCountry.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Country"));
            return false;
        }
        return true;
    }


    private void VerifyAddress(AddNewAddress addNewAddress, String Url) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<AddNewAddress> call = authService.mAddEditAddress("Token " + mPreference.getToken(), Url, addNewAddress);
        call.enqueue(new Callback<AddNewAddress>() {
            @Override
            public void onResponse(@NonNull Call<AddNewAddress> call, @NonNull Response<AddNewAddress> response) {
                AddNewAddress userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    hideProgressDialog();
                    launchClearStack(mContext, MyAddressActivity.class);
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(@NonNull Call<AddNewAddress> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
