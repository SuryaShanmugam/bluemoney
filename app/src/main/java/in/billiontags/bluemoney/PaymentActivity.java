package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.dialogs.PromocodeDialog;
import in.billiontags.bluemoney.model.Data;
import in.billiontags.bluemoney.model.PaymentSuccess;
import in.billiontags.bluemoney.model.RechargeTransaction;
import in.billiontags.bluemoney.model.WalletAmount;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.BluemoneyCash.getBluemoneycash;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener, Runnable, PaymentResultWithDataListener {


    public ImageView img_back;
    public Context mContext;
    public TextView Wallet_amount, Recharge_amount, Coupon_Amount, BluemoneyAmount, PayableAmount, mApplyCode;
    public String balance;
    public String mrechargeAmount, mobilenumber;
    public boolean paywithwallet;
    private int walletamount;
    private Button Pay_now;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private CheckBox checkbox;
    private String Operator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_);
        initObjects();
        getWalletAmount();
        initCallbacks();
        setUrl();


    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        img_back = findViewById(R.id.img_back);
        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
        Wallet_amount = findViewById(R.id.Wallet_amount);
        Recharge_amount = findViewById(R.id.Recharge_amount);
        Coupon_Amount = findViewById(R.id.Coupon_Amount);
        BluemoneyAmount = findViewById(R.id.Bluemoney_Amount);
        PayableAmount = findViewById(R.id.payable_Amount);
        mApplyCode = findViewById(R.id.Apply_coupon);
        Pay_now = findViewById(R.id.Pay_now);
        checkbox = findViewById(R.id.checkbox);

        Intent i = getIntent();
        mrechargeAmount = i.getStringExtra("RechargeAmount");
        balance = i.getStringExtra("BalanceAmount");
        Operator = i.getStringExtra("Operator");
        mobilenumber = i.getStringExtra("mobile");

        Recharge_amount.setText(String.format("₹  %s", mrechargeAmount));
        Coupon_Amount.setText(String.valueOf("\u20b9  " + 0));
    }

    private void setUrl() {
        String mUrl = ApiClient.BASE_URL + "/wallet/reward/" + 1 + "/";
        getBluemoneycash(mUrl, mContext, BluemoneyAmount, Integer.parseInt(mrechargeAmount), PayableAmount);

    }


    private void initCallbacks() {
        Pay_now.setOnClickListener(this);
        img_back.setOnClickListener(this);
        mApplyCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == Pay_now) {
            processdata();

        } else if (v == mApplyCode) {
            PromocodeDialog promocodeDialog = new PromocodeDialog(this);
            promocodeDialog.promoDialog();


        } else if (v == img_back) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @SuppressWarnings("RedundantIfStatement")
    private void processdata() {
        String mobile = mobilenumber;
        String serviceprovider = Operator;
        String amount = mrechargeAmount;

        if (checkbox.isChecked()) {
            paywithwallet = true;
        } else {
            paywithwallet = false;
        }

        showProgressDialog("Processing....");
        PaynowPrepaid(new RechargeTransaction(serviceprovider, mobile, amount, paywithwallet, "1"));
    }


    @Override
    public void run() {
    }


    private void PaynowPrepaid(RechargeTransaction rechargeTransaction) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<RechargeTransaction> call = authService.getRechargeTransaction("Token " + mPreference.getToken(), rechargeTransaction);
        call.enqueue(new Callback<RechargeTransaction>() {

            @Override
            public void onResponse(@NonNull Call<RechargeTransaction> call, @NonNull Response<RechargeTransaction> response) {
                RechargeTransaction validateRecharge = response.body();

                if (response.isSuccessful() && validateRecharge != null) {
                    int amount = validateRecharge.getmAmount();
                    String mOrderId = validateRecharge.getmOrderId();
                    hideProgressDialog();
                    processPayment(amount, mOrderId);


                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<RechargeTransaction> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
                hideProgressDialog();
            }
        });


    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    private void getWalletAmount() {
        RechargesService rechargeService = ApiClient.getClient().create(RechargesService.class);
        Call<WalletAmount> call = rechargeService.getWalletAmount("Token " + mPreference.getToken());
        call.enqueue(new Callback<WalletAmount>() {
            @Override
            public void onResponse(@NonNull Call<WalletAmount> call, @NonNull Response<WalletAmount> response) {
                WalletAmount walletAmount = response.body();
                if (response.isSuccessful() && walletAmount != null) {
                    walletamount = walletAmount.getmUserCash();
                    Wallet_amount.setText(String.format("₹  %s", String.valueOf(walletamount)));
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletAmount> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());

            }
        });

    }

    private void processPayment(int amount, String orderId) {
        Checkout checkout = new Checkout();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getString(R.string.app_name));
            jsonObject.put("description", "Pay Now");
            jsonObject.put("currency", "INR");
            jsonObject.put("amount", amount);
            JSONObject themeObject = new JSONObject();
            themeObject.put("color", "#01ABF2");
            themeObject.put("emi_mode", true);
            jsonObject.put("theme", themeObject);
            JSONObject prefillObject = new JSONObject();
            prefillObject.put("name", mPreference.getName());
            prefillObject.put("contact", mPreference.getPhone());
            prefillObject.put("email", mPreference.getEmail());
            jsonObject.put("prefill", prefillObject);
            jsonObject.put("order_id", orderId);
        } catch (JSONException e) {
            Toast.makeText(this, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        checkout.open(this, jsonObject);
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Log.e("paymentsuccess", "" + paymentData.getOrderId());
        Log.e("paymentsuccess", "" + paymentData.getPaymentId());
        Log.e("paymentsuccess", "" + paymentData.getSignature());
        paymentSuccess(new PaymentSuccess(paymentData.getOrderId(), paymentData.getPaymentId(), paymentData.getSignature(), "1"));
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        ToastBuilder.build(mContext, s);
    }


    private void paymentSuccess(PaymentSuccess paymentSuccess) {

        showProgressDialog("Processing....");
        RechargesService checkoutService = ApiClient.getClient().create(RechargesService.class);
        Call<Data> call = checkoutService.paymentSuccess("Token " + mPreference.getToken(),
                paymentSuccess);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                hideProgressDialog();
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    ToastBuilder.build(mContext, dataResponse.getData());
                } else {
                    Log.e("error", "" + response.errorBody());
                    ToastBuilder.build(mContext, response.message());
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }
}
