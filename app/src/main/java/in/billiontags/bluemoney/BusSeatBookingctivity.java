package in.billiontags.bluemoney;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.billiontags.bluemoney.adapter.BusSeatBookingAdapter;
import in.billiontags.bluemoney.adapter.MyPagerAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.callback.BusSeatBookingCallback;
import in.billiontags.bluemoney.fragments.BusLowerDeckFragment;
import in.billiontags.bluemoney.fragments.BusUpperDeckFragment;
import in.billiontags.bluemoney.model.BusBoardingPointLocation;
import in.billiontags.bluemoney.model.BusLowerDesk;
import in.billiontags.bluemoney.model.BusSeatBooking;
import in.billiontags.bluemoney.model.BusSeatBookingProcess;
import in.billiontags.bluemoney.model.BusSeatCountSelected;
import in.billiontags.bluemoney.model.BusSeatCountUnSelected;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.app.Constant.KEY_BUSFACILITY;
import static in.billiontags.bluemoney.app.Constant.KEY_BUSTYPE;
import static in.billiontags.bluemoney.app.Constant.KEY_BUSTYPEMODEL;
import static in.billiontags.bluemoney.app.Constant.KEY_OPERATORCODE;
import static in.billiontags.bluemoney.app.Constant.KEY_PARTIALCANCELLATIONS;
import static in.billiontags.bluemoney.app.Constant.KEY_SCHEDULECODE;
import static in.billiontags.bluemoney.app.Constant.KEY_TRAVELDATE;
import static in.billiontags.bluemoney.app.Constant.KEY_TRAVELSNAME;
import static in.billiontags.bluemoney.utils.Activity.launchWithBundle;

public class BusSeatBookingctivity extends AppCompatActivity implements View.OnClickListener, BusSeatBookingCallback,
        TabLayout.OnTabSelectedListener {


    private static final String KEY_ARRAY_BOARDINGPPOINT = "boardingpoints";
    private static final String KEY_ARRAY_DROPPINGPPOINT = "droppingpoints";
    private static final String KEY_SOURCENAME = "sourcename";
    private static final String KEY_DESTINATIONNAME = "destinationname";
    private static final String KEY_SEATNO = "seatno";
    private static final String KEY_SEATFARE = "seatfare";
    private static final String KEY_CANCELLATIONTERMS = "cancellationterms";
    private static final String KEY_TOTALFARE = "totalbusfare";
    public TextView mTextViewTravelerName, mTextviewBusDetails;
    public GridLayoutManager mLayoutManager;
    public ArrayList<BusBoardingPointLocation> mBoardingPointList;
    public ArrayList<BusBoardingPointLocation> mDroppingPointList;
    protected String mTravelDate, mSourceNameCode, mDestinationNameCode,
            mSechedulecode, mOperatorcode, mTravelername, mBustype, mSourceName,
            mDestinationName, mSeatno, mbusTypemodel, mPartialcancellations, mbusfacility;
    int Rowslist, ColumnList;

    ArrayList<String> mTotalSeatCount = new ArrayList<>();
    ArrayList<String> mTotalSeatFare = new ArrayList<>();
    int mNoOfTickets;
    ArrayList<Integer> mtotalseatList = new ArrayList<>();
    double amount;
    double totalfare;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private ArrayList<BusLowerDesk> mBusStructureList;
    private ArrayList<BusLowerDesk> mBusLowerDeckSleeperList;
    private ArrayList<BusLowerDesk> mBusUpperDeckSleeperList;
    private BusSeatBookingAdapter mAdapter;
    private ImageView mImageBack;
    private Button mButtonProceed;
    private TextView mTotalFare, mTotalSeatNo, totalseatcount;
    private LinearLayout mSeaterLayout, mRecyclerviewLayout;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;
    private ArrayList<String> mCancellationTerms = new ArrayList<>();
    private RelativeLayout mFareLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_seat_bookingctivity);
        processBundle();
        initObjects();
        initCallbacks();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonProceed) {
            if (mTotalSeatCount.size() > 0) {
                if (mTotalSeatCount.size() <= 6) {
                    Intent i = new Intent(BusSeatBookingctivity.this, BusSelectBoardingPointActivity.class);
                    i.putParcelableArrayListExtra(KEY_ARRAY_BOARDINGPPOINT, mBoardingPointList);
                    i.putParcelableArrayListExtra(KEY_ARRAY_DROPPINGPPOINT, mDroppingPointList);
                    i.putExtra(KEY_OPERATORCODE, mOperatorcode);
                    i.putExtra(KEY_SCHEDULECODE, mSechedulecode);
                    i.putExtra(KEY_SOURCENAME, mSourceName);
                    i.putExtra(KEY_TRAVELSNAME, mTravelername);
                    i.putExtra(KEY_DESTINATIONNAME, mDestinationName);
                    i.putExtra(KEY_BUSTYPE, mbusTypemodel);
                    i.putExtra(KEY_TRAVELDATE, mTravelDate);
                    i.putExtra(KEY_TOTALFARE, mTotalFare.getText().toString());
                    i.putStringArrayListExtra(KEY_SEATNO, mTotalSeatCount);
                    i.putStringArrayListExtra(KEY_SEATFARE, mTotalSeatFare);
                    startActivity(i);
                } else {
                    ToastBuilder.build(mContext, "Maximum 6 seats to Allowed");
                }
            } else {

                ToastBuilder.build(mContext, "Please Select Atleast 1 Seat");

            }


        }
        if (v == mImageBack) {
            onBackPressed();
        } else if (v == mTextviewBusDetails) {
            Bundle bundle = new Bundle();
            bundle.putStringArrayList(KEY_CANCELLATIONTERMS, mCancellationTerms);
            bundle.putString(KEY_BUSFACILITY, mbusfacility);
            bundle.putString(KEY_BUSTYPEMODEL, mbusTypemodel);
            bundle.putString(KEY_PARTIALCANCELLATIONS, mPartialcancellations);
            launchWithBundle(mContext, BusAmentiesDetailActivity.class, bundle);
            overridePendingTransition(R.anim.slide_in_bottom, R.anim.slide_in_bottom);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initCallbacks() {
        mButtonProceed.setOnClickListener(this);
        mTextviewBusDetails.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mTabLayout.addOnTabSelectedListener(this);
    }

    private void initObjects() {
        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mTextViewTravelerName = findViewById(R.id.txt_travlername);
        mFareLayout = findViewById(R.id.fare_layout);
        mTextviewBusDetails = findViewById(R.id.txt_Details);
        mButtonProceed = findViewById(R.id.btn_Proceed);
        totalseatcount = findViewById(R.id.totalseatcount);
        mSeaterLayout = findViewById(R.id.mseater_availablity);
        mRecyclerviewLayout = findViewById(R.id.recyclerview_layout);
        mTextViewTravelerName.setText(String.format("%s\n%s", mTravelername, mBustype));
        mRecyclerView = findViewById(R.id.busseat);
        mImageBack = findViewById(R.id.img_back);
        mTotalFare = findViewById(R.id.total_fare);
        mTotalSeatNo = findViewById(R.id.totalseat);
        mTabLayout = findViewById(R.id.tab);
        mViewPager = findViewById(R.id.pager_profile);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);
        mBusStructureList = new ArrayList<>();
        mBoardingPointList = new ArrayList<>();
        mDroppingPointList = new ArrayList<>();
        mBusLowerDeckSleeperList = new ArrayList<>();
        mBusUpperDeckSleeperList = new ArrayList<>();

        getBusListService(new BusSeatBookingProcess(mTravelDate, mSourceNameCode, mDestinationNameCode, mOperatorcode, mSechedulecode));
        mAdapter = new BusSeatBookingAdapter(mContext, mBusStructureList, this);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Lower Deck"));
        mFragmentList.add(BusLowerDeckFragment.newInstance(ColumnList, Rowslist, mBusLowerDeckSleeperList));
        mTabLayout.addTab(mTabLayout.newTab().setText("Upper Deck"));
        mFragmentList.add(BusUpperDeckFragment.newInstance(ColumnList, Rowslist, mBusUpperDeckSleeperList));
        mPagerAdapter.notifyDataSetChanged();
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        setTabsFont();

    }

    @SuppressLint("DefaultLocale")
    @Override
    public void mOnItemClick(int pos) {
        BusLowerDesk busLowerDesk = mBusStructureList.get(pos);
        if (busLowerDesk.getmSeatStatus().equals("A") ||
                busLowerDesk.getmSeatStatus().equals("F")
                || busLowerDesk.getmSeatStatus().equals("M")) {
            busLowerDesk.setmSeatStatus("S");
            mItemClickSelect(busLowerDesk.getmSeatFare(), busLowerDesk.getmSeatNumber(), pos);
        } else if (busLowerDesk.getmSeatStatus().equals("S")) {
            busLowerDesk.setmSeatStatus("A");
            mAdapter.notifyDataSetChanged();
            mItemUnSelect(busLowerDesk.getmSeatNumber(), busLowerDesk.getmSeatFare(), pos);
        }
        mAdapter.notifyDataSetChanged();

    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBusSeatSelected(BusSeatCountSelected event) {
        mItemClickSelect(event.getFare(), event.getmSeatNumber(), event.getPosition());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBusSeatUnSelected(BusSeatCountUnSelected event) {

        mItemUnSelect(event.getmSeatNumber(), event.getFare(), event.getPosition());
    }


    @SuppressLint("DefaultLocale")
    public void mItemClickSelect(double fare, String seatNumber, int pos) {
        mTotalSeatCount.add(seatNumber);
        mtotalseatList.add(pos);
        mTotalSeatFare.add(String.valueOf(fare));
        mTotalSeatNo.append(seatNumber + ",");
        totalseatcount.setText(String.format("Seat (%d)", mtotalseatList.size()));
        totalfare = fare;
        amount += totalfare;
        mTotalFare.setText(String.format("Total Fare ₹ %s", String.valueOf(amount)));
    }

    public void mItemUnSelect(String seatNumber, double fare, int pos) {
        totalfare = fare;
        amount -= totalfare;
        mTotalFare.setText(String.format("Total Fare ₹ %s", String.valueOf(amount)));
        mtotalseatList.remove(Integer.valueOf(pos));
        mTotalSeatCount.remove(seatNumber);
        mTotalSeatFare.remove(String.valueOf(fare));
        String value = String.valueOf(mtotalseatList.size());
        totalseatcount.setText(String.format("Seat (%s)", value.replace("[", "")));
        mTotalSeatNo.setText(String.format("%s", mTotalSeatCount.toString().replace("[", "").replace("]", "")));
    }

    private void initRecyclerView() {
        mLayoutManager = new GridLayoutManager(mContext, Rowslist, GridLayoutManager.HORIZONTAL, false);
        mLayoutManager.setSpanCount(ColumnList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mTravelDate = bundle.getString(KEY_TRAVELDATE);
            mSourceNameCode = bundle.getString(Constant.KEY_SOURCEPLACE);
            mDestinationNameCode = bundle.getString(Constant.KEY_DESTINATIONPLACE);
            mSechedulecode = bundle.getString(KEY_SCHEDULECODE);
            mOperatorcode = bundle.getString(KEY_OPERATORCODE);
            mTravelername = bundle.getString(KEY_TRAVELSNAME);
            mBustype = bundle.getString(KEY_BUSTYPE);
            mSourceName = bundle.getString(Constant.KEY_SOURCENAME);
            mDestinationName = bundle.getString(Constant.KEY_DESTINATIONNAME);
            mNoOfTickets = bundle.getInt(Constant.KEY_NOOFTICKETS);
            mCancellationTerms.addAll(bundle.getStringArrayList(Constant.KEY_CANCELLATIONTERMS));
            mbusTypemodel = bundle.getString(Constant.KEY_BUSTYPEMODEL);
            mPartialcancellations = bundle.getString(Constant.KEY_PARTIALCANCELLATIONS);
            mbusfacility = bundle.getString(Constant.KEY_BUSFACILITY);
        }

    }

    private void getBusListService(BusSeatBookingProcess busSeatBookingProcess) {
        showProgressDialog("Loading....");
        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<BusSeatBooking> call = categoryService.getBusSeatDetails(busSeatBookingProcess);
        call.enqueue(new retrofit2.Callback<BusSeatBooking>() {

            @Override
            public void onResponse(@NonNull Call<BusSeatBooking> call, @NonNull Response<BusSeatBooking> response) {
                BusSeatBooking busSeatBooking = response.body();
                if (response.isSuccessful() && busSeatBooking != null) {
                    if (busSeatBooking.getmRowslist().isJsonArray()) {
                        Rowslist = Integer.parseInt(busSeatBooking.getmRowslist().getAsJsonArray().get(0).getAsString());
                        ColumnList = Integer.parseInt(busSeatBooking.getmColumnList().getAsJsonArray().get(0).getAsString());
                        initTabs();
                    } else {
                        ColumnList = Integer.parseInt(busSeatBooking.getmColumnList().getAsString());
                        Rowslist = Integer.parseInt(busSeatBooking.getmRowslist().getAsString());
                    }


                    if (busSeatBooking.getmBoardingPoints() != null) {
                        mBoardingPointList.addAll(busSeatBooking.getmBoardingPoints());
                    } else if (busSeatBooking.getmDroppingPoints() != null) {
                        mDroppingPointList.addAll(busSeatBooking.getmDroppingPoints());
                    }

                    if (busSeatBooking.getMbusMapStructure().getmLowerDeck() != null) {
                        hideProgressDialog();
                        mBusStructureList.clear();
                        mRecyclerviewLayout.setVisibility(View.VISIBLE);
                        mSeaterLayout.setVisibility(View.VISIBLE);
                        mFareLayout.setVisibility(View.VISIBLE);
                        mButtonProceed.setVisibility(View.VISIBLE);
                        mTabLayout.setVisibility(View.GONE);
                        mViewPager.setVisibility(View.GONE);
                        Collections.sort(busSeatBooking.getMbusMapStructure().getmLowerDeck(), new Comparator<BusLowerDesk>() {
                            @Override
                            public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                                return Integer.valueOf(o1.getmColumn()).compareTo(o2.getmColumn());
                            }
                        });
                        ArrayList<BusLowerDesk> mColumnList1 = new ArrayList<>();
                        ArrayList<BusLowerDesk> mColumnList2 = new ArrayList<>();
                        ArrayList<BusLowerDesk> mColumnList3 = new ArrayList<>();
                        ArrayList<BusLowerDesk> mColumnList4 = new ArrayList<>();
                        ArrayList<BusLowerDesk> mColumnList5 = new ArrayList<>();
                        for (int m = 0; m < busSeatBooking.getMbusMapStructure().getmLowerDeck().size(); m++) {
                            if (busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m).getmColumn() == 1) {
                                mColumnList1.add(busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m));
                            } else if (busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m).getmColumn() == 2) {
                                mColumnList2.add(busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m));
                            } else if (busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m).getmColumn() == 3) {
                                mColumnList3.add(busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m));
                            } else if (busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m).getmColumn() == 4) {
                                mColumnList4.add(busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m));
                            } else if (busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m).getmColumn() == 5) {
                                mColumnList5.add(busSeatBooking.getMbusMapStructure().getmLowerDeck().get(m));
                            }
                        }
                        Collections.sort(mColumnList1, new Comparator<BusLowerDesk>() {
                            @Override
                            public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                                return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                            }
                        });

                        Collections.sort(mColumnList2, new Comparator<BusLowerDesk>() {
                            @Override
                            public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                                return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                            }
                        });
                        Collections.sort(mColumnList3, new Comparator<BusLowerDesk>() {
                            @Override
                            public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                                return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                            }
                        });
                        Collections.sort(mColumnList4, new Comparator<BusLowerDesk>() {
                            @Override
                            public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                                return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                            }
                        });
                        Collections.sort(mColumnList5, new Comparator<BusLowerDesk>() {
                            @Override
                            public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                                return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                            }
                        });
                        mBusStructureList.addAll(mColumnList1);
                        mBusStructureList.addAll(mColumnList2);
                        mBusStructureList.addAll(mColumnList3);
                        mBusStructureList.addAll(mColumnList4);
                        mBusStructureList.addAll(mColumnList5);
                        initRecyclerView();
                        mAdapter.notifyDataSetChanged();


                        for (int i = 0; i < busSeatBooking.getMbusMapStructure().getmLowerDeck().size(); i++) {
                            if (busSeatBooking.getMbusMapStructure().getmLowerDeck().get(i).getmLayer().equals("1") && busSeatBooking.getMbusMapStructure().getmUpperDeck() != null) {
                                mBusLowerDeckSleeperList.add(busSeatBooking.getMbusMapStructure().getmLowerDeck().get(i));
                                mSeaterLayout.setVisibility(View.GONE);
                                mRecyclerviewLayout.setVisibility(View.GONE);
                                mFareLayout.setVisibility(View.VISIBLE);
                                mButtonProceed.setVisibility(View.VISIBLE);
                                mTabLayout.setVisibility(View.VISIBLE);
                                mViewPager.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                    if (busSeatBooking.getMbusMapStructure().getmUpperDeck() != null) {
                        for (int j = 0; j < busSeatBooking.getMbusMapStructure().getmUpperDeck().size(); j++) {
                            if (busSeatBooking.getMbusMapStructure().getmUpperDeck().get(j).getmLayer().equals("2")) {
                                hideProgressDialog();
                                mBusUpperDeckSleeperList.add(busSeatBooking.getMbusMapStructure().getmUpperDeck().get(j));

                            }
                        }
                    }

                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BusSeatBooking> call, @NonNull Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(mContext.getAssets(),
                                    "fonts/Lato-Regular.ttf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(mContext.getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

}

