package in.billiontags.bluemoney;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.adapter.BusPassengerTicketdetailsAdapter;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.model.BusAddpassengerdetails;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.app.Constant.KEY_DESTINATIONNAME;
import static in.billiontags.bluemoney.app.Constant.KEY_SELECTEDDROPPINGLOCATION;
import static in.billiontags.bluemoney.app.Constant.KEY_SOURCENAME;
import static in.billiontags.bluemoney.utils.Utils.getbusTravelDate;

public class BusMakePaymentActivity extends AppCompatActivity implements View.OnClickListener {

    public Context mContext;
    public ArrayList<BusAddpassengerdetails> mPassengerDetails = new ArrayList<>();
    protected TextView mTextViewTravelsName, mTextViewmBusType, mTextViewTravelDate,
            mTextViewFromSource, mTextViewToDestination, mTextViewBoardingLocation, mTextViewDroppingLocation, mTextViewTotalFare;
    private RecyclerView mRecyclerView;
    private ArrayList<BusAddpassengerdetails> mMyordersItemList = new ArrayList<>();
    private BusPassengerTicketdetailsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private String mSourceName, mDestinationName, mTravelername,
            mBusType, mTravelDate, mBoardinpointLocation, mDroppingPointLocation, mTotalFare;
    private Button mProceed;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_make_payment);
        processBundle();
        initObjects();
        initCallbacks();
        initRecyclerView();
    }

    private void initCallbacks() {
        mProceed.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        mMyordersItemList = new ArrayList<>();
        if (bundle != null) {
            mSourceName = bundle.getString(KEY_SOURCENAME);
            mDestinationName = bundle.getString(KEY_DESTINATIONNAME);
            mTravelername = bundle.getString(Constant.KEY_TRAVELSNAME);
            mMyordersItemList = bundle.getParcelableArrayList(Constant.KEY_ARRAY_PASSENGERDETAILS);
            mBusType = bundle.getString(Constant.KEY_BUSTYPE);
            mTravelDate = bundle.getString(Constant.KEY_TRAVELDATE);
            mTotalFare = bundle.getString(Constant.KEY_TOTALFARE);
            mBoardinpointLocation = bundle.getString(Constant.KEY_SELECTEDBOARDINGLOCATION);
            mDroppingPointLocation = bundle.getString(KEY_SELECTEDDROPPINGLOCATION);
        }

        mPassengerDetails.addAll(mMyordersItemList);
    }

    private void initRecyclerView() {

        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);


    }

    private void initObjects() {
        mContext = this;
        mRecyclerView = findViewById(R.id.recyclerview);
        mProceed = findViewById(R.id.btn_Proceed);
        mImageBack = findViewById(R.id.img_back);
        mTextViewTravelsName = findViewById(R.id.txt_travelsname);
        mTextViewmBusType = findViewById(R.id.title_bus_type);
        mTextViewTravelDate = findViewById(R.id.txt_travelDate);
        mTextViewFromSource = findViewById(R.id.from_source);
        mTextViewToDestination = findViewById(R.id.txt_destination);
        mTextViewBoardingLocation = findViewById(R.id.txt_boardinglocation);
        mTextViewTotalFare = findViewById(R.id.totalfare);
        mTextViewDroppingLocation = findViewById(R.id.txt_droppingpoint);
        mAdapter = new BusPassengerTicketdetailsAdapter(mContext, mMyordersItemList);
        mLayoutManager = new LinearLayoutManager(mContext);
        mTextViewTravelsName.setText(mTravelername);
        mTextViewmBusType.setText(mBusType);
        mTextViewTravelDate.setText(String.format("Travel Date -%s", getbusTravelDate(mTravelDate)));
        mTextViewFromSource.setText(mSourceName);
        mTextViewToDestination.setText(mDestinationName);
        mTextViewBoardingLocation.setText(mBoardinpointLocation);
        mTextViewDroppingLocation.setText(mDroppingPointLocation);
        mTextViewTotalFare.setText(mTotalFare);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }
    }
}
