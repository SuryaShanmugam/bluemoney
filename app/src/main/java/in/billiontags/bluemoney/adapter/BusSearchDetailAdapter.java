package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusSearchDetailCallback;
import in.billiontags.bluemoney.holder.BusSearchDetailHolder;
import in.billiontags.bluemoney.model.BusSearchResults;

import static in.billiontags.bluemoney.utils.Utils.getDuration;
import static in.billiontags.bluemoney.utils.Utils.getParsedday;
import static in.billiontags.bluemoney.utils.Utils.getTime;


public class BusSearchDetailAdapter extends RecyclerView.Adapter<BusSearchDetailHolder> {

    private Context mContext;
    private ArrayList<BusSearchResults> mBusSearchResults;
    private BusSearchDetailCallback mCallback;
    private String mPunctuality, mBusQuality, mStaffbehaviour;
    private double mPunctualityrating, mBusQualityRating, mStaffRating, mTotalRating;

    public BusSearchDetailAdapter(Context context, ArrayList<BusSearchResults> myPhotoList, BusSearchDetailCallback callback) {
        mContext = context;
        mBusSearchResults = myPhotoList;
        mCallback = callback;

    }

    @NonNull
    @Override
    public BusSearchDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell, parent, false);
        return new BusSearchDetailHolder(view, mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull BusSearchDetailHolder holder, final int position) {
        BusSearchResults busSearchResults = mBusSearchResults.get(position);
        if (!busSearchResults.getmTicketFare().isJsonArray()) {
            holder.mTextViewPrice.setText(String.format("₹ %s", busSearchResults.getmTicketFare().getAsString()));
        } else {
            holder.mTextViewPrice.setText(String.format("₹ %s", busSearchResults.getmTicketFare().getAsJsonArray().get(0).getAsString()));
        }
        holder.mTextViewdate.setText(busSearchResults.getmTravelDate());
        holder.mTextViewTravelsName.setText(busSearchResults.getmOperatorName());
        holder.mTextViewBusType.setText(busSearchResults.getmBustype());
        holder.mTextViewSeats.setText(busSearchResults.getmAvailableSeats());
        holder.mTextViewDuration.setText(String.format("%s hrs", getDuration(busSearchResults.getmJourneyTime())));
        holder.mTextViewDepatureTime.setText(getTime(busSearchResults.getmDepartureTime()));
        mPunctuality = busSearchResults.getRatings().getmPunctuality();
        mBusQuality = busSearchResults.getRatings().getmPunctuality();
        mStaffbehaviour = busSearchResults.getRatings().getmPunctuality();
        mgetRating();
        holder.mTextViewRatingCount.setText(String.format("%s ratings", String.valueOf(mTotalRating)));
        holder.mTravelerRating.setRating((float) mTotalRating);
        Date dateToday = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String dateYYYYMMDD = sdf.format(dateToday);
        if (Objects.equals(dateYYYYMMDD, busSearchResults.getmTravelDate())) {
            holder.mTextViewDay.setText("Today");
        } else {
            holder.mTextViewDay.setText(getParsedday(busSearchResults.getmTravelDate()));
        }
    }

    private void mgetRating() {
        if (mPunctuality.equals("GOOD")) {
            mPunctualityrating = 5;
        } else if (mPunctuality.equals("AVERAGE")) {
            mPunctualityrating = 3.5;
        } else if (mPunctuality.equals("POOR")) {
            mPunctualityrating = 1.5;
        } else if (mPunctuality.equals("UNRATED")) {
            mPunctualityrating = 0;
        } else if (mBusQuality.equals("GOOD")) {
            mBusQualityRating = 5;
        } else if (mBusQuality.equals("AVERAGE")) {
            mBusQualityRating = 3.5;
        } else if (mBusQuality.equals("POOR")) {
            mBusQualityRating = 1.5;
        } else if (mBusQuality.equals("UNRATED")) {
            mBusQualityRating = 0;
        } else if (mStaffbehaviour.equals("GOOD")) {
            mStaffRating = 5;
        } else if (mStaffbehaviour.equals("AVERAGE")) {
            mStaffRating = 3.5;
        } else if (mStaffbehaviour.equals("POOR")) {
            mStaffRating = 1.5;
        } else if (mStaffbehaviour.equals("UNRATED")) {
            mStaffRating = 0;

        }
        mTotalRating = mPunctualityrating + mBusQualityRating + mStaffRating / 3;
    }


    @Override
    public int getItemCount() {
        return mBusSearchResults.size();
    }


}







