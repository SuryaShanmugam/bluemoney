package in.billiontags.bluemoney.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.PromocodeCallback;
import in.billiontags.bluemoney.holder.PromocodeViewHolder;
import in.billiontags.bluemoney.model.Promocode;


public class PromocodeAdapter extends RecyclerView.Adapter<PromocodeViewHolder> {

    public Context mContext;
    private ArrayList<Promocode> mMyPhotoList;
    private PromocodeCallback mCallback;

    public PromocodeAdapter(Context context, ArrayList<Promocode> myPhotoList, PromocodeCallback callback) {
        mContext = context;
        mMyPhotoList = myPhotoList;
        mCallback = callback;
    }

    @Override
    public PromocodeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PromocodeViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_promocode, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(PromocodeViewHolder holder, final int position) {
        final Promocode promocode = mMyPhotoList.get(position);
        holder.mPromocode.setText(promocode.getmOfferCode());

        if (promocode.getmAmount() > 0) {
            holder.Amount_rate.setVisibility(View.VISIBLE);
            holder.Amount_rate.setText(String.format("₹ %s", promocode.getmAmount()));
        } else {
            holder.Amount_rate.setVisibility(View.INVISIBLE);
        }

        holder.Validity.setText(String.format("Offers Valid Upto %s Only", promocode.getmExpiryOn()));
        holder.mPromotitle.setText(promocode.getmTitle());
        holder.mDescription.setText(promocode.getmDescription());

    }

    @Override
    public int getItemCount() {
        return mMyPhotoList.size();
    }


}
