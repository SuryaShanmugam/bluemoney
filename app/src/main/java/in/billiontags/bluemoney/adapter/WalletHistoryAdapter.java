package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.RechargeHistoryCallback;
import in.billiontags.bluemoney.holder.RechargehistoryHolder;
import in.billiontags.bluemoney.model.MyOrdersItem;


public class WalletHistoryAdapter extends RecyclerView.Adapter<RechargehistoryHolder> {

    protected MyPreference myPreference;
    private Context mContext;
    private ArrayList<MyOrdersItem> mMyOrdersItemList;
    private RechargeHistoryCallback mCallback;
    private int mColorGreen, mColorRed;

    public WalletHistoryAdapter(Context context, ArrayList<MyOrdersItem> myPhotoList, RechargeHistoryCallback callback) {
        mContext = context;
        mMyOrdersItemList = myPhotoList;
        myPreference = new MyPreference(mContext);
        mCallback = callback;
        mColorGreen = ContextCompat.getColor(mContext, R.color.green);
        mColorRed = ContextCompat.getColor(mContext, R.color.red);
    }

    @Override
    public RechargehistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RechargehistoryHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rechargehistory, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(RechargehistoryHolder holder, final int position) {
        MyOrdersItem myOrdersItem = mMyOrdersItemList.get(position);
        holder.mTextViewStatus.setText(myOrdersItem.ismStatus() ? "Success" : "Failure");
        holder.mTextViewStatus.setTextColor(myOrdersItem.ismBookingStatus() ? mColorGreen : mColorRed);
        holder.mImageViewStatus.setImageResource(R.drawable.ic_wallet_success);
        holder.mTextViewPrice.setText(String.format(mContext.getString(R.string.wallet_history), myOrdersItem.getmTxnAmount()));
        holder.mTextViewPaymentMode.setText("via Wallet");
        holder.mTextViewMobile.setText("9715718698");
        holder.mTextViewDate.setText(myOrdersItem.getmCreatedOn());

    }

    @Override
    public int getItemCount() {
        return mMyOrdersItemList.size();
    }


}







