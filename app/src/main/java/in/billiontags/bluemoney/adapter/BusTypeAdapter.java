package in.billiontags.bluemoney.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusTypeCallback;
import in.billiontags.bluemoney.holder.BusTypeHolder;
import in.billiontags.bluemoney.model.BusType;

/**
 * Created by Bobby on 2/2/2018
 */

public class BusTypeAdapter extends RecyclerView.Adapter<BusTypeHolder> {

    private List<BusType> mBusTypeList;
    private BusTypeCallback mCallback;
    private int mColorNormal, mColorSelected;
    private Context mContext;
    public BusTypeAdapter(Context context, List<BusType> ticketList, BusTypeCallback callback) {
        mBusTypeList = ticketList;
        mCallback = callback;
        mContext = context;
        mColorNormal = ContextCompat.getColor(context, R.color.secondary_txt);
        mColorSelected = ContextCompat.getColor(context, R.color.primary);
    }

    @Override
    public BusTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BusTypeHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bus_type, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(BusTypeHolder holder, int position) {
        BusType busType = mBusTypeList.get(position);
        holder.mTextViewBusType.setText(busType.getBusType());
        holder.mTextViewBusType.setTextColor(busType.isSelected() ? mColorSelected : mColorNormal);


    }

    @Override
    public int getItemCount() {
        return mBusTypeList.size();
    }
}
