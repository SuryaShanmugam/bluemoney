package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.holder.WalletItemHolder;
import in.billiontags.bluemoney.model.WalletTransaction;


public class WalletTransactionAdapter extends RecyclerView.Adapter<WalletItemHolder> {

    protected MyPreference myPreference;
    private Context mContext;
    private ArrayList<WalletTransaction> mWalletTransactionList;

    public WalletTransactionAdapter(Context context, ArrayList<WalletTransaction> myPhotoList) {
        mContext = context;
        mWalletTransactionList = myPhotoList;


    }

    @Override
    public WalletItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WalletItemHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wallet_history, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(WalletItemHolder holder, final int position) {
        WalletTransaction walletTransaction = mWalletTransactionList.get(position);

        holder.mRewardTitle.setText(walletTransaction.getmRewardUnit());
        holder.mCreatedDate.setText(walletTransaction.getmCreatedOn());


        if (!walletTransaction.isMisDebit()) {
            holder.mAmount.setText(String.format("+ %s", String.format(" ₹ %s", String.valueOf(walletTransaction.getmAmount()))));
            holder.mAmount.setTextColor(ContextCompat.getColor(mContext, R.color.amount_green));
        } else {
            holder.mAmount.setText(String.format("- %s", String.format(" ₹ %s", String.valueOf(walletTransaction.getmAmount()))));
            holder.mAmount.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        }

    }

    @Override
    public int getItemCount() {
        return mWalletTransactionList.size();
    }


}







