package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.holder.BusAmenitiesHolder;


public class BusAmenitiesAdapter extends RecyclerView.Adapter<BusAmenitiesHolder> {

    protected MyPreference myPreference;
    private Context mContext;
    private ArrayList<String> mMyOrdersItemList;

    private int mColorGreen, mColorRed;

    public BusAmenitiesAdapter(Context context, ArrayList<String> myPhotoList) {
        mContext = context;
        mMyOrdersItemList = myPhotoList;
        myPreference = new MyPreference(mContext);
        mColorGreen = ContextCompat.getColor(mContext, R.color.green);
        mColorRed = ContextCompat.getColor(mContext, R.color.red);
    }

    @Override
    public BusAmenitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BusAmenitiesHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bus_amenities, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(BusAmenitiesHolder holder, final int position) {

        holder.mTextViewCancellationTerms.setText(mMyOrdersItemList.get(position));

    }

    @Override
    public int getItemCount() {
        return mMyOrdersItemList.size();
    }


}







