package in.billiontags.bluemoney.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.holder.BusAddPassengerDetailsHolder;
import in.billiontags.bluemoney.model.BusAddpassengerdetails;


public class BusAddPassengerDetailsAdapter extends RecyclerView.Adapter<BusAddPassengerDetailsHolder> {

    public Context mContext;
    BusAddPassengerDetailsHolder holder;
    private ArrayList<BusAddpassengerdetails> mBusAddpassengerdetails;
    private String name, age, gender;

    public BusAddPassengerDetailsAdapter(Context context, ArrayList<BusAddpassengerdetails> myPhotoList) {
        mContext = context;
        mBusAddpassengerdetails = myPhotoList;
    }

    @NonNull
    @Override
    public BusAddPassengerDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bus_add_passengerdetails, parent, false);
        holder = new BusAddPassengerDetailsHolder(view);
        return new BusAddPassengerDetailsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BusAddPassengerDetailsHolder holder, @SuppressLint("RecyclerView") int position) {
        final BusAddpassengerdetails busBoardingPointLocation = mBusAddpassengerdetails.get(position);
        holder.mTextViewPassengerName.setText(String.format("Passenger (Seat no:%s)", busBoardingPointLocation.getmSeatNo()));

        holder.mEditTextPassengerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                name = s.toString();
                busBoardingPointLocation.setmPassengername(name);
            }
        });
        holder.mEditTextPassengerAge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                age = s.toString();
                busBoardingPointLocation.setmPassengerage(age);

            }
        });

        holder.grp_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_male:
                        gender = "Male";
                        break;
                    case R.id.radio_female:
                        gender = "Female";
                        break;
                }
                busBoardingPointLocation.setGender(gender);
            }
        });


    }


    @Override
    public int getItemCount() {
        return mBusAddpassengerdetails.size();
    }


}







