package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.holder.BusPassengerTicketDetailsHolder;
import in.billiontags.bluemoney.model.BusAddpassengerdetails;


public class BusPassengerTicketdetailsAdapter extends RecyclerView.Adapter<BusPassengerTicketDetailsHolder> {

    protected MyPreference myPreference;
    private Context mContext;
    private ArrayList<BusAddpassengerdetails> mMyOrdersItemList;


    public BusPassengerTicketdetailsAdapter(Context context, ArrayList<BusAddpassengerdetails> myPhotoList) {
        mContext = context;
        mMyOrdersItemList = myPhotoList;
        myPreference = new MyPreference(mContext);

    }

    @NonNull
    @Override
    public BusPassengerTicketDetailsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BusPassengerTicketDetailsHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_buspassengerticket_details, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(@NonNull BusPassengerTicketDetailsHolder holder, final int position) {


        BusAddpassengerdetails busAddpassengerdetails = mMyOrdersItemList.get(position);
        holder.mTextViewName.setText(busAddpassengerdetails.getmPassengername());
        holder.mTextViewAge.setText(busAddpassengerdetails.getmPassengerage());

        if(busAddpassengerdetails.getGender()!=null)
        {
            if (busAddpassengerdetails.getGender().equals("Male")) {
                holder.mTextViewGender.setText("M");
            } else {
                holder.mTextViewGender.setText("F");
            }
        }
        holder.mTextViewseatno.setText(busAddpassengerdetails.getmSeatNo());
        holder.mTextViewFare.setText(String.format("₹ %s", busAddpassengerdetails.getAmount()));

    }

    @Override
    public int getItemCount() {
        return mMyOrdersItemList.size();
    }


}







