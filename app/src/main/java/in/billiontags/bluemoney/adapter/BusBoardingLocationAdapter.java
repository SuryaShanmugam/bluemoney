package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusBoardingLocationCallback;
import in.billiontags.bluemoney.holder.BusBoardingLocationHolder;
import in.billiontags.bluemoney.model.BusBoardingPointLocation;


public class BusBoardingLocationAdapter extends RecyclerView.Adapter<BusBoardingLocationHolder> {

    private Context mContext;
    private ArrayList<BusBoardingPointLocation> mBusBoardingPointLocation;
    private BusBoardingLocationCallback mCallback;

    public BusBoardingLocationAdapter(Context context, ArrayList<BusBoardingPointLocation> myPhotoList, BusBoardingLocationCallback callback) {
        mContext = context;
        mBusBoardingPointLocation = myPhotoList;
        mCallback = callback;

    }

    @Override
    public BusBoardingLocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BusBoardingLocationHolder
                (LayoutInflater.from(parent.getContext()).inflate(R.layout.item_boardingpoints, parent, false), mCallback);


    }

    @Override
    public void onBindViewHolder(BusBoardingLocationHolder holder, final int position) {
        BusBoardingPointLocation busBoardingPointLocation = mBusBoardingPointLocation.get(position);
        holder.mTextViewboardinglocation.setText(busBoardingPointLocation.getmName());
        holder.mTextViewboardingtime.setText(busBoardingPointLocation.getmTime());
    }


    @Override
    public int getItemCount() {
        return mBusBoardingPointLocation.size();
    }


}







