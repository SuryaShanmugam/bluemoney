package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.MyAddressCallback;
import in.billiontags.bluemoney.holder.AddAddressHolder;
import in.billiontags.bluemoney.model.MyaddressList;


public class MyAddressAdapter extends RecyclerView.Adapter<AddAddressHolder> {

    protected MyPreference myPreference;
    private Context mContext;
    private ArrayList<MyaddressList> mMyaddressListList;
    private MyAddressCallback mCallback;

    public MyAddressAdapter(Context context, ArrayList<MyaddressList> myPhotoList, MyAddressCallback callback) {
        mContext = context;
        mMyaddressListList = myPhotoList;

        myPreference = new MyPreference(mContext);
        mCallback = callback;
    }

    @Override
    public AddAddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddAddressHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_address, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(AddAddressHolder holder, final int position) {
        MyaddressList myaddressList = mMyaddressListList.get(position);
        holder.mTextViewAddress.setText(String.format("%s, %s,\n%s, %s, %s,%s",
                myaddressList.getmAddress1(), myaddressList.getmAddress2(),
                myaddressList.getmCity(), myaddressList.getmState(),
                myaddressList.getmZipCode(), myaddressList.getmCountry()));

        holder.mTextViewMobile.setText(String.format("Contact No: %s", myaddressList.getmMobileNumber()));

    }

    @Override
    public int getItemCount() {
        return mMyaddressListList.size();
    }


}







