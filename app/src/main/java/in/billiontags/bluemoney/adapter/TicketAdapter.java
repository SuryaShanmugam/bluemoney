package in.billiontags.bluemoney.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.TicketCallback;
import in.billiontags.bluemoney.holder.TicketHolder;
import in.billiontags.bluemoney.model.Ticket;

/**
 * Created by Bobby on 2/2/2018
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketHolder> {

    private List<Ticket> mTicketList;
    private TicketCallback mCallback;
    private int mColorNormal, mColorSelected;
    public TicketAdapter(Context context, List<Ticket> ticketList, TicketCallback callback) {
        mTicketList = ticketList;
        mCallback = callback;
        mColorNormal = ContextCompat.getColor(context, R.color.secondary_txt);
        mColorSelected = ContextCompat.getColor(context, R.color.primary);
    }

    @Override
    public TicketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TicketHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ticket, parent, false), mCallback);
    }

    @Override
    public void onBindViewHolder(TicketHolder holder, int position) {
        Ticket ticket = mTicketList.get(position);
        holder.mTextViewTicket.setText(String.valueOf(ticket.getNo()));
        holder.mTextViewTicket.setTextColor(ticket.isSelected() ? mColorSelected : mColorNormal);
    }

    @Override
    public int getItemCount() {
        return mTicketList.size();
    }


}
