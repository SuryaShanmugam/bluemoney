package in.billiontags.bluemoney.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.model.BusStations;

public class BusStationAdapter extends ArrayAdapter<BusStations> implements Filterable {

    Context context;
    private int resource, textViewResourceId;
    private ArrayList<BusStations> items, tempItems, suggestions;
    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((BusStations) resultValue).getmStationName();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (BusStations busStations : tempItems) {
                    if (busStations.getmStationName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(busStations);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<BusStations> mbusStations = (ArrayList<BusStations>) results.values;
            if (results.count > 0) {
                clear();
                addAll(mbusStations);
                notifyDataSetChanged();
            }

        }
    };

    public BusStationAdapter(Context context, int resource, int textViewResourceId, ArrayList<BusStations> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<BusStations>(items); // this makes the difference.
        suggestions = new ArrayList<BusStations>();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater != null) {
                view = inflater.inflate(R.layout.item_busstations, parent, false);
            }
        }
        BusStations people = items.get(position);
        if (people != null) {
            TextView lblName = view.findViewById(R.id.txt_stations);
            if (lblName != null)
                lblName.setText(people.getmStationName());
        }
        return view;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }
}