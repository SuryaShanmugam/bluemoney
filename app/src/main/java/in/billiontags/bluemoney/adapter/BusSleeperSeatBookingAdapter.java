package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusSeatBookingCallback;
import in.billiontags.bluemoney.holder.BusSeatBookingHolder;
import in.billiontags.bluemoney.model.BusLowerDesk;


public class BusSleeperSeatBookingAdapter extends RecyclerView.Adapter<BusSeatBookingHolder> {

    private Context mContext;
    private ArrayList<BusLowerDesk> mBusLowerDesk;
    private BusSeatBookingCallback mCallback;


    public BusSleeperSeatBookingAdapter(Context context, ArrayList<BusLowerDesk> myPhotoList, BusSeatBookingCallback callback) {
        mContext = context;
        mBusLowerDesk = myPhotoList;
        mCallback = callback;

    }

    @NonNull
    @Override
    public BusSeatBookingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BusSeatBookingHolder
                (LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bus_sleeper, parent,
                        false), mCallback);
    }

    @Override
    public int getItemViewType(int position) {
        return mBusLowerDesk.size();

    }

    @Override
    public void onBindViewHolder(@NonNull BusSeatBookingHolder holder, final int position) {
        BusLowerDesk busSearchResults = mBusLowerDesk.get(position);

        if (busSearchResults.getmSeatType().equals("NA"))
        {
            holder.mBusSeatUnAvailableImage.setVisibility(View.INVISIBLE);
            holder.mBusSeatAvailable.setVisibility(View.GONE);
            holder.mBusSeatSelectedImage.setVisibility(View.GONE);
            holder.mBusSeatFemale.setVisibility(View.GONE);
            holder.mBusSeatGents.setVisibility(View.GONE);
        } else {
            switch (busSearchResults.getmSeatStatus()) {
                case "B":
                    holder.mBusSeatUnAvailableImage.setVisibility(View.VISIBLE);
                    holder.mBusSeatAvailable.setVisibility(View.GONE);
                    holder.mBusSeatSelectedImage.setVisibility(View.GONE);
                    holder.mBusSeatFemale.setVisibility(View.GONE);
                    holder.mBusSeatGents.setVisibility(View.GONE);
                    break;
                case "A":
                    holder.mBusSeatUnAvailableImage.setVisibility(View.GONE);
                    holder.mBusSeatAvailable.setVisibility(View.VISIBLE);
                    holder.mBusSeatSelectedImage.setVisibility(View.GONE);
                    holder.mBusSeatFemale.setVisibility(View.GONE);
                    holder.mBusSeatGents.setVisibility(View.GONE);
                    break;
                case "M":
                    holder.mBusSeatUnAvailableImage.setVisibility(View.GONE);
                    holder.mBusSeatAvailable.setVisibility(View.GONE);
                    holder.mBusSeatSelectedImage.setVisibility(View.GONE);
                    holder.mBusSeatFemale.setVisibility(View.GONE);
                    holder.mBusSeatGents.setVisibility(View.VISIBLE);
                    break;
                case "F":
                    holder.mBusSeatUnAvailableImage.setVisibility(View.GONE);
                    holder.mBusSeatAvailable.setVisibility(View.GONE);
                    holder.mBusSeatSelectedImage.setVisibility(View.GONE);
                    holder.mBusSeatFemale.setVisibility(View.VISIBLE);
                    holder.mBusSeatGents.setVisibility(View.GONE);
                    break;
                case "S":
                    holder.mBusSeatUnAvailableImage.setVisibility(View.GONE);
                    holder.mBusSeatAvailable.setVisibility(View.GONE);
                    holder.mBusSeatSelectedImage.setVisibility(View.VISIBLE);
                    holder.mBusSeatFemale.setVisibility(View.GONE);
                    holder.mBusSeatGents.setVisibility(View.GONE);
                    break;

            }
        }
    }


    @Override
    public int getItemCount() {
        return mBusLowerDesk.size();
    }


}







