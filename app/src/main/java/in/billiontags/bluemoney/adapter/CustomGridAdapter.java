package in.billiontags.bluemoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.model.BusLowerDesk;

/**
 * Created by admin on 3/16/2018.
 */

public class CustomGridAdapter extends BaseAdapter {

    LayoutInflater inflater;
    private Context context;
    private ArrayList<BusLowerDesk> mColumList1 = new ArrayList<>();
    private ArrayList<BusLowerDesk> mColumList2 = new ArrayList<>();
    private ArrayList<BusLowerDesk> mColumList3 = new ArrayList<>();
    private ArrayList<BusLowerDesk> mColumList4 = new ArrayList<>();

    public CustomGridAdapter(Context context, ArrayList<BusLowerDesk> mColumList,
                             ArrayList<BusLowerDesk> mColumList1, ArrayList<BusLowerDesk> mColumList2,
                             ArrayList<BusLowerDesk> mColumList3) {
        this.context = context;
        this.mColumList1 = mColumList;
        this.mColumList2 = mColumList1;
        this.mColumList3 = mColumList2;
        this.mColumList4 = mColumList3;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.item_grid, null);
        }
        ImageView button = convertView.findViewById(R.id.img_seat);
        ImageView button1 = convertView.findViewById(R.id.img_seat2);
        ImageView button2 = convertView.findViewById(R.id.img_seat3);
        ImageView button3 = convertView.findViewById(R.id.img_seat4);

        if (mColumList1 != null) {
            button.setVisibility(View.VISIBLE);

        } else if (mColumList2 == null) {
            button1.setVisibility(View.INVISIBLE);
        } else if (mColumList3 != null) {
            button2.setVisibility(View.VISIBLE);
        } else if (mColumList4 != null) {
            button3.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return mColumList1.size();
    }

    @Override
    public Object getItem(int position) {
        return mColumList1.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

