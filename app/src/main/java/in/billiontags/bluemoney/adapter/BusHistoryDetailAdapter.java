package in.billiontags.bluemoney.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.BushistoryDetailCallback;
import in.billiontags.bluemoney.holder.BusHistoryDetailHolder;
import in.billiontags.bluemoney.model.PassengerDetails;


public class BusHistoryDetailAdapter extends RecyclerView.Adapter<BusHistoryDetailHolder> {

    protected MyPreference myPreference;
    private Context mContext;
    private ArrayList<PassengerDetails> mMyOrdersItemList;
    private BushistoryDetailCallback mCallback;
    private int mColorGreen, mColorRed;

    public BusHistoryDetailAdapter(Context context, ArrayList<PassengerDetails> myPhotoList, BushistoryDetailCallback callback) {
        mContext = context;
        mMyOrdersItemList = myPhotoList;

        myPreference = new MyPreference(mContext);
        mCallback = callback;

        mColorGreen = ContextCompat.getColor(mContext, R.color.green);
        mColorRed = ContextCompat.getColor(mContext, R.color.red);
    }

    @Override
    public BusHistoryDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BusHistoryDetailHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bushistory, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(BusHistoryDetailHolder holder, final int position) {
        PassengerDetails myOrdersItem = mMyOrdersItemList.get(position);


    }

    @Override
    public int getItemCount() {
        return mMyOrdersItemList.size();
    }


}







