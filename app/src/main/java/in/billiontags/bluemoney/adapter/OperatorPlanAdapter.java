package in.billiontags.bluemoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.model.Operator;


public class OperatorPlanAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Operator> data;
    private LayoutInflater inflater;

    public OperatorPlanAdapter(Context context, ArrayList<Operator> arraylist1) {
        this.context = context;
        data = arraylist1;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;


        if (convertView == null) {
            // inflate the layout
            if (inflater != null) {
                convertView = inflater.inflate(R.layout.item_operatorname, parent, false);
            }
            // well set up the ViewHolder
            holder = new ViewHolder();
            if (convertView != null) {
                holder.mTextviewOperatorName = convertView.findViewById(R.id.txt_operator);
            }
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextviewOperatorName.setText(data.get(position).getmServiceName());
        return convertView;

    }


    private static class ViewHolder {
        TextView mTextviewOperatorName;

    }
}
