package in.billiontags.bluemoney;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import in.billiontags.bluemoney.adapter.BusAddPassengerDetailsAdapter;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.holder.BusAddPassengerDetailsHolder;
import in.billiontags.bluemoney.model.BusAddpassengerdetails;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.app.Constant.KEY_BUSTYPE;
import static in.billiontags.bluemoney.app.Constant.KEY_DESTINATIONNAME;
import static in.billiontags.bluemoney.app.Constant.KEY_OPERATORCODE;
import static in.billiontags.bluemoney.app.Constant.KEY_SCHEDULECODE;
import static in.billiontags.bluemoney.app.Constant.KEY_SEATFARE;
import static in.billiontags.bluemoney.app.Constant.KEY_SELECTEDBOARDINGLOCATION;
import static in.billiontags.bluemoney.app.Constant.KEY_SELECTEDDROPPINGLOCATION;
import static in.billiontags.bluemoney.app.Constant.KEY_SOURCENAME;
import static in.billiontags.bluemoney.app.Constant.KEY_TOTALFARE;
import static in.billiontags.bluemoney.app.Constant.KEY_TRAVELDATE;
import static in.billiontags.bluemoney.app.Constant.KEY_TRAVELSNAME;
import static in.billiontags.bluemoney.utils.Activity.launchWithBundle;

public class BusAddPassengerDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String KEY_ARRAY_PASSENGERDETAILS = "passengerDetails";
    protected String mSourceName, mDestinationName, mSechedulecode, mOperatorcode,
            mTravelername, mBusType, mTravelDate, mBoardinpointLocation, mDroppingPointLocation, mTotalFare;
    protected MyPreference mPreference;
    protected EditText mEditTextMobilenumber, mEditTextEmail;
    ArrayList<String> mSeatNo = new ArrayList<>();
    ArrayList<String> mTotalSeatFare = new ArrayList<>();
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<BusAddpassengerdetails> mMyordersItemList;
    private BusAddPassengerDetailsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private Button mContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_add_passenger_detail);
        processBundle();
        initObjects();
        initCallbacks();
        initRecyclerView();
    }

    private void initCallbacks() {
        mContinue.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void processBundle() {
        Intent i = getIntent();
        mMyordersItemList = new ArrayList<>();
        mSourceName = i.getStringExtra(KEY_SOURCENAME);
        mDestinationName = i.getStringExtra(KEY_DESTINATIONNAME);
        mSechedulecode = i.getStringExtra(KEY_SCHEDULECODE);
        mOperatorcode = i.getStringExtra(KEY_OPERATORCODE);
        mTravelername = i.getStringExtra(Constant.KEY_TRAVELSNAME);
        mSeatNo = i.getStringArrayListExtra(Constant.KEY_SEATNO);
        mTotalSeatFare = i.getStringArrayListExtra(KEY_SEATFARE);
        mBusType = i.getStringExtra(Constant.KEY_BUSTYPE);
        mTravelDate = i.getStringExtra(Constant.KEY_TRAVELDATE);
        mBoardinpointLocation = i.getStringExtra(Constant.KEY_SELECTEDBOARDINGLOCATION);
        mDroppingPointLocation = i.getStringExtra(KEY_SELECTEDDROPPINGLOCATION);
        mTotalFare = i.getStringExtra(Constant.KEY_TOTALFARE);
        for (int l = 0; l < mSeatNo.size(); l++) {

            mMyordersItemList.add(new BusAddpassengerdetails(mSeatNo.get(l), mTotalSeatFare.get(l)));


        }
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);


    }

    private void initObjects() {
        mContext = this;
        mRecyclerView = findViewById(R.id.buspassengerdetails);
        mContinue = findViewById(R.id.btn_Continue);
        mEditTextMobilenumber = findViewById(R.id.edit_mobilenumber);
        mEditTextEmail = findViewById(R.id.edit_email);

        mPreference = new MyPreference(mContext);
        mAdapter = new BusAddPassengerDetailsAdapter(mContext, mMyordersItemList);
        mLayoutManager = new LinearLayoutManager(mContext);

        mEditTextEmail.setText(mPreference.getEmail());
        mEditTextMobilenumber.setText(mPreference.getPhone());
    }

    @Override
    public void onClick(View v) {
        if (v == mContinue) {
            if (mRecyclerView.getChildCount() > 0) {
                for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
                    if (mRecyclerView.findViewHolderForLayoutPosition(i) instanceof BusAddPassengerDetailsHolder) {
                        BusAddPassengerDetailsHolder holder = (BusAddPassengerDetailsHolder) mRecyclerView.findViewHolderForLayoutPosition(i);
                        if (TextUtils.isEmpty(holder.mEditTextPassengerName.getText().toString())) {
                            holder.mEditTextPassengerName.requestFocus();
                            holder.mEditTextPassengerName.setError("Enter the Name");
                        } else if (TextUtils.isEmpty(holder.mEditTextPassengerAge.getText().toString())) {
                            holder.mEditTextPassengerAge.requestFocus();
                            holder.mEditTextPassengerAge.setError("Enter a Age");
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putString(KEY_TRAVELSNAME, mTravelername);
                            bundle.putString(KEY_BUSTYPE, mBusType);
                            bundle.putString(KEY_SOURCENAME, mSourceName);
                            bundle.putString(KEY_DESTINATIONNAME, mDestinationName);
                            bundle.putString(KEY_TRAVELDATE, mTravelDate);
                            bundle.putString(KEY_SELECTEDBOARDINGLOCATION, mBoardinpointLocation);
                            bundle.putString(KEY_SELECTEDDROPPINGLOCATION, mDroppingPointLocation);
                            bundle.putString(KEY_TRAVELDATE, mTravelDate);
                            bundle.putString(KEY_TOTALFARE, mTotalFare);
                            bundle.putParcelableArrayList(KEY_ARRAY_PASSENGERDETAILS, mMyordersItemList);
                            launchWithBundle(mContext, BusMakePaymentActivity.class, bundle);
                        }
                    }

                }

            }
        }
    }


}
