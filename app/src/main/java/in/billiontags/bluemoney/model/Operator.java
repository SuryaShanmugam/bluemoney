package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class Operator {


    @SerializedName("id")
    private int mId;
    @SerializedName("service_name")
    private String mServiceName;
    @SerializedName("service_key")
    private String mServiceKey;


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmServiceName() {
        return mServiceName;
    }

    public void setmServiceName(String mServiceName) {
        this.mServiceName = mServiceName;
    }

    public String getmServiceKey() {
        return mServiceKey;
    }

    public void setmServiceKey(String mServiceKey) {
        this.mServiceKey = mServiceKey;
    }
}
