package in.billiontags.bluemoney.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;


public class BusLowerDesk implements  Parcelable{


    @SerializedName("operatorServiceTax")
    private String mOperatorServiceTax;
    @SerializedName("row")
    private int mRow;
    @SerializedName("seatStatus")
    private String mSeatStatus;
    @SerializedName("seatType")
    private String mSeatType;
    @SerializedName("layer")
    private String mLayer;
    @SerializedName("seatFare")
    private double mSeatFare;
    @SerializedName("baseFare")
    private String mBaseFare;
    @SerializedName("column")
    private int mColumn;
    @SerializedName("seatNumber")
    private String mSeatNumber;
    @SerializedName("operatorServiceCharge")
    private String mOperatorServiceCharge;

    @SerializedName("seatIndex")
    private String mSeatIndex;


    public BusLowerDesk(Parcel in) {
        mOperatorServiceTax = in.readString();
        mRow = in.readInt();
        mSeatStatus = in.readString();
        mSeatType = in.readString();
        mLayer = in.readString();
        mSeatFare = in.readDouble();
        mBaseFare = in.readString();
        mColumn = in.readInt();
        mSeatNumber = in.readString();
        mOperatorServiceCharge = in.readString();
        mSeatIndex = in.readString();
    }

    public static final Creator<BusLowerDesk> CREATOR = new Creator<BusLowerDesk>() {
        @Override
        public BusLowerDesk createFromParcel(Parcel in) {
            return new BusLowerDesk(in);
        }

        @Override
        public BusLowerDesk[] newArray(int size) {
            return new BusLowerDesk[size];
        }
    };

    public BusLowerDesk() {

    }

    public String getmOperatorServiceTax() {
        return mOperatorServiceTax;
    }

    public void setmOperatorServiceTax(String mOperatorServiceTax) {
        this.mOperatorServiceTax = mOperatorServiceTax;
    }

    public int getmRow() {
        return mRow;
    }

    public void setmRow(int mRow) {
        this.mRow = mRow;
    }

    public String getmSeatStatus() {
        return mSeatStatus;
    }

    public void setmSeatStatus(String mSeatStatus) {
        this.mSeatStatus = mSeatStatus;
    }

    public String getmSeatType() {
        return mSeatType;
    }

    public void setmSeatType(String mSeatType) {
        this.mSeatType = mSeatType;
    }

    public String getmLayer() {
        return mLayer;
    }

    public void setmLayer(String mLayer) {
        this.mLayer = mLayer;
    }

    public double getmSeatFare() {
        return mSeatFare;
    }

    public void setmSeatFare(double mSeatFare) {
        this.mSeatFare = mSeatFare;
    }

    public String getmBaseFare() {
        return mBaseFare;
    }

    public void setmBaseFare(String mBaseFare) {
        this.mBaseFare = mBaseFare;
    }

    public int getmColumn() {
        return mColumn;
    }

    public void setmColumn(int mColumn) {
        this.mColumn = mColumn;
    }

    public String getmSeatNumber() {
        return mSeatNumber;
    }

    public void setmSeatNumber(String mSeatNumber) {
        this.mSeatNumber = mSeatNumber;
    }

    public String getmOperatorServiceCharge() {
        return mOperatorServiceCharge;
    }

    public void setmOperatorServiceCharge(String mOperatorServiceCharge) {
        this.mOperatorServiceCharge = mOperatorServiceCharge;
    }

    public String getmSeatIndex() {
        return mSeatIndex;
    }

    public void setmSeatIndex(String mSeatIndex) {
        this.mSeatIndex = mSeatIndex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mOperatorServiceTax);
        dest.writeInt(mRow);
        dest.writeString(mSeatStatus);
        dest.writeString(mSeatType);
        dest.writeString(mLayer);
        dest.writeDouble(mSeatFare);
        dest.writeString(mBaseFare);
        dest.writeInt(mColumn);
        dest.writeString(mSeatNumber);
        dest.writeString(mOperatorServiceCharge);
        dest.writeString(mSeatIndex);
    }



}
