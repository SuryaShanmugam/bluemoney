package in.billiontags.bluemoney.model;

/**
 * Created by Bobby on 2/2/2018
 */

public class Ticket {

    private int mNo;
    private boolean mSelected;

    public Ticket(int no, boolean selected) {
        mNo = no;
        mSelected = selected;
    }

    public int getNo() {
        return mNo;
    }

    public void setNo(int no) {
        mNo = no;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }
}
