package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 29/07/17
 */

public class User {


    @SerializedName("id")
    private int mId;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("type")
    private boolean mType;
    @SerializedName("role")
    private String mRole;
    @SerializedName("token")
    private String mToken;

    @SerializedName("userprofile")
    private UserProfile muserprofile;


    public User(String username, String email, String password, boolean type, String role) {
        mUsername = username;
        mEmail = email;
        mPassword = password;
        mType = type;
        mRole = role;

    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public boolean ismType() {
        return mType;
    }

    public void setmType(boolean mType) {
        this.mType = mType;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }

    public UserProfile getMuserprofile() {
        return muserprofile;
    }

    public void setMuserprofile(UserProfile muserprofile) {
        this.muserprofile = muserprofile;
    }

    public class UserProfile {
        @SerializedName("id")
        private int mId;
        @SerializedName("profile_pic")
        private String mProfilepic;
        @SerializedName("dob")
        private String mDob;
        @SerializedName("gender")
        private String mGender;


        public int getmId() {
            return mId;
        }

        public void setmId(int mId) {
            this.mId = mId;
        }

        public String getmProfilepic() {
            return mProfilepic;
        }

        public void setmProfilepic(String mProfilepic) {
            this.mProfilepic = mProfilepic;
        }

        public String getmDob() {
            return mDob;
        }

        public void setmDob(String mDob) {
            this.mDob = mDob;
        }

        public String getmGender() {
            return mGender;
        }

        public void setmGender(String mGender) {
            this.mGender = mGender;
        }
    }
}
