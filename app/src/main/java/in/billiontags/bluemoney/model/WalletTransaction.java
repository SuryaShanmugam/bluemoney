package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

import static in.billiontags.bluemoney.utils.Utils.getParsedDate;


public class WalletTransaction {


    @SerializedName("id")
    private int mId;
    @SerializedName("reward_unit")
    private String mRewardUnit;
    @SerializedName("amount")
    private int mAmount;
    @SerializedName("is_debit")
    private boolean misDebit;
    @SerializedName("created_on")
    private String mCreatedOn;
    @SerializedName("balance")
    private String mBalance;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmRewardUnit() {
        return mRewardUnit;
    }

    public void setmRewardUnit(String mRewardUnit) {
        this.mRewardUnit = mRewardUnit;
    }

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public boolean isMisDebit() {
        return misDebit;
    }

    public void setMisDebit(boolean misDebit) {
        this.misDebit = misDebit;
    }

    public String getmCreatedOn() {
        return getParsedDate(mCreatedOn);
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }

    public String getmBalance() {
        return mBalance;
    }

    public void setmBalance(String mBalance) {
        this.mBalance = mBalance;
    }
}

