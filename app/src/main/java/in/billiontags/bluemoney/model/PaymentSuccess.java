package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 09/08/17
 */

public class PaymentSuccess {

    @SerializedName("razorpay_order_id")
    private String mRazorpayOrderId;
    @SerializedName("razorpay_payment_id")
    private String mRazorpayPaymentId;
    @SerializedName("razorpay_signature")
    private String mRazorpaySignature;
    @SerializedName("type")
    private String mType;

    public PaymentSuccess(String razorpayOrderId, String razorpayPaymentId, String razorpaySignature, String type) {
        mRazorpayOrderId = razorpayOrderId;
        mRazorpayPaymentId = razorpayPaymentId;
        mRazorpaySignature = razorpaySignature;
        mType = type;
    }

    public String getmRazorpayOrderId() {
        return mRazorpayOrderId;
    }

    public void setmRazorpayOrderId(String mRazorpayOrderId) {
        this.mRazorpayOrderId = mRazorpayOrderId;
    }

    public String getmRazorpayPaymentId() {
        return mRazorpayPaymentId;
    }

    public void setmRazorpayPaymentId(String mRazorpayPaymentId) {
        this.mRazorpayPaymentId = mRazorpayPaymentId;
    }

    public String getmRazorpaySignature() {
        return mRazorpaySignature;
    }

    public void setmRazorpaySignature(String mRazorpaySignature) {
        this.mRazorpaySignature = mRazorpaySignature;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public void setRazorpaySignature(String razorpaySignature) {
        mRazorpaySignature = razorpaySignature;


    }
}
