package in.billiontags.bluemoney.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 2/13/2018.
 */

public class BusAddpassengerdetails  implements Parcelable{

    public static final Creator<BusAddpassengerdetails> CREATOR = new Creator<BusAddpassengerdetails>() {
        @Override
        public BusAddpassengerdetails createFromParcel(Parcel in) {
            return new BusAddpassengerdetails(in);
        }

        @Override
        public BusAddpassengerdetails[] newArray(int size) {
            return new BusAddpassengerdetails[size];
        }
    };
    private String mSeatNo;
    private String Amount;
    private String mPassengername;
    private String mPassengerage;
    private String gender;


    public BusAddpassengerdetails(String seatno, String amount) {
        mSeatNo = seatno;
        Amount = amount;
    }

    protected BusAddpassengerdetails(Parcel in) {
        mSeatNo = in.readString();
        Amount = in.readString();
        mPassengername = in.readString();
        mPassengerage = in.readString();
        gender = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mSeatNo);
        dest.writeString(Amount);
        dest.writeString(mPassengername);
        dest.writeString(mPassengerage);
        dest.writeString(gender);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getmSeatNo() {
        return mSeatNo;
    }

    public void setmSeatNo(String mSeatNo) {
        this.mSeatNo = mSeatNo;
    }

    public String getmPassengername() {
        return mPassengername;
    }

    public void setmPassengername(String mPassengername) {
        this.mPassengername = mPassengername;
    }

    public String getmPassengerage() {
        return mPassengerage;
    }

    public void setmPassengerage(String mPassengerage) {
        this.mPassengerage = mPassengerage;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
