package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/3/2018.
 */

public class ValidateRechargeData {

    @SerializedName("account")
    private String mAccountNumber;
    @SerializedName("service_key")
    private String mServiceKey;
    @SerializedName("amount")
    private String mAmount;
    @SerializedName("ipay_errordesc")
    private String mIpayErrorDesc;
    @SerializedName("ipay_errorcode")
    private String mIpayErrorCode;
    @SerializedName("particulars")
    private String mParticulars;

    public ValidateRechargeData(String account, String servicekey, String amount) {

        mAccountNumber = account;
        mServiceKey = servicekey;
        mAmount = amount;
    }


    public String getmAccountNumber() {
        return mAccountNumber;
    }

    public void setmAccountNumber(String mAccountNumber) {
        this.mAccountNumber = mAccountNumber;
    }

    public String getmServiceKey() {
        return mServiceKey;
    }

    public void setmServiceKey(String mServiceKey) {
        this.mServiceKey = mServiceKey;
    }

    public String getmAmount() {
        return mAmount;
    }

    public void setmAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public String getmIpayErrorDesc() {
        return mIpayErrorDesc;
    }

    public void setmIpayErrorDesc(String mIpayErrorDesc) {
        this.mIpayErrorDesc = mIpayErrorDesc;
    }

    public String getmIpayErrorCode() {
        return mIpayErrorCode;
    }

    public void setmIpayErrorCode(String mIpayErrorCode) {
        this.mIpayErrorCode = mIpayErrorCode;
    }

    public String getmParticulars() {
        return mParticulars;
    }

    public void setmParticulars(String mParticulars) {
        this.mParticulars = mParticulars;
    }
}
