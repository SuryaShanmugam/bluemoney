package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class ResendPojo {


    @SerializedName("username")
    private String mUsername;


    public ResendPojo(String username) {

        mUsername = username;

    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }
}
