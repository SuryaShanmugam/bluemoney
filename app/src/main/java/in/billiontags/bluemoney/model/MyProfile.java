package in.billiontags.bluemoney.model;


import com.google.gson.annotations.SerializedName;

public class MyProfile {

    @SerializedName("id")
    private int mId;
    @SerializedName("first_name")
    private String mFirstName;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("userprofile")
    private UserProfileDeatils userprofile;
    @SerializedName("recharge_count")
    private String mRechargeCount;
    @SerializedName("hotel_count")
    private String mHotelCount;
    @SerializedName("bus_count")
    private String mBusCount;
    @SerializedName("flight_count")
    private String FlightCount;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public UserProfileDeatils getUserprofile() {
        return userprofile;
    }

    public void setUserprofile(UserProfileDeatils userprofile) {
        this.userprofile = userprofile;
    }

    public String getmRechargeCount() {
        return mRechargeCount;
    }

    public void setmRechargeCount(String mRechargeCount) {
        this.mRechargeCount = mRechargeCount;
    }

    public String getmHotelCount() {
        return mHotelCount;
    }

    public void setmHotelCount(String mHotelCount) {
        this.mHotelCount = mHotelCount;
    }

    public String getmBusCount() {
        return mBusCount;
    }

    public void setmBusCount(String mBusCount) {
        this.mBusCount = mBusCount;
    }

    public String getFlightCount() {
        return FlightCount;
    }

    public void setFlightCount(String flightCount) {
        FlightCount = flightCount;
    }

    public class UserProfileDeatils {
        @SerializedName("id")
        private int mId;
        @SerializedName("profile_pic")
        private String mProfilepic;
        @SerializedName("dob")
        private String mDob;
        @SerializedName("gender")
        private String mGender;


        public int getmId() {
            return mId;
        }

        public void setmId(int mId) {
            this.mId = mId;
        }

        public String getmProfilepic() {
            return mProfilepic;
        }

        public void setmProfilepic(String mProfilepic) {
            this.mProfilepic = mProfilepic;
        }

        public String getmDob() {
            return mDob;
        }

        public void setmDob(String mDob) {
            this.mDob = mDob;
        }

        public String getmGender() {
            return mGender;
        }

        public void setmGender(String mGender) {
            this.mGender = mGender;
        }
    }
}
