package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/24/2018.
 */

public class BusBoardingPointDetails {

    @SerializedName("time")
    private String mBoardingTime;
    @SerializedName("name")
    private String mBoardingPoint;
    @SerializedName("code")
    private String mCode;


    public String getmBoardingTime() {
        return mBoardingTime;
    }

    public void setmBoardingTime(String mBoardingTime) {
        this.mBoardingTime = mBoardingTime;
    }

    public String getmBoardingPoint() {
        return mBoardingPoint;
    }

    public void setmBoardingPoint(String mBoardingPoint) {
        this.mBoardingPoint = mBoardingPoint;
    }

    public String getmCode() {
        return mCode;
    }

    public void setmCode(String mCode) {
        this.mCode = mCode;
    }
}
