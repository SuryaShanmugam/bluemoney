package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/24/2018.
 */

public class BusTravelsSearch {

    @SerializedName("travelDate")
    private String mTravelDate;
    @SerializedName("fromStationCode")
    private String mFromStationCode;
    @SerializedName("toStationCode")
    private String mToStationCode;


    public BusTravelsSearch(String traveldate, String fromstation, String tostation) {
        mTravelDate = traveldate;
        mFromStationCode = fromstation;
        mToStationCode = tostation;

    }

    public String getmTravelDate() {
        return mTravelDate;
    }

    public void setmTravelDate(String mTravelDate) {
        this.mTravelDate = mTravelDate;
    }

    public String getmFromStationCode() {
        return mFromStationCode;
    }

    public void setmFromStationCode(String mFromStationCode) {
        this.mFromStationCode = mFromStationCode;
    }

    public String getmToStationCode() {
        return mToStationCode;
    }

    public void setmToStationCode(String mToStationCode) {
        this.mToStationCode = mToStationCode;
    }
}
