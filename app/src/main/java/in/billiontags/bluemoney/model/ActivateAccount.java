package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

public class ActivateAccount {


    @SerializedName("mobile_number")
    private String mMobileNumber;
    @SerializedName("otp")
    private String mOtp;
    @SerializedName("client")
    private String mClient;
    @SerializedName("role")
    private String mRole;

    public ActivateAccount(String mobileNumber, String otp, String client, String role) {
        mMobileNumber = mobileNumber;
        mOtp = otp;
        mClient = client;
        mRole = role;
    }

    public String getmMobileNumber() {
        return mMobileNumber;
    }

    public void setmMobileNumber(String mMobileNumber) {
        this.mMobileNumber = mMobileNumber;
    }

    public String getmOtp() {
        return mOtp;
    }

    public void setmOtp(String mOtp) {
        this.mOtp = mOtp;
    }

    public String getmClient() {
        return mClient;
    }

    public void setmClient(String mClient) {
        this.mClient = mClient;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }


}
