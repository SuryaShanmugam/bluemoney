package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ServiceOperatorList {

    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mOperatorType;
    @SerializedName("category")
    private ArrayList<Operator> Operators = new ArrayList<>();

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmOperatorType() {
        return mOperatorType;
    }

    public void setmOperatorType(String mOperatorType) {
        this.mOperatorType = mOperatorType;
    }

    public ArrayList<Operator> getOperators() {
        return Operators;
    }

    public void setOperators(ArrayList<Operator> operators) {
        Operators = operators;
    }
}
