package in.billiontags.bluemoney.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


public class BusBoardingPointLocation implements Parcelable {

    public static final Creator<BusBoardingPointLocation> CREATOR = new Creator<BusBoardingPointLocation>() {
        @Override
        public BusBoardingPointLocation createFromParcel(Parcel in) {
            return new BusBoardingPointLocation(in);
        }

        @Override
        public BusBoardingPointLocation[] newArray(int size) {
            return new BusBoardingPointLocation[size];
        }
    };
    @SerializedName("landMark")
    private String mLandmark;
    @SerializedName("address")
    private String mAddress;
    @SerializedName("contactNumber")
    private String mContactNumber;
    @SerializedName("time")
    private String mTime;
    @SerializedName("code")
    private String mCode;
    @SerializedName("name")
    private String mName;

    public BusBoardingPointLocation(Parcel in) {
        mLandmark = in.readString();
        mAddress = in.readString();
        mContactNumber = in.readString();
        mTime = in.readString();
        mCode = in.readString();
        mName = in.readString();
    }

    public BusBoardingPointLocation(String name) {
        this.mName = name;
    }

    public String getmLandmark() {
        return mLandmark;
    }

    public void setmLandmark(String mLandmark) {
        this.mLandmark = mLandmark;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmContactNumber() {
        return mContactNumber;
    }

    public void setmContactNumber(String mContactNumber) {
        this.mContactNumber = mContactNumber;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getmCode() {
        return mCode;
    }

    public void setmCode(String mCode) {
        this.mCode = mCode;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mLandmark);
        dest.writeString(mAddress);
        dest.writeString(mContactNumber);
        dest.writeString(mTime);
        dest.writeString(mCode);
        dest.writeString(mName);
    }
}
