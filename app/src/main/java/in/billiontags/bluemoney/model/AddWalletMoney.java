package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class AddWalletMoney {

    @SerializedName("amount")
    private String mAmount;

    @SerializedName("order_id")
    private String mOrderId;

    public AddWalletMoney(String amount) {
        mAmount = amount;

    }


    public String getmAmount() {
        return mAmount;
    }

    public void setmAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public String getmOrderId() {
        return mOrderId;
    }

    public void setmOrderId(String mOrderId) {
        this.mOrderId = mOrderId;
    }
}
