package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class LoginPojo {


    @SerializedName("username")
    private String mUsername;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("client")
    private String mClient;
    @SerializedName("role")
    private String mRole;

    public LoginPojo(String username, String password, String client, String role) {
        mUsername = username;
        mPassword = password;
        mClient = client;
        mRole = role;
    }


    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmClient() {
        return mClient;
    }

    public void setmClient(String mClient) {
        this.mClient = mClient;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }
}
