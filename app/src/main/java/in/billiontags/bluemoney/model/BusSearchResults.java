package in.billiontags.bluemoney.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

public class BusSearchResults {


    @SerializedName("availableSeats")
    private String mAvailableSeats;
    @SerializedName("arrivalTime")
    private String mArrivalTime;
    @SerializedName("operatorName")
    private String mOperatorName;
    @SerializedName("serviceNumber")
    private String mServiceNumber;
    @SerializedName("journeyTime")
    private String mJourneyTime;
    @SerializedName("acBus")
    private String mAcBus;
    @SerializedName("scheduleCode")
    private String mScheduleCode;
    @SerializedName("operatorServiceTax")
    private String mOperatorServiceYack;

    @SerializedName("operatorDetails")
    private OperatorDetails mOperatorDetails;
    @SerializedName("ratings")
    private Ratings ratings;
    @SerializedName("operatorCode")
    private String mOpertorCode;
    @SerializedName("ticketFare")
    private JsonElement mTicketFare;
    @SerializedName("departureTime")
    private String mDepartureTime;
    @SerializedName("busType")
    private String mBustype;

    @SerializedName("cancellationTerms")
    private JsonElement mCancellationTerms;

    private String mTravelDate;

    public JsonElement getmCancellationTerms() {
        return mCancellationTerms;
    }

    public void setmCancellationTerms(JsonElement mCancellationTerms) {
        this.mCancellationTerms = mCancellationTerms;
    }

    public String getmTravelDate() {
        return mTravelDate;
    }

    public void setmTravelDate(String mTravelDate) {
        this.mTravelDate = mTravelDate;
    }

    public String getmAvailableSeats() {
        return mAvailableSeats;
    }

    public void setmAvailableSeats(String mAvailableSeats) {
        this.mAvailableSeats = mAvailableSeats;
    }


    public String getmArrivalTime() {
        return mArrivalTime;
    }

    public void setmArrivalTime(String mArrivalTime) {
        this.mArrivalTime = mArrivalTime;
    }

    public String getmOperatorName() {
        return mOperatorName;
    }

    public void setmOperatorName(String mOperatorName) {
        this.mOperatorName = mOperatorName;
    }

    public String getmServiceNumber() {
        return mServiceNumber;
    }

    public void setmServiceNumber(String mServiceNumber) {
        this.mServiceNumber = mServiceNumber;
    }

    public String getmJourneyTime() {
        return mJourneyTime;
    }

    public void setmJourneyTime(String mJourneyTime) {
        this.mJourneyTime = mJourneyTime;
    }

    public String getmAcBus() {
        return mAcBus;
    }

    public void setmAcBus(String mAcBus) {
        this.mAcBus = mAcBus;
    }

    public String getmScheduleCode() {
        return mScheduleCode;
    }

    public void setmScheduleCode(String mScheduleCode) {
        this.mScheduleCode = mScheduleCode;
    }

    public String getmOperatorServiceYack() {
        return mOperatorServiceYack;
    }

    public void setmOperatorServiceYack(String mOperatorServiceYack) {
        this.mOperatorServiceYack = mOperatorServiceYack;
    }


    public OperatorDetails getmOperatorDetails() {
        return mOperatorDetails;
    }

    public void setmOperatorDetails(OperatorDetails mOperatorDetails) {
        this.mOperatorDetails = mOperatorDetails;
    }

    public Ratings getRatings() {
        return ratings;
    }

    public void setRatings(Ratings ratings) {
        this.ratings = ratings;
    }

    public String getmOpertorCode() {
        return mOpertorCode;
    }

    public void setmOpertorCode(String mOpertorCode) {
        this.mOpertorCode = mOpertorCode;
    }

    public JsonElement getmTicketFare() {
        return mTicketFare;
    }

    public void setmTicketFare(JsonElement mTicketFare) {
        this.mTicketFare = mTicketFare;
    }

    public String getmDepartureTime() {
        return mDepartureTime;
    }

    public void setmDepartureTime(String mDepartureTime) {
        this.mDepartureTime = mDepartureTime;
    }

    public String getmBustype() {
        return mBustype;
    }

    public void setmBustype(String mBustype) {
        this.mBustype = mBustype;
    }

    public class OperatorDetails {
        @SerializedName("maxSeat")
        private String mMaxSeats;
        @SerializedName("alternateMobileRequired")
        private String mAlternateMobileRequired;
        @SerializedName("mobileTicketAllowed")
        private String mMobileTickeAllowed;
        @SerializedName("addressRequired")
        private String mAddressRequired;
        @SerializedName("passengerDetails")
        private String mPassengerDetails;
        @SerializedName("idProofRequired")
        private String mIdProofRequired;
        @SerializedName("partialCancelAllowed")
        private String mPartialCancelAllowed;


        public String getmMaxSeats() {
            return mMaxSeats;
        }

        public void setmMaxSeats(String mMaxSeats) {
            this.mMaxSeats = mMaxSeats;
        }

        public String getmAlternateMobileRequired() {
            return mAlternateMobileRequired;
        }

        public void setmAlternateMobileRequired(String mAlternateMobileRequired) {
            this.mAlternateMobileRequired = mAlternateMobileRequired;
        }

        public String getmMobileTickeAllowed() {
            return mMobileTickeAllowed;
        }

        public void setmMobileTickeAllowed(String mMobileTickeAllowed) {
            this.mMobileTickeAllowed = mMobileTickeAllowed;
        }

        public String getmAddressRequired() {
            return mAddressRequired;
        }

        public void setmAddressRequired(String mAddressRequired) {
            this.mAddressRequired = mAddressRequired;
        }

        public String getmPassengerDetails() {
            return mPassengerDetails;
        }

        public void setmPassengerDetails(String mPassengerDetails) {
            this.mPassengerDetails = mPassengerDetails;
        }

        public String getmIdProofRequired() {
            return mIdProofRequired;
        }

        public void setmIdProofRequired(String mIdProofRequired) {
            this.mIdProofRequired = mIdProofRequired;
        }

        public String getmPartialCancelAllowed() {
            return mPartialCancelAllowed;
        }

        public void setmPartialCancelAllowed(String mPartialCancelAllowed) {
            this.mPartialCancelAllowed = mPartialCancelAllowed;
        }
    }

    public class Ratings {
        @SerializedName("punctuality")
        private String mPunctuality;
        @SerializedName("staffBehavior")
        private String mStaffBehaviour;
        @SerializedName("busQuality")
        private String mBusQuality;

        public String getmPunctuality() {
            return mPunctuality;
        }

        public void setmPunctuality(String mPunctuality) {
            this.mPunctuality = mPunctuality;
        }

        public String getmStaffBehaviour() {
            return mStaffBehaviour;
        }

        public void setmStaffBehaviour(String mStaffBehaviour) {
            this.mStaffBehaviour = mStaffBehaviour;
        }

        public String getmBusQuality() {
            return mBusQuality;
        }

        public void setmBusQuality(String mBusQuality) {
            this.mBusQuality = mBusQuality;
        }
    }
}

