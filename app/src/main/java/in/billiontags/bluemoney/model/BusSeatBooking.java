package in.billiontags.bluemoney.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class BusSeatBooking {

    @SerializedName("code")
    private String mCode;
    @SerializedName("rowsList")
    private JsonElement mRowslist;
    @SerializedName("columnsList")

    private JsonElement mColumnList;

    @SerializedName("busMapStructure")
    private BusMapStructure mbusMapStructure;

    @SerializedName("layerCount")
    private String mLayerCount;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("availableSeats")
    private String mAvailableSeats;
    @SerializedName("boardingPoints")
    private ArrayList<BusBoardingPointLocation> mBoardingPoints = new ArrayList<>();
    @SerializedName("droppingPoints")
    private ArrayList<BusBoardingPointLocation> mDroppingPoints = new ArrayList<>();

    public ArrayList<BusBoardingPointLocation> getmDroppingPoints() {
        return mDroppingPoints;
    }

    public void setmDroppingPoints(ArrayList<BusBoardingPointLocation> mDroppingPoints) {
        this.mDroppingPoints = mDroppingPoints;
    }

    public JsonElement getmRowslist() {
        return mRowslist;
    }

    public void setmRowslist(JsonElement mRowslist) {
        this.mRowslist = mRowslist;
    }

    public JsonElement getmColumnList() {
        return mColumnList;
    }

    public void setmColumnList(JsonElement mColumnList) {
        this.mColumnList = mColumnList;
    }

    public String getmCode() {
        return mCode;
    }

    public void setmCode(String mCode) {
        this.mCode = mCode;
    }
    public BusMapStructure getMbusMapStructure() {
        return mbusMapStructure;
    }

    public void setMbusMapStructure(BusMapStructure mbusMapStructure) {
        this.mbusMapStructure = mbusMapStructure;
    }


    public String getmLayerCount() {
        return mLayerCount;
    }

    public void setmLayerCount(String mLayerCount) {
        this.mLayerCount = mLayerCount;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmAvailableSeats() {
        return mAvailableSeats;
    }

    public void setmAvailableSeats(String mAvailableSeats) {
        this.mAvailableSeats = mAvailableSeats;
    }

    public ArrayList<BusBoardingPointLocation> getmBoardingPoints() {
        return mBoardingPoints;
    }

    public void setmBoardingPoints(ArrayList<BusBoardingPointLocation> mBoardingPoints) {
        this.mBoardingPoints = mBoardingPoints;
    }

    public class BusMapStructure {
        @SerializedName("lower")
        private ArrayList<BusLowerDesk> mLowerDeck = new ArrayList<>();
        @SerializedName("upper")
        private ArrayList<BusLowerDesk> mUpperDeck = new ArrayList<>();


        public ArrayList<BusLowerDesk> getmLowerDeck() {
            return mLowerDeck;
        }

        public void setmLowerDesk(ArrayList<BusLowerDesk> mLowerDeck) {
            this.mLowerDeck = mLowerDeck;
        }

        public ArrayList<BusLowerDesk> getmUpperDeck() {
            return mUpperDeck;
        }

        public void setmUpperDesk(ArrayList<BusLowerDesk> mUpperDesk) {
            this.mUpperDeck = mUpperDesk;
        }
    }
}
