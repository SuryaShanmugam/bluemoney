package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class MyOrdersHistory {

    @SerializedName("type")
    private String mType;

    @SerializedName("prev_url")
    private String mPrevUrl;
    @SerializedName("next_url")
    private String mNextUrl;
    @SerializedName("count")
    private int mCount;
    @SerializedName("page_size")
    private int mPageSize;
    @SerializedName("results")
    private ArrayList<MyOrdersItem> myOrdersItems = new ArrayList<>();


    public MyOrdersHistory(String type) {
        mType = type;
    }


    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmPrevUrl() {
        return mPrevUrl;
    }

    public void setmPrevUrl(String mPrevUrl) {
        this.mPrevUrl = mPrevUrl;
    }

    public String getmNextUrl() {
        return mNextUrl;
    }

    public void setmNextUrl(String mNextUrl) {
        this.mNextUrl = mNextUrl;
    }

    public int getmCount() {
        return mCount;
    }

    public void setmCount(int mCount) {
        this.mCount = mCount;
    }

    public int getmPageSize() {
        return mPageSize;
    }

    public void setmPageSize(int mPageSize) {
        this.mPageSize = mPageSize;
    }

    public ArrayList<MyOrdersItem> getMyOrdersItems() {
        return myOrdersItems;
    }

    public void setMyOrdersItems(ArrayList<MyOrdersItem> myOrdersItems) {
        this.myOrdersItems = myOrdersItems;
    }
}
