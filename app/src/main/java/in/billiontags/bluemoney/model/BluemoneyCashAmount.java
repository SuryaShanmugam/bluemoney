package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/6/2018.
 */

public class BluemoneyCashAmount {

    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("discount")
    private int mDiscount;
    @SerializedName("max_amount")
    private int mMaxAmount;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmDiscount() {
        return mDiscount;
    }

    public void setmDiscount(int mDiscount) {
        this.mDiscount = mDiscount;
    }

    public int getmMaxAmount() {
        return mMaxAmount;
    }

    public void setmMaxAmount(int mMaxAmount) {
        this.mMaxAmount = mMaxAmount;
    }
}
