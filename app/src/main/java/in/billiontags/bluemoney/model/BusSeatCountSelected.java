package in.billiontags.bluemoney.model;


public class BusSeatCountSelected {

    private String mSeatNumber;
    private int position;
    private double fare;

    public BusSeatCountSelected(double mfare, String SeatNumber, int pos) {
        position = pos;
        mSeatNumber = SeatNumber;
        fare = mfare;
    }

    public String getmSeatNumber() {
        return mSeatNumber;
    }

    public void setmSeatNumber(String mSeatNumber) {
        this.mSeatNumber = mSeatNumber;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public double getFare() {
        return fare;
    }

    public void setFare(double fare) {
        this.fare = fare;
    }
}
