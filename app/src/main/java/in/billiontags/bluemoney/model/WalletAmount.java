package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class WalletAmount {

    @SerializedName("id")
    private int mId;
    @SerializedName("user_cash")
    private int mUserCash;
    @SerializedName("blue_money_cash")
    private int mBluemoneyCash;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getmUserCash() {
        return mUserCash;
    }

    public void setmUserCash(int mUserCash) {
        this.mUserCash = mUserCash;
    }

    public int getmBluemoneyCash() {
        return mBluemoneyCash;
    }

    public void setmBluemoneyCash(int mBluemoneyCash) {
        this.mBluemoneyCash = mBluemoneyCash;
    }
}
