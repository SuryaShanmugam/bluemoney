package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class BusStations {

    @SerializedName("station_code")
    private String mStationCode;
    @SerializedName("station_name")
    private String mStationName;


    public String getmStationCode() {
        return mStationCode;
    }

    public void setmStationCode(String mStationCode) {
        this.mStationCode = mStationCode;
    }

    public String getmStationName() {
        return mStationName;
    }

    public void setmStationName(String mStationName) {
        this.mStationName = mStationName;
    }
}
