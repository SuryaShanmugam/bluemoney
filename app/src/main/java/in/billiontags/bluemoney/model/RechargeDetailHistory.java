package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

public class RechargeDetailHistory {

    @SerializedName("tagged_object")
    private TaggedObject tagged_object;
    @SerializedName("txn_id")
    private String mTxnId;
    @SerializedName("txn_amount")
    private int mTxnAmount;
    @SerializedName("pay_method")
    private String mPaymethod;
    @SerializedName("status")
    private boolean mStatus;
    @SerializedName("pay_bank")
    private String mPayBank;
    @SerializedName("type")
    private String mtype;


    public TaggedObject getTagged_object() {
        return tagged_object;
    }

    public void setTagged_object(TaggedObject tagged_object) {
        this.tagged_object = tagged_object;
    }

    public String getmTxnId() {
        return mTxnId;
    }

    public void setmTxnId(String mTxnId) {
        this.mTxnId = mTxnId;
    }

    public int getmTxnAmount() {
        return mTxnAmount;
    }

    public void setmTxnAmount(int mTxnAmount) {
        this.mTxnAmount = mTxnAmount;
    }

    public String getmPaymethod() {
        return mPaymethod;
    }

    public void setmPaymethod(String mPaymethod) {
        this.mPaymethod = mPaymethod;
    }

    public boolean ismStatus() {
        return mStatus;
    }

    public void setmStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }

    public String getmPayBank() {
        return mPayBank;
    }

    public void setmPayBank(String mPayBank) {
        this.mPayBank = mPayBank;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public class TaggedObject {
        @SerializedName("id")
        private int mId;
        @SerializedName("service_provider")
        private ServiceProvider mServiceProvider;
        @SerializedName("mobile_number")
        private String mMobilenumber;
        @SerializedName("trans_amt")
        private int mTrensferAmount;
        @SerializedName("status")
        private boolean mStatus;


        public int getmId() {
            return mId;
        }

        public void setmId(int mId) {
            this.mId = mId;
        }

        public ServiceProvider getmServiceProvider() {
            return mServiceProvider;
        }

        public void setmServiceProvider(ServiceProvider mServiceProvider) {
            this.mServiceProvider = mServiceProvider;
        }

        public String getmMobilenumber() {
            return mMobilenumber;
        }

        public void setmMobilenumber(String mMobilenumber) {
            this.mMobilenumber = mMobilenumber;
        }

        public int getmTrensferAmount() {
            return mTrensferAmount;
        }

        public void setmTrensferAmount(int mTrensferAmount) {
            this.mTrensferAmount = mTrensferAmount;
        }

        public boolean ismStatus() {
            return mStatus;
        }

        public void setmStatus(boolean mStatus) {
            this.mStatus = mStatus;
        }
    }


    public class ServiceProvider {
        @SerializedName("id")
        private int mId;
        @SerializedName("category")
        private String mCategory;
        @SerializedName("service_name")
        private String mServiceName;

        public int getmId() {
            return mId;
        }

        public void setmId(int mId) {
            this.mId = mId;
        }

        public String getmCategory() {
            return mCategory;
        }

        public void setmCategory(String mCategory) {
            this.mCategory = mCategory;
        }

        public String getmServiceName() {
            return mServiceName;
        }

        public void setmServiceName(String mServiceName) {
            this.mServiceName = mServiceName;
        }
    }
}
