package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 29/07/17
 */

public class ForgotPass {


    @SerializedName("id")
    private String mId;
    @SerializedName("mobile_number")
    private String mMobileNumber;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("role")
    private String mRole;

    @SerializedName("username")
    private String mUserName;

    public ForgotPass(String mobileNumber, String role) {
        mMobileNumber = mobileNumber;
        mRole = role;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmMobileNumber() {
        return mMobileNumber;
    }

    public void setmMobileNumber(String mMobileNumber) {
        this.mMobileNumber = mMobileNumber;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }


    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }
}
