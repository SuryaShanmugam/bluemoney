package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/8/2018.
 */

public class FindOperator {

    @SerializedName("operator_name")
    private String mOperatorName;
    @SerializedName("circle_name")
    private String mCircleName;
    @SerializedName("circle_code")
    private String mCircleCode;
    @SerializedName("operator_code")
    private String mOperatoCode;
    @SerializedName("mobile")
    private String mMobileNumber;


    public FindOperator(String mobileNumber) {
        mMobileNumber = mobileNumber;
    }


    public String getmMobileNumber() {
        return mMobileNumber;
    }

    public void setmMobileNumber(String mMobileNumber) {
        this.mMobileNumber = mMobileNumber;
    }

    public String getmOperatorName() {
        return mOperatorName;
    }

    public void setmOperatorName(String mOperatorName) {
        this.mOperatorName = mOperatorName;
    }

    public String getmCircleName() {
        return mCircleName;
    }

    public void setmCircleName(String mCircleName) {
        this.mCircleName = mCircleName;
    }

    public String getmCircleCode() {
        return mCircleCode;
    }

    public void setmCircleCode(String mCircleCode) {
        this.mCircleCode = mCircleCode;
    }

    public String getmOperatoCode() {
        return mOperatoCode;
    }

    public void setmOperatoCode(String mOperatoCode) {
        this.mOperatoCode = mOperatoCode;
    }
}
