package in.billiontags.bluemoney.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BusOperatorsList {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("fromStationCode")
    private String mFromStationcode;
    @SerializedName("searchResult")
    private ArrayList<BusSearchResults> mSearchResults = new ArrayList<>();

    @SerializedName("toStationCode")
    private String mToStationCode;
    @SerializedName("travelDate")
    private String mTravelDate;

    @SerializedName("code")
    private String mCode;


    public String getmToStationCode() {
        return mToStationCode;
    }

    public void setmToStationCode(String mToStationCode) {
        this.mToStationCode = mToStationCode;
    }

    public String getmTravelDate() {
        return mTravelDate;
    }

    public void setmTravelDate(String mTravelDate) {
        this.mTravelDate = mTravelDate;
    }

    public String getmCode() {
        return mCode;
    }

    public void setmCode(String mCode) {
        this.mCode = mCode;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmFromStationcode() {
        return mFromStationcode;
    }

    public void setmFromStationcode(String mFromStationcode) {
        this.mFromStationcode = mFromStationcode;
    }

    public ArrayList<BusSearchResults> getmSearchResults() {
        return mSearchResults;
    }

    public void setmSearchResults(ArrayList<BusSearchResults> mSearchResults) {
        this.mSearchResults = mSearchResults;
    }
}
