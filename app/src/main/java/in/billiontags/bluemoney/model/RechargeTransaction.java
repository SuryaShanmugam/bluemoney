package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/6/2018.
 */

public class RechargeTransaction {


    @SerializedName("service_provider")
    private String mServiceProvider;
    @SerializedName("mobile_number")
    private String mMobileNumber;
    @SerializedName("trans_amt")
    private String mTransmitAmount;
    @SerializedName("is_wallet")
    private boolean isWallet;
    @SerializedName("type")
    private String mRechargeType;

    @SerializedName("order_id")
    private String mOrderId;
    @SerializedName("amount")
    private int mAmount;

    public RechargeTransaction(String serviceprovider, String mobilenumber, String amount, boolean iswallet, String rechargetype) {

        mServiceProvider = serviceprovider;
        mMobileNumber = mobilenumber;
        mTransmitAmount = amount;
        isWallet = iswallet;
        mRechargeType = rechargetype;
    }


    public String getmServiceProvider() {
        return mServiceProvider;
    }

    public void setmServiceProvider(String mServiceProvider) {
        this.mServiceProvider = mServiceProvider;
    }

    public String getmMobileNumber() {
        return mMobileNumber;
    }

    public void setmMobileNumber(String mMobileNumber) {
        this.mMobileNumber = mMobileNumber;
    }

    public String getmTransmitAmount() {
        return mTransmitAmount;
    }

    public void setmTransmitAmount(String mTransmitAmount) {
        this.mTransmitAmount = mTransmitAmount;
    }

    public boolean isWallet() {
        return isWallet;
    }

    public void setWallet(boolean wallet) {
        isWallet = wallet;
    }

    public String getmRechargeType() {
        return mRechargeType;
    }

    public void setmRechargeType(String mRechargeType) {
        this.mRechargeType = mRechargeType;
    }

    public String getmOrderId() {
        return mOrderId;
    }

    public void setmOrderId(String mOrderId) {
        this.mOrderId = mOrderId;
    }

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }
}
