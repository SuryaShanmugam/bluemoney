package in.billiontags.bluemoney.model;


import com.google.gson.annotations.SerializedName;


public class BusSeatBookingProcess {

    @SerializedName("travelDate")
    private String mTravelDate;
    @SerializedName("fromStationCode")
    private String mFromStationCode;
    @SerializedName("toStationCode")
    private String mToStationCode;
    @SerializedName("operatorCode")
    private String mOperatorCode;

    @SerializedName("scheduleCode")
    private String mScheduleCode;


    public BusSeatBookingProcess(String traveldate, String fromstation, String tostation, String operatorcode, String schedulecode) {
        mTravelDate = traveldate;
        mFromStationCode = fromstation;
        mToStationCode = tostation;
        mOperatorCode = operatorcode;
        mScheduleCode = schedulecode;
    }


    public String getmTravelDate() {
        return mTravelDate;
    }

    public void setmTravelDate(String mTravelDate) {
        this.mTravelDate = mTravelDate;
    }

    public String getmFromStationCode() {
        return mFromStationCode;
    }

    public void setmFromStationCode(String mFromStationCode) {
        this.mFromStationCode = mFromStationCode;
    }

    public String getmToStationCode() {
        return mToStationCode;
    }

    public void setmToStationCode(String mToStationCode) {
        this.mToStationCode = mToStationCode;
    }

    public String getmOperatorCode() {
        return mOperatorCode;
    }

    public void setmOperatorCode(String mOperatorCode) {
        this.mOperatorCode = mOperatorCode;
    }

    public String getmScheduleCode() {
        return mScheduleCode;
    }

    public void setmScheduleCode(String mScheduleCode) {
        this.mScheduleCode = mScheduleCode;
    }
}
