package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

import static in.billiontags.bluemoney.utils.Utils.getParsedDate;


public class MyOrdersItem {

    @SerializedName("content_type")
    private String mContentType;

    @SerializedName("txn_amount")
    private int mTxnAmount;
    @SerializedName("id")
    private int mId;
    @SerializedName("created_on")
    private String mCreatedOn;
    @SerializedName("txn_id")
    private String mTxnId;
    @SerializedName("status")
    private boolean mStatus;
    @SerializedName("booking_status")
    private boolean mBookingStatus;


    public String getmContentType() {
        return mContentType;
    }

    public void setmContentType(String mContentType) {
        this.mContentType = mContentType;
    }

    public int getmTxnAmount() {
        return mTxnAmount;
    }

    public void setmTxnAmount(int mTxnAmount) {
        this.mTxnAmount = mTxnAmount;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmCreatedOn() {
        return getParsedDate(mCreatedOn);
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }

    public String getmTxnId() {
        return mTxnId;
    }

    public void setmTxnId(String mTxnId) {
        this.mTxnId = mTxnId;
    }

    public boolean ismStatus() {
        return mStatus;
    }

    public void setmStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }

    public boolean ismBookingStatus() {
        return mBookingStatus;
    }

    public void setmBookingStatus(boolean mBookingStatus) {
        this.mBookingStatus = mBookingStatus;
    }
}
