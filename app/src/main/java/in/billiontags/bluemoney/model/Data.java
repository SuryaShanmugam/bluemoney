package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 01/08/17
 */

public class Data {

    @SerializedName("data")
    private String mData;

    public Data(String data) {
        mData = data;
    }

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }
}
