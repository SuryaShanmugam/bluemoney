package in.billiontags.bluemoney.model;

/**
 * Created by Bobby on 2/2/2018
 */

public class BusType {

    private String mBusType;
    private boolean mSelected;

    public BusType(String busType, boolean selected) {
        mBusType = busType;
        mSelected = selected;
    }

    public String getBusType() {
        return mBusType;
    }

    public void setBusType(String busType) {
        mBusType = busType;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }
}
