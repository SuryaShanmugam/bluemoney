package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2/14/2018.
 */

public class PassengerDetails {


    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mPname;
    @SerializedName("age")
    private String mPage;
    @SerializedName("gender")
    private String mPgender;
    @SerializedName("amount")
    private int mAmount;
    @SerializedName("seatNumber")
    private String mSeatNumber;
    @SerializedName("seat_type")
    private String mSeatType;
    @SerializedName("canceldetails")
    private String mCanceldetails;
    @SerializedName("is_upper")
    private boolean isUpper;


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmPname() {
        return mPname;
    }

    public void setmPname(String mPname) {
        this.mPname = mPname;
    }

    public String getmPage() {
        return mPage;
    }

    public void setmPage(String mPage) {
        this.mPage = mPage;
    }

    public String getmPgender() {
        return mPgender;
    }

    public void setmPgender(String mPgender) {
        this.mPgender = mPgender;
    }

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public String getmSeatNumber() {
        return mSeatNumber;
    }

    public void setmSeatNumber(String mSeatNumber) {
        this.mSeatNumber = mSeatNumber;
    }

    public String getmSeatType() {
        return mSeatType;
    }

    public void setmSeatType(String mSeatType) {
        this.mSeatType = mSeatType;
    }

    public String getmCanceldetails() {
        return mCanceldetails;
    }

    public void setmCanceldetails(String mCanceldetails) {
        this.mCanceldetails = mCanceldetails;
    }

    public boolean isUpper() {
        return isUpper;
    }

    public void setUpper(boolean upper) {
        isUpper = upper;
    }
}
