package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;


public class PaymentResponse {


    @SerializedName("success")
    private int success;
    @SerializedName("error")
    private int error;
    @SerializedName("data")
    private Data data;
    @SerializedName("errors")
    private Errors errors;
    @SerializedName("message")
    private String message;
    @SerializedName("username")
    private String mUsername;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }


    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public class Data {
        @SerializedName("url")
        private String url;

        @SerializedName("post_data")
        private PostData post_data;


        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public PostData getPost_data() {
            return post_data;
        }

        public void setPost_data(PostData post_data) {
            this.post_data = post_data;
        }
    }


    public class PostData {


        @SerializedName("user_id")
        private String user_id;

        @SerializedName("applied_coupon_code")
        private String applied_coupon_code;
        @SerializedName("amount")
        private String amount;
        @SerializedName("merchant_param1")
        private String merchant_param1;
        @SerializedName("merchant_param2")
        private String merchant_param2;
        @SerializedName("merchant_param3")
        private String merchant_param3;
        @SerializedName("merchant_param4")
        private String merchant_param4;
        @SerializedName("wallet_amount")
        private String wallet_amount;


        @SerializedName("coupon_amount")
        private String coupon_amount;
        @SerializedName("cc_pay_amount")
        private String cc_pay_amount;
        @SerializedName("wallet_reduced")
        private Boolean wallet_reduced;


        @SerializedName("merchant_param5")
        private String merchant_param5;
        @SerializedName("billing_name")
        private String billing_name;
        @SerializedName("billing_email")
        private String billing_email;


        @SerializedName("billing_zip")
        private String billing_zip;
        @SerializedName("billing_country")
        private String billing_country;
        @SerializedName("billing_tel")
        private String billing_tel;

        @SerializedName("billing_address")
        private String billing_address;
        @SerializedName("billing_city")
        private String billing_city;
        @SerializedName("billing_state")
        private String billing_state;
        @SerializedName("tid")
        private String tid;
        @SerializedName("order_id")
        private String order_id;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getApplied_coupon_code() {
            return applied_coupon_code;
        }

        public void setApplied_coupon_code(String applied_coupon_code) {
            this.applied_coupon_code = applied_coupon_code;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getMerchant_param1() {
            return merchant_param1;
        }

        public void setMerchant_param1(String merchant_param1) {
            this.merchant_param1 = merchant_param1;
        }

        public String getMerchant_param2() {
            return merchant_param2;
        }

        public void setMerchant_param2(String merchant_param2) {
            this.merchant_param2 = merchant_param2;
        }

        public String getMerchant_param3() {
            return merchant_param3;
        }

        public void setMerchant_param3(String merchant_param3) {
            this.merchant_param3 = merchant_param3;
        }

        public String getMerchant_param4() {
            return merchant_param4;
        }

        public void setMerchant_param4(String merchant_param4) {
            this.merchant_param4 = merchant_param4;
        }

        public String getWallet_amount() {
            return wallet_amount;
        }

        public void setWallet_amount(String wallet_amount) {
            this.wallet_amount = wallet_amount;
        }

        public String getCoupon_amount() {
            return coupon_amount;
        }

        public void setCoupon_amount(String coupon_amount) {
            this.coupon_amount = coupon_amount;
        }

        public String getCc_pay_amount() {
            return cc_pay_amount;
        }

        public void setCc_pay_amount(String cc_pay_amount) {
            this.cc_pay_amount = cc_pay_amount;
        }

        public Boolean getWallet_reduced() {
            return wallet_reduced;
        }

        public void setWallet_reduced(Boolean wallet_reduced) {
            this.wallet_reduced = wallet_reduced;
        }

        public String getMerchant_param5() {
            return merchant_param5;
        }

        public void setMerchant_param5(String merchant_param5) {
            this.merchant_param5 = merchant_param5;
        }

        public String getBilling_name() {
            return billing_name;
        }

        public void setBilling_name(String billing_name) {
            this.billing_name = billing_name;
        }

        public String getBilling_email() {
            return billing_email;
        }

        public void setBilling_email(String billing_email) {
            this.billing_email = billing_email;
        }


        public String getBilling_zip() {
            return billing_zip;
        }

        public void setBilling_zip(String billing_zip) {
            this.billing_zip = billing_zip;
        }

        public String getBilling_country() {
            return billing_country;
        }

        public void setBilling_country(String billing_country) {
            this.billing_country = billing_country;
        }

        public String getBilling_tel() {
            return billing_tel;
        }

        public void setBilling_tel(String billing_tel) {
            this.billing_tel = billing_tel;
        }

        public String getBilling_address() {
            return billing_address;
        }

        public void setBilling_address(String billing_address) {
            this.billing_address = billing_address;
        }

        public String getBilling_city() {
            return billing_city;
        }

        public void setBilling_city(String billing_city) {
            this.billing_city = billing_city;
        }

        public String getBilling_state() {
            return billing_state;
        }

        public void setBilling_state(String billing_state) {
            this.billing_state = billing_state;
        }

        public String getTid() {
            return tid;
        }

        public void setTid(String tid) {
            this.tid = tid;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }
    }


    public class Errors {

    }
}
