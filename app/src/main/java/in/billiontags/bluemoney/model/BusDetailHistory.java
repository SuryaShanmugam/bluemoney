package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BusDetailHistory {

    @SerializedName("tagged_object")
    private TaggedObject tagged_object;
    @SerializedName("txn_id")
    private String mTxnId;
    @SerializedName("txn_amount")
    private int mTxnAmount;
    @SerializedName("pay_method")
    private String mPaymethod;
    @SerializedName("status")
    private boolean mStatus;
    @SerializedName("pay_bank")
    private String mPayBank;
    @SerializedName("type")
    private String mtype;


    public TaggedObject getTagged_object() {
        return tagged_object;
    }

    public void setTagged_object(TaggedObject tagged_object) {
        this.tagged_object = tagged_object;
    }

    public String getmTxnId() {
        return mTxnId;
    }

    public void setmTxnId(String mTxnId) {
        this.mTxnId = mTxnId;
    }

    public int getmTxnAmount() {
        return mTxnAmount;
    }

    public void setmTxnAmount(int mTxnAmount) {
        this.mTxnAmount = mTxnAmount;
    }

    public String getmPaymethod() {
        return mPaymethod;
    }

    public void setmPaymethod(String mPaymethod) {
        this.mPaymethod = mPaymethod;
    }

    public boolean ismStatus() {
        return mStatus;
    }

    public void setmStatus(boolean mStatus) {
        this.mStatus = mStatus;
    }

    public String getmPayBank() {
        return mPayBank;
    }

    public void setmPayBank(String mPayBank) {
        this.mPayBank = mPayBank;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public class TaggedObject {
        @SerializedName("id")
        private int mId;
        @SerializedName("travel_date")
        private String mTravelDate;
        @SerializedName("boarding_point_name")
        private String mBoardingPointName;
        @SerializedName("dropping_point_name")
        private String mDroppingPointName;
        @SerializedName("operator_name")
        private String mOperatorName;
        @SerializedName("txt_status")
        private String mTxnStatus;
        @SerializedName("passenger")
        private ArrayList<PassengerDetails> Passengers = new ArrayList<>();


        public int getmId() {
            return mId;
        }

        public void setmId(int mId) {
            this.mId = mId;
        }

        public String getmTravelDate() {
            return mTravelDate;
        }

        public void setmTravelDate(String mTravelDate) {
            this.mTravelDate = mTravelDate;
        }

        public String getmBoardingPointName() {
            return mBoardingPointName;
        }

        public void setmBoardingPointName(String mBoardingPointName) {
            this.mBoardingPointName = mBoardingPointName;
        }

        public String getmDroppingPointName() {
            return mDroppingPointName;
        }

        public void setmDroppingPointName(String mDroppingPointName) {
            this.mDroppingPointName = mDroppingPointName;
        }

        public String getmOperatorName() {
            return mOperatorName;
        }

        public void setmOperatorName(String mOperatorName) {
            this.mOperatorName = mOperatorName;
        }

        public String getmTxnStatus() {
            return mTxnStatus;
        }

        public void setmTxnStatus(String mTxnStatus) {
            this.mTxnStatus = mTxnStatus;
        }

        public ArrayList<PassengerDetails> getPassengers() {
            return Passengers;
        }

        public void setPassengers(ArrayList<PassengerDetails> passengers) {
            Passengers = passengers;
        }
    }


}
