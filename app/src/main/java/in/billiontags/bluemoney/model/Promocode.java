package in.billiontags.bluemoney.model;

import com.google.gson.annotations.SerializedName;

import static in.billiontags.bluemoney.utils.Utils.getParsedDate;

/**
 * Created by admin on 2/7/2018.
 */

public class Promocode {

    @SerializedName("id")
    private int mId;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("amount")
    private int mAmount;
    @SerializedName("is_active")
    private boolean isActive;
    @SerializedName("offer_code")
    private String mOfferCode;
    @SerializedName("expiry_on")
    private String mExpiryOn;
    @SerializedName("created_on")
    private String mCreatedOn;


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getmOfferCode() {
        return mOfferCode;
    }

    public void setmOfferCode(String mOfferCode) {
        this.mOfferCode = mOfferCode;
    }

    public String getmExpiryOn() {
        return getParsedDate(mExpiryOn);
    }

    public void setmExpiryOn(String mExpiryOn) {
        this.mExpiryOn = mExpiryOn;
    }

    public String getmCreatedOn() {
        return mCreatedOn;
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }
}
