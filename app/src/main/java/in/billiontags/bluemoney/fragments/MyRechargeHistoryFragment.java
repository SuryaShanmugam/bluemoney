package in.billiontags.bluemoney.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.RechargeHistoryAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.RechargeHistoryCallback;
import in.billiontags.bluemoney.dialogs.RechargeDetailviewDialog;
import in.billiontags.bluemoney.model.MyOrdersHistory;
import in.billiontags.bluemoney.model.MyOrdersItem;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Response;


public class MyRechargeHistoryFragment extends Fragment implements RechargeHistoryCallback, SwipeRefreshLayout.OnRefreshListener, Runnable, View.OnClickListener {

    private View mRootView;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<MyOrdersItem> mMyordersItemList;
    private RechargeHistoryAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private SwipeRefreshLayout mRefreshLayout;
    private boolean mIsLoading;
    private int mCount = 0;
    private int mVisibleThreshold = 10;
    private int mLastVisibleItem, mTotalItemCount;
    private String mUrl;

    public MyRechargeHistoryFragment() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_rechargehistory, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        showProgressDialog("Loading....");
        setUrl();
        return mRootView;
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.post(this);
    }

    private void initObjects() {
        mContext = getActivity();
        mPreference = new MyPreference(mContext);
        mRecyclerView = mRootView.findViewById(R.id.recyclerview);
        mRefreshLayout = mRootView.findViewById(R.id.refresh);
        mProgressDialog = new ProgressDialog(mContext);
        mMyordersItemList = new ArrayList<>();
        mAdapter = new RechargeHistoryAdapter(mContext, mMyordersItemList, this);
        mLayoutManager = new LinearLayoutManager(mContext);
    }


    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        mRefreshLayout.post(this);
    }

    @Override
    public void mOnItemClick(int pos) {
        MyOrdersItem myOrdersItem = mMyordersItemList.get(pos);
        int mrechargeId = myOrdersItem.getmId();
        RechargeDetailviewDialog rechargeDetailviewDialog = new RechargeDetailviewDialog(mContext);
        rechargeDetailviewDialog.mRechargeDialog(mrechargeId, myOrdersItem.ismStatus());
    }


    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                mTotalItemCount = mLayoutManager.getItemCount();
                Log.e("mTotalItemCount", "" + mTotalItemCount);

                mLastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                if (!mIsLoading && (mUrl != null) && (mTotalItemCount <= (mLastVisibleItem + mVisibleThreshold))) {
                    getListService();
                }
            }
        });
    }

    @Override
    public void run() {
    }

    @Override
    public void onRefresh() {
        setUrl();
    }


    private void setLoading(boolean loading) {
        mIsLoading = loading;
    }

    private void setUrl() {
        mUrl = ApiClient.BASE_URL + "users/list/my-orders/";
        Log.e("mUrl", "" + mUrl);
        getListService();
    }

    private void getListService() {
        setLoading(true);

        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<MyOrdersHistory> call = categoryService.getRechargeHistory("Token " + mPreference.getToken(), mUrl, new MyOrdersHistory("1"));

        call.enqueue(new retrofit2.Callback<MyOrdersHistory>() {

            @Override
            public void onResponse(@NonNull Call<MyOrdersHistory> call, @NonNull Response<MyOrdersHistory> response) {
                MyOrdersHistory myOrdersHistory = response.body();
                if (response.isSuccessful() && myOrdersHistory != null) {
                    hideProgressDialog();
                    setLoading(false);

                    if (isAdded()) {
                        mCount = myOrdersHistory.getmCount();
                        mUrl = myOrdersHistory.getmNextUrl();
                        mRefreshLayout.setRefreshing(false);
                        mMyordersItemList.clear();
                        mMyordersItemList.addAll(myOrdersHistory.getMyOrdersItems());
                        mAdapter.notifyDataSetChanged();
                    }


                } else {
                    hideProgressDialog();
                    if (isAdded()) {
                        mRefreshLayout.setRefreshing(false);
                        ErrorHandler.processError(mContext, response.code(), response.errorBody());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyOrdersHistory> call, @NonNull Throwable t) {
                if (isAdded()) {
                    mRefreshLayout.setRefreshing(false);
                    hideProgressDialog();
                    ToastBuilder.build(mContext, t.getMessage());
                }
            }
        });
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {


    }


}
