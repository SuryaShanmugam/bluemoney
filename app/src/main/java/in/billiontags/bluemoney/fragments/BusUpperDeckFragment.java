package in.billiontags.bluemoney.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.BusSleeperSeatBookingAdapter;
import in.billiontags.bluemoney.callback.BusSeatBookingCallback;
import in.billiontags.bluemoney.model.BusLowerDesk;
import in.billiontags.bluemoney.model.BusSeatCountSelected;
import in.billiontags.bluemoney.model.BusSeatCountUnSelected;

public class BusUpperDeckFragment extends Fragment implements BusSeatBookingCallback {

    private static final String KEY_COLUMNLIST = "lowercolumn";
    private static final String KEY_UPPERDECK = "upperdecarray";
    private static final String KEY_ROWSLIST = "upperrows";

    public int mId;
    public GridLayoutManager mLayoutManager;
    protected Context mContext;
    protected String mUrl;
    ArrayList<BusLowerDesk> mColumnList1 = new ArrayList<>();
    ArrayList<BusLowerDesk> mColumnList2 = new ArrayList<>();
    ArrayList<BusLowerDesk> mColumnList3 = new ArrayList<>();
    ArrayList<BusLowerDesk> mColumnList4 = new ArrayList<>();
    private View mRootView;
    private RecyclerView mRecyclerView;
    private BusSleeperSeatBookingAdapter mAdapter;
    private ArrayList<BusLowerDesk> mBusStructureList;
    private ArrayList<BusLowerDesk> mBusLowerList = new ArrayList<>();
    private int columlist, Rowslist;

    public BusUpperDeckFragment() {
    }

    public static BusUpperDeckFragment newInstance(int mColumnList, int Rowslist, ArrayList<BusLowerDesk> mLowerlist) {

        Bundle bundle = new Bundle();
        bundle.putInt(KEY_COLUMNLIST, mColumnList);
        bundle.putInt(KEY_ROWSLIST, Rowslist);

        bundle.putParcelableArrayList(KEY_UPPERDECK, mLowerlist);

        BusUpperDeckFragment productFragment = new BusUpperDeckFragment();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_lowerdeck, container, false);

        processbundle();
        initObjects();
        initRecyclerView();
        return mRootView;
    }

    private void processbundle() {
        Bundle bundle = getArguments();
        mBusLowerList.clear();
        if (bundle != null) {
            columlist = bundle.getInt(KEY_COLUMNLIST);
            Rowslist = bundle.getInt(KEY_ROWSLIST);
            mBusStructureList = bundle.getParcelableArrayList(KEY_UPPERDECK);
            ArrayList<BusLowerDesk> mColumnList1 = new ArrayList<>();
            ArrayList<BusLowerDesk> mColumnList2 = new ArrayList<>();
            ArrayList<BusLowerDesk> mColumnList3 = new ArrayList<>();
            ArrayList<BusLowerDesk> mColumnList4 = new ArrayList<>();

            Collections.sort(mBusStructureList, new Comparator<BusLowerDesk>() {
                @Override
                public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                    return Integer.valueOf(o1.getmColumn()).compareTo(o2.getmColumn());
                }
            });
            for (int m = 0; m < mBusStructureList.size(); m++) {
                if (mBusStructureList.get(m).getmColumn() == 1) {
                    mColumnList1.add(mBusStructureList.get(m));
                } else if (mBusStructureList.get(m).getmColumn() == 2) {
                    mColumnList2.add(mBusStructureList.get(m));
                } else if (mBusStructureList.get(m).getmColumn() == 3) {
                    mColumnList3.add(mBusStructureList.get(m));
                } else if (mBusStructureList.get(m).getmColumn() == 4) {
                    mColumnList4.add(mBusStructureList.get(m));
                }
            }

            Collections.sort(mColumnList1, new Comparator<BusLowerDesk>() {
                @Override
                public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                    return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                }
            });

            Collections.sort(mColumnList2, new Comparator<BusLowerDesk>() {
                @Override
                public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                    return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                }
            });
            Collections.sort(mColumnList3, new Comparator<BusLowerDesk>() {
                @Override
                public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                    return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                }
            });
            Collections.sort(mColumnList4, new Comparator<BusLowerDesk>() {
                @Override
                public int compare(BusLowerDesk o1, BusLowerDesk o2) {
                    return Integer.valueOf(o1.getmRow()).compareTo(o2.getmRow());
                }
            });


            mBusLowerList.addAll(mColumnList1);
            mBusLowerList.addAll(mColumnList2);
            mBusLowerList.addAll(mColumnList3);
            mBusLowerList.addAll(mColumnList4);


        }
    }

    private void initObjects() {

        mContext = getActivity();
        mRecyclerView = mRootView.findViewById(R.id.bussleeper);
        mBusStructureList = new ArrayList<>();
        mAdapter = new BusSleeperSeatBookingAdapter(mContext, mBusLowerList, this);
    }

    private void initRecyclerView() {
        mLayoutManager = new GridLayoutManager(mContext, Rowslist, GridLayoutManager.HORIZONTAL, false);
        mLayoutManager.setSpanCount(columlist);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void mOnItemClick(int pos) {
        BusLowerDesk busLowerDesk = mBusLowerList.get(pos);
        if (busLowerDesk.getmSeatStatus().equals("A") ||
                busLowerDesk.getmSeatStatus().equals("F")
                || busLowerDesk.getmSeatStatus().equals("M")) {
            busLowerDesk.setmSeatStatus("S");
            EventBus.getDefault().post(new BusSeatCountSelected(busLowerDesk.getmSeatFare(), busLowerDesk.getmSeatNumber(), pos));
            mAdapter.notifyDataSetChanged();
        } else if (busLowerDesk.getmSeatStatus().equals("S")) {
            busLowerDesk.setmSeatStatus("A");
            EventBus.getDefault().post(new BusSeatCountUnSelected(busLowerDesk.getmSeatFare(), busLowerDesk.getmSeatNumber(), pos));
            mAdapter.notifyDataSetChanged();

        }

    }
}
