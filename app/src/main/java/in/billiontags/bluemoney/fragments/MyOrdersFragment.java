package in.billiontags.bluemoney.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.MyPagerAdapter;
import in.billiontags.bluemoney.app.MyPreference;

public class MyOrdersFragment extends Fragment implements View.OnClickListener, TabLayout.OnTabSelectedListener, Runnable {


    public int mId;
    protected Context mContext;
    protected String mUrl;
    MyPreference myPreference;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;

    private View mRootView;

    public MyOrdersFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_myorder, container, false);
        initObjects();
        initCallbacks();
        initTabs();

        return mRootView;
    }

    private void initObjects() {
        mTabLayout = mRootView.findViewById(R.id.tab);
        mViewPager = mRootView.findViewById(R.id.pager_profile);
        mContext = getActivity();
        myPreference = new MyPreference(mContext);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getChildFragmentManager(), mFragmentList);
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);
    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Recharge"));
        mFragmentList.add(new MyRechargeHistoryFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Bus Bookings"));
        mFragmentList.add(new MyBusBookingListFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Hotel Bookings"));
        mFragmentList.add(new MyHotelBookingListFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Filght Bookings"));
        mFragmentList.add(new MyFlightListFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("Wallet"));
        mFragmentList.add(new MyWalletListFragment());
        mPagerAdapter.notifyDataSetChanged();

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void run() {

    }
}
