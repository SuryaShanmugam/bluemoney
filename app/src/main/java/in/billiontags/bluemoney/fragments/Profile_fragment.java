package in.billiontags.bluemoney.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.billiontags.bluemoney.EditProfileActivity;
import in.billiontags.bluemoney.MyAddressActivity;
import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.MyProfile;
import in.billiontags.bluemoney.model.MyaddressList;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.billiontags.bluemoney.utils.Activity.launch;
import static in.billiontags.bluemoney.utils.Activity.launchWithBundle;


public class Profile_fragment extends Fragment implements View.OnClickListener {

    private static final String USR_NAME = "name";
    private static final String USR_EMAIL = "email";
    private static final String USR_GENDER = "gender";
    private static final String USR_DOB = "dob";
    private static final String USR_PROFILEPIC = "pic";
    MyPreference myPreference;
    String gender;
    List<MyaddressList> CountList;
    private Context mContext;
    private View mRootView;
    private TextView mTextViewProfileName, mTextViewMobile, mTextViewUserName, mTextViewUserGender,
            mTextViewUserEmail, mTextViewUserDob, mTextViewUpdateProfile, mTextViewViewAddress, mTextViewshowAddress;
    private CircleImageView mImageUserProfile;
    private String profileImage;
    private MyPreference mPreference;


    public Profile_fragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_profile, container, false);
        initObjects();
        initCallbacks();


        getMyProfile();
        getListService();
        return mRootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    private void initObjects() {
        mContext = getActivity();
        myPreference = new MyPreference(mContext);
        mImageUserProfile = mRootView.findViewById(R.id.image_profile);
        mTextViewProfileName = mRootView.findViewById(R.id.txt_profilename);
        mTextViewMobile = mRootView.findViewById(R.id.txt_mobilenumber);
        mTextViewUserName = mRootView.findViewById(R.id.txt_usrname);
        mTextViewUserGender = mRootView.findViewById(R.id.txt_usrgender);
        mTextViewUserEmail = mRootView.findViewById(R.id.txt_usremail);
        mTextViewUserDob = mRootView.findViewById(R.id.txt_usrdob);
        mTextViewViewAddress = mRootView.findViewById(R.id.txt_Viewmore);
        mTextViewshowAddress = mRootView.findViewById(R.id.address);
        mTextViewUpdateProfile = mRootView.findViewById(R.id.txt_updateprofile);
        mTextViewMobile.setText(myPreference.getPhone());
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mTextViewUpdateProfile.setOnClickListener(this);
        mTextViewViewAddress.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewUpdateProfile) {

            Bundle bundle = new Bundle();
            bundle.putString(USR_NAME, mTextViewUserName.getText().toString());
            bundle.putString(USR_GENDER, gender);
            bundle.putString(USR_EMAIL, mTextViewUserEmail.getText().toString());
            bundle.putString(USR_DOB, mTextViewUserDob.getText().toString());
            bundle.putString(USR_PROFILEPIC, profileImage);
            launchWithBundle(mContext, EditProfileActivity.class, bundle);
        } else if (v == mTextViewViewAddress) {
            launch(mContext, MyAddressActivity.class);
        }


    }

    public void getMyProfile() {

        RechargesService rechargeService = ApiClient.getClient().create(RechargesService.class);
        Call<MyProfile> call = rechargeService.getMyProfile("Token " + myPreference.getToken());
        call.enqueue(new Callback<MyProfile>() {
            @Override
            public void onResponse(@NonNull Call<MyProfile> call, @NonNull Response<MyProfile> response) {
                MyProfile myProfile = response.body();
                if (response.isSuccessful() && myProfile != null) {
                    if (isAdded()) {
                        mTextViewUserName.setText(myProfile.getmFirstName());
                        mTextViewProfileName.setText(myProfile.getmFirstName());

                        if (myProfile.getUserprofile() != null) {
                            if (myProfile.getUserprofile().getmGender().equals("M")) {
                                mTextViewUserGender.setText(R.string.male);
                            } else {
                                mTextViewUserGender.setText(R.string.female);
                            }
                        }

                        gender = myProfile.getUserprofile().getmGender();
                        mTextViewUserEmail.setText(myProfile.getmEmail());
                        mTextViewUserDob.setText(myProfile.getUserprofile().getmDob());
                        profileImage = myProfile.getUserprofile().getmProfilepic();
                        Glide.with(mContext).load(myProfile.getUserprofile().getmProfilepic())
                                .apply(new RequestOptions().placeholder(R.drawable.ic_man).error(R.drawable.ic_man))
                                .into(mImageUserProfile);

                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyProfile> call, @NonNull Throwable t) {
                if (isAdded()) {
                    ToastBuilder.build(mContext, t.getMessage());
                }


            }
        });


    }

    private void getListService() {

        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<ArrayList<MyaddressList>> call = categoryService.getMyaddress("Token " + mPreference.getToken());

        call.enqueue(new retrofit2.Callback<ArrayList<MyaddressList>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<MyaddressList>> call, @NonNull Response<ArrayList<MyaddressList>> response) {
                List<MyaddressList> CategoryListResponse = response.body();
                if (response.isSuccessful() && CategoryListResponse != null) {

                    if(isAdded())
                    {
                        for (int i = 0; i < CategoryListResponse.size() - 1; i++) {
                            CountList = new ArrayList<>();
                            if (CategoryListResponse.get(i).ismIsActive()) {
                                CountList.add(CategoryListResponse.get(i));
                                mTextViewshowAddress.setText(String.format("%s, %s, %s, %s, %s,%s",
                                        CategoryListResponse.get(i).getmAddress1(), CategoryListResponse.get(i).getmAddress2(),
                                        CategoryListResponse.get(i).getmCity(), CategoryListResponse.get(i).getmState(),
                                        CategoryListResponse.get(i).getmZipCode(), CategoryListResponse.get(i).getmCountry()));
                            }
                            mTextViewViewAddress.setText(String.format(getString(R.string.view_more), CountList.size()));
                        }
                    }


                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<MyaddressList>> call, @NonNull Throwable t) {

                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        getListService();
        getMyProfile();
    }
}
