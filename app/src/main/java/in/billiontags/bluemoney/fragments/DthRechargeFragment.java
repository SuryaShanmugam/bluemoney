package in.billiontags.bluemoney.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import in.billiontags.bluemoney.PaymentActivity;
import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.dialogs.Operatordialog;
import in.billiontags.bluemoney.model.Operator;
import in.billiontags.bluemoney.model.ValidateRechargeData;
import in.billiontags.bluemoney.presenter.DthoperatorApi;
import in.billiontags.bluemoney.presenter.Operatorpresenter;
import in.billiontags.bluemoney.presenter.Operatorrequestview;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DthRechargeFragment extends Fragment implements View.OnClickListener, Operatorrequestview {

    public String mRechargeAmount;
    TextView change_operator;
    ArrayList<Operator> truckslist = new ArrayList<>();
    Operatorpresenter mOperatorPresenter;
    String Operatorid = "";
    String Operatorname = "";
    private Context mContext;
    private View mRootView;
    private TextInputLayout mTextLayoutmCardNumber, mTextLayoutOperator, mTextLayoutAmount;
    private EditText mEditTextmCardNumber, mEditTextOperatorname, mEditTextAmount;
    private Button recharge_now;
    private ProgressDialog mProgressDialog;

    public DthRechargeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_dth, container, false);
        initObjects();
        initCallbacks();
        return mRootView;
    }

    private void initCallbacks() {
        mEditTextOperatorname.setOnClickListener(this);
        recharge_now.setOnClickListener(this);
    }

    private void initObjects() {

        mContext = getActivity();
        mProgressDialog = new ProgressDialog(mContext);
        mTextLayoutmCardNumber = mRootView.findViewById(R.id.layout_CardNumber);
        mTextLayoutOperator = mRootView.findViewById(R.id.layout_Operator);
        mTextLayoutAmount = mRootView.findViewById(R.id.layout_Amount);
        mEditTextmCardNumber = mRootView.findViewById(R.id.mCardNumber);
        mEditTextOperatorname = mRootView.findViewById(R.id.Operator);
        mEditTextAmount = mRootView.findViewById(R.id.Amount);
        recharge_now = mRootView.findViewById(R.id.recharge_now);
        change_operator = mRootView.findViewById(R.id.change_operator);
        mOperatorPresenter = DthoperatorApi.newInstance(DthRechargeFragment.this);
        mOperatorPresenter.Getting_trailer_list();


    }

    @Override
    public void onClick(View v) {
        if (v == recharge_now) {
            processdata();
        } else if (v == mEditTextOperatorname) {
            mOperatorListclick();
        }
    }


    public void mOperatorListclick() {
        Operatordialog operatordialog = new Operatordialog(getActivity(), DthRechargeFragment.this);
        operatordialog.mOperatorListDialog(getActivity(), truckslist);
    }

    @Override
    public void mOperatorlist(ArrayList<Operator> trckslist) {
        truckslist = trckslist;

    }

    @Override
    public void setOperatorName(String operatorid, String operatorname) {
        Operatorid = operatorid;
        Operatorname = operatorname;
        mEditTextOperatorname.setText(Operatorname);
    }


    private void processdata() {
        String mCardNumberdata = mEditTextmCardNumber.getText().toString().trim();
        String select_operator1 = Operatorid;
        mRechargeAmount = mEditTextAmount.getText().toString().trim();
        String opera = mEditTextOperatorname.getText().toString().trim();
        if (validateInput(opera, mCardNumberdata, mRechargeAmount)) {
            showProgressDialog("Validating....");
            getValidateRecharge(new ValidateRechargeData(mCardNumberdata, select_operator1, mRechargeAmount), mCardNumberdata, select_operator1, mRechargeAmount);
        }
    }

    private boolean validateInput(String select_operator11, String cardnumber, String amount) {
        if (TextUtils.isEmpty(select_operator11)) {
            mEditTextOperatorname.requestFocus();
            mTextLayoutOperator.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Operator"));
            return false;
        } else if (TextUtils.isEmpty(cardnumber)) {
            mEditTextmCardNumber.requestFocus();
            mTextLayoutmCardNumber.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Card Number"));
            return false;
        } else if (TextUtils.isEmpty(amount)) {
            mEditTextAmount.requestFocus();
            mTextLayoutAmount.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Amount"));
            return false;
        }
        return true;
    }


    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void getValidateRecharge(ValidateRechargeData validateRechargeData, final String mCardnumber, final String operator, final String amount) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<ValidateRechargeData> call = authService.getRechargeValidation(validateRechargeData);
        call.enqueue(new Callback<ValidateRechargeData>() {

            @Override
            public void onResponse(@NonNull Call<ValidateRechargeData> call, @NonNull Response<ValidateRechargeData> response) {
                ValidateRechargeData validateRecharge = response.body();
                hideProgressDialog();
                if (response.isSuccessful() && validateRecharge != null) {
                    if (validateRecharge.getmIpayErrorCode().equals("TXN")) {
                        Intent i = new Intent(mContext, PaymentActivity.class);
                        i.putExtra("RechargeAmount", amount);
                        i.putExtra("Operator", operator);
                        i.putExtra("mobile", mCardnumber);
                        startActivity(i);
                    } else if (validateRecharge.getmIpayErrorCode().equals("IAN")) {
                        ToastBuilder.build(mContext, validateRecharge.getmIpayErrorDesc());
                    }
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ValidateRechargeData> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
                hideProgressDialog();
            }
        });
    }
}
