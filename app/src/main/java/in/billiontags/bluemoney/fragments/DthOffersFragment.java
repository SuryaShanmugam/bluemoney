package in.billiontags.bluemoney.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.billiontags.bluemoney.R;

/**
 * Created by admin on 2/6/2018.
 */

public class DthOffersFragment extends Fragment {

    private static final String EXTRA_ID = "id";
    private Context mContext;
    private View mRootView;

    public DthOffersFragment() {

    }

    public static DthRechargeFragment newInstance(int id) {

        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, id);
        DthRechargeFragment productFragment = new DthRechargeFragment();
        productFragment.setArguments(bundle);
        return productFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_dth, container, false);

        return mRootView;
    }
}
