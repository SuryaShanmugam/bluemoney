package in.billiontags.bluemoney.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import in.billiontags.bluemoney.PaymentActivity;
import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.dialogs.Operatordialog;
import in.billiontags.bluemoney.model.FindOperator;
import in.billiontags.bluemoney.model.Operator;
import in.billiontags.bluemoney.model.ValidateRechargeData;
import in.billiontags.bluemoney.presenter.Operatorpresenter;
import in.billiontags.bluemoney.presenter.Operatorrequestview;
import in.billiontags.bluemoney.presenter.PospaidoperatorApi;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DatacardPostpaidFragment extends Fragment implements View.OnClickListener, Runnable, Operatorrequestview {


    public String mRechargeAmount;
    TextView change_operator;
    TextView Browse_plan_layout;
    ArrayList<Operator> truckslist = new ArrayList<>();
    Operatorpresenter operatorPresenter;
    String Operatorid = "";
    String Operatorname = "";
    private Context mContext;
    private View mRootView;
    private Button recharge_now;
    private ProgressDialog mProgressDialog;
    private TextInputLayout mTextLayoutMobile, mTextLayoutOperator, mTextLayoutAmount;
    private EditText mEditTextMobile, mEditTextOperatorname, mEditTextAmount;
    private ImageView mobileImage;

    public DatacardPostpaidFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_datacardpostpaid, container, false);
        initObjects();
        initCallbacks();


        return mRootView;
    }

    private void initObjects() {

        mContext = getActivity();
        mProgressDialog = new ProgressDialog(mContext);
        mTextLayoutMobile = mRootView.findViewById(R.id.layout_mobilenumber);
        mTextLayoutOperator = mRootView.findViewById(R.id.layout_Operator);
        mTextLayoutAmount = mRootView.findViewById(R.id.layout_Amount);
        mEditTextMobile = mRootView.findViewById(R.id.mobile_number);
        mEditTextOperatorname = mRootView.findViewById(R.id.Operator);
        mobileImage = mRootView.findViewById(R.id.mobileImage);
        mEditTextAmount = mRootView.findViewById(R.id.Amount);
        recharge_now = mRootView.findViewById(R.id.recharge_now);
        Browse_plan_layout = mRootView.findViewById(R.id.Browse_plan_layout);
        recharge_now.setText(R.string.btn_pay_bill);
        change_operator = mRootView.findViewById(R.id.change_operator);

        operatorPresenter = PospaidoperatorApi.newInstance(DatacardPostpaidFragment.this);
        operatorPresenter.Getting_trailer_list();
        mTextchangeListner(mEditTextMobile);


    }

    private void mTextchangeListner(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    mobileImage.setVisibility(View.VISIBLE);
                    mobileImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            editText.getText().clear();
                            mEditTextOperatorname.getText().clear();
                        }
                    });
                } else
                    mobileImage.setVisibility(View.GONE);

                if (start > 3) {
                    getOperator(new FindOperator(editText.getText().toString()), count);
                }
            }
        });

    }


    private void initCallbacks() {
        mEditTextOperatorname.setOnClickListener(this);
        Browse_plan_layout.setOnClickListener(this);
        recharge_now.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == recharge_now) {
            processdata();
        } else if (v == change_operator) {
            mOperatorListclick();
        }
    }

    public void mOperatorListclick() {
        Operatordialog operatordialog = new Operatordialog(getActivity(), DatacardPostpaidFragment.this);
        operatordialog.mOperatorListDialog(getActivity(), truckslist);
    }

    private void processdata() {
        String mobilenumber = mEditTextMobile.getText().toString().trim();
        String select_operator1 = Operatorid;
        String operator = mEditTextOperatorname.getText().toString();
        mRechargeAmount = mEditTextAmount.getText().toString().trim();
        if (validateInput(mobilenumber, operator, mRechargeAmount)) {
            showProgressDialog("Validating....");
            getValidateRecharge(new ValidateRechargeData(mobilenumber, select_operator1, mRechargeAmount), mobilenumber, select_operator1, mRechargeAmount);
        }
    }

    private boolean validateInput(String mobilenumber11, String select_operator11, String amount) {
        if (TextUtils.isEmpty(mobilenumber11)) {
            mEditTextMobile.requestFocus();
            mTextLayoutMobile.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Mobile Number"));
            return false;
        } else if (mobilenumber11.length() < 5) {
            mEditTextMobile.requestFocus();
            mTextLayoutMobile.setError(getString(R.string.error_mobile_length));
            return false;
        } else if (TextUtils.isEmpty(select_operator11)) {
            mEditTextOperatorname.requestFocus();
            mTextLayoutOperator.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Operator"));
            return false;
        } else if (TextUtils.isEmpty(amount)) {
            mEditTextAmount.requestFocus();
            mTextLayoutAmount.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Amount"));
            return false;
        }
        return true;
    }

    @Override
    public void run() {

    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    @Override
    public void mOperatorlist(ArrayList<Operator> trckslist) {
        truckslist = trckslist;
    }


    @Override
    public void setOperatorName(String operatorid, String operatorname) {
        Operatorid = operatorid;
        Operatorname = operatorname;
        Log.e("Operatorid", "" + Operatorid);
        mEditTextOperatorname.setText(Operatorname);

    }

    private void getValidateRecharge(ValidateRechargeData validateRechargeData, final String mobilenumber, final String operator, final String Amount) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<ValidateRechargeData> call = authService.getRechargeValidation(validateRechargeData);
        call.enqueue(new Callback<ValidateRechargeData>() {

            @Override
            public void onResponse(@NonNull Call<ValidateRechargeData> call, @NonNull Response<ValidateRechargeData> response) {
                ValidateRechargeData validateRecharge = response.body();
                hideProgressDialog();
                if (response.isSuccessful() && validateRecharge != null) {
                    if (validateRecharge.getmIpayErrorCode().equals("TXN")) {
                        Intent i = new Intent(mContext, PaymentActivity.class);
                        i.putExtra("RechargeAmount", Amount);
                        i.putExtra("Operator", operator);
                        i.putExtra("mobile", mobilenumber);
                        startActivity(i);
                    } else if (validateRecharge.getmIpayErrorCode().equals("IAN")) {
                        ToastBuilder.build(mContext, validateRecharge.getmIpayErrorDesc());
                    }
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ValidateRechargeData> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
                hideProgressDialog();
            }
        });
    }

    public void getOperator(FindOperator findOperator, final int Count) {
        RechargesService rechargeService = ApiClient.getClient().create(RechargesService.class);
        Call<FindOperator> call = rechargeService.getOperator(findOperator);
        call.enqueue(new Callback<FindOperator>() {


            @Override
            public void onResponse(@NonNull Call<FindOperator> call, @NonNull Response<FindOperator> response) {
                FindOperator findOperator = response.body();
                if (response.isSuccessful() && findOperator != null) {
                    if (Count == 0) {
                        mEditTextOperatorname.getText().clear();
                    } else {
                        mEditTextOperatorname.setText(findOperator.getmOperatorName());
                        Operatorid = findOperator.getmOperatoCode();
                    }

                } else {
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<FindOperator> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());

            }
        });


    }
}
