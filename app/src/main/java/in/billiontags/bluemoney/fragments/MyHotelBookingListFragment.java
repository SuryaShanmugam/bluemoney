package in.billiontags.bluemoney.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.HotelHistoryAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.RechargeHistoryCallback;
import in.billiontags.bluemoney.model.MyOrdersHistory;
import in.billiontags.bluemoney.model.MyOrdersItem;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Response;

public class MyHotelBookingListFragment extends Fragment implements RechargeHistoryCallback, SwipeRefreshLayout.OnRefreshListener, Runnable, View.OnClickListener {

    String mUrl;
    private View mRootView;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<MyOrdersItem> mMyordersItemList;
    private HotelHistoryAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private SwipeRefreshLayout mRefreshLayout;

    public MyHotelBookingListFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_rechargehistory, container, false);

        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        setUrl();
        return mRootView;
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.post(this);
    }

    private void initObjects() {
        mContext = getActivity();
        mPreference = new MyPreference(mContext);
        mRecyclerView = mRootView.findViewById(R.id.recyclerview);
        mRefreshLayout = mRootView.findViewById(R.id.refresh);
        mProgressDialog = new ProgressDialog(mContext);
        mMyordersItemList = new ArrayList<>();
        mAdapter = new HotelHistoryAdapter(mContext, mMyordersItemList, this);
        mLayoutManager = new LinearLayoutManager(mContext);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        mRefreshLayout.post(this);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void run() {

    }

    @Override
    public void onRefresh() {
        getListService();
    }

    private void setUrl() {

        mUrl = ApiClient.BASE_URL + "users/list/my-orders/";
        Log.e("mUrl", "" + mUrl);
        getListService();
    }

    private void getListService() {
        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<MyOrdersHistory> call = categoryService.getRechargeHistory("Token " + mPreference.getToken(), mUrl, new MyOrdersHistory("3"));
        call.enqueue(new retrofit2.Callback<MyOrdersHistory>() {

            @Override
            public void onResponse(@NonNull Call<MyOrdersHistory> call, @NonNull Response<MyOrdersHistory> response) {
                MyOrdersHistory myOrdersHistory = response.body();
                if (response.isSuccessful() && myOrdersHistory != null) {
                    if (isAdded()) {
                        hideProgressDialog();
                        mRefreshLayout.setRefreshing(false);
                        mMyordersItemList.clear();
                        mMyordersItemList.addAll(myOrdersHistory.getMyOrdersItems());
                        mAdapter.notifyDataSetChanged();
                    }

                } else {
                    hideProgressDialog();
                    mRefreshLayout.setRefreshing(false);
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyOrdersHistory> call, @NonNull Throwable t) {
                if (isAdded()) {
                    mRefreshLayout.setRefreshing(false);
                    hideProgressDialog();
                    ToastBuilder.build(mContext, t.getMessage());
                }
            }
        });
    }


    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {


    }


    @Override
    public void mOnItemClick(int id) {

    }
}
