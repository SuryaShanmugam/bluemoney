package in.billiontags.bluemoney.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.MyPagerAdapter;
import in.billiontags.bluemoney.app.MyPreference;


public class FragmentHome extends Fragment implements TabLayout.OnTabSelectedListener, Runnable {

    public int mId;
    protected Context mContext;
    protected String mUrl;
    MyPreference myPreference;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private MyPagerAdapter mPagerAdapter;
    private List<Fragment> mFragmentList;
    private int[] tabIcons = {
            R.drawable.ic_icon_home,
            R.drawable.ic_postpaid_new,
            R.drawable.ic_user_male_black_shape
    };
    private View mRootView;


    public FragmentHome() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.activity_main_, container, false);
        initObjects();
        initCallbacks();
        initTabs();


        return mRootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
        int posotion = tab.getPosition();

        if (posotion == 0) {

            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_icon_home);
            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));
        } else if (posotion == 1) {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_postpaid);

            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));

        } else if (posotion == 2) {

            mTabLayout.getTabAt(2).setIcon(R.drawable.ic_user_male_black_shape_new);
            mTabLayout.setTabTextColors(
                    getResources().getColor(R.color.tab_text_color),
                    getResources().getColor(R.color.white));

        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        int posotion = tab.getPosition();
        if (posotion == 0) {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_icon_home_new);
        } else if (posotion == 1) {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_postpaid_new);
        } else if (posotion == 2) {
            mTabLayout.getTabAt(2).setIcon(R.drawable.ic_user_male_black_shape);
        }

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        int posotion = tab.getPosition();
        if (posotion == 0) {
            mTabLayout.getTabAt(0).setIcon(R.drawable.ic_icon_home);
        } else if (posotion == 1) {
            mTabLayout.getTabAt(1).setIcon(R.drawable.ic_postpaid);
        } else if (posotion == 2) {
            mTabLayout.getTabAt(2).setIcon(R.drawable.ic_user_male_black_shape_new);
        }
    }

    @Override
    public void run() {

    }

    private void initObjects() {

        mTabLayout = mRootView.findViewById(R.id.tab);
        mViewPager = mRootView.findViewById(R.id.pager_profile);
        mContext = getActivity();
        myPreference = new MyPreference(mContext);
        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getChildFragmentManager(), mFragmentList);

    }

    private void initTabs() {
        mTabLayout.addTab(mTabLayout.newTab().setText("Home"));
        mFragmentList.add(new Homefragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("My Wallet"));
        mFragmentList.add(new WalletFragment());
        mTabLayout.addTab(mTabLayout.newTab().setText("My Profile"));
        mFragmentList.add(new Profile_fragment());
        mPagerAdapter.notifyDataSetChanged();

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        setupTabIcons();
        setTabsFont();
    }

    private void initCallbacks() {
        mTabLayout.addOnTabSelectedListener(this);
    }

    private void setupTabIcons() {

        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
        mTabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(
                            Typeface.createFromAsset(mContext.getAssets(),
                                    "fonts/Lato-Regular.ttf"),
                            Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(mContext.getResources().getDimension(R.dimen.small_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }
}
