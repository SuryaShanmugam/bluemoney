package in.billiontags.bluemoney.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.WalletTransactionAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.WalletTransaction;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.billiontags.bluemoney.utils.WalletAmount.getWalletAmount;


public class WalletFragment extends Fragment implements View.OnClickListener, Runnable {

    public ProgressDialog mProgressDialog;
    public TextView mTextViewWalletBal;
    MyPreference myPreference;
    private View mRootView;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<WalletTransaction> mWalletTransactionList;
    private WalletTransactionAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    public WalletFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_wallet, container, false);
        initObjects();
        initRecyclerView();
        getWalletTransaction();
        return mRootView;
    }


    private void initObjects() {
        mContext = getActivity();
        myPreference = new MyPreference(mContext);
        mRecyclerView = mRootView.findViewById(R.id.recyclerview);
        mTextViewWalletBal = mRootView.findViewById(R.id.txt_walletbalance);
        mProgressDialog = new ProgressDialog(mContext);
        myPreference = new MyPreference(mContext);
        mWalletTransactionList = new ArrayList<>();
        mAdapter = new WalletTransactionAdapter(mContext, mWalletTransactionList);
        mLayoutManager = new LinearLayoutManager(mContext);
        getWalletAmount(mContext, mTextViewWalletBal);
    }

    private void initRecyclerView() {
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void getWalletTransaction() {
        RechargesService apiService = ApiClient.getClient().create(RechargesService.class);
        Call<ArrayList<WalletTransaction>> call = apiService.getWalletTransaction("Token " + myPreference.getToken());
        call.enqueue(new Callback<ArrayList<WalletTransaction>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<WalletTransaction>> call, @NonNull Response<ArrayList<WalletTransaction>> response) {
                List<WalletTransaction> walletTransactions = response.body();
                if (response.isSuccessful() && walletTransactions != null) {

                    if (isAdded()) {
                        mWalletTransactionList.clear();
                        mWalletTransactionList.addAll(walletTransactions);
                        mAdapter.notifyDataSetChanged();
                    }

                } else {

                    if (isAdded()) {
                        ErrorHandler.processError(mContext, response.code(), response.errorBody());
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<WalletTransaction>> call, @NonNull Throwable t) {
                if (isAdded()) {
                    ToastBuilder.build(mContext, t.getMessage());
                }
            }
        });
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void run() {

    }
}
