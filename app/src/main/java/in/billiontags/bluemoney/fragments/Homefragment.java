package in.billiontags.bluemoney.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.billiontags.bluemoney.BroadbandRechargeActivity;
import in.billiontags.bluemoney.BusSearchActivity;
import in.billiontags.bluemoney.DTHRechargeActivity;
import in.billiontags.bluemoney.DataCardRechargeActivity;
import in.billiontags.bluemoney.ElectricityRechargeActivity;
import in.billiontags.bluemoney.GasBillPaymentActivity;
import in.billiontags.bluemoney.InsurancePayActivity;
import in.billiontags.bluemoney.LandlineActivity;
import in.billiontags.bluemoney.MobileRechargeActivity;
import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.MyProfile;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.billiontags.bluemoney.utils.Activity.launch;

public class Homefragment extends Fragment implements View.OnClickListener {


    public ImageView mImageViewBack;
    LinearLayout mMobileLyout, mDThLayout, mLandlineLayout, mElectricityLayout,
            mBroadbandlayout, mDataCardLayout, mInsuranceLayout, mGasLayout, mBusLayout;
    MyPreference myPreference;
    private Context mContext;
    private View mRootView;
    private TextView mTextViewRechargeCount, mTextViewHotelCount, mTextViewBusBookingCount, mTextViewFlightBookingCount;

    public Homefragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_home, container, false);
        initObjects();
        initCallbacks();
        getMyProfileCount();

        return mRootView;
    }

    private void initObjects() {
        mContext = getActivity();
        mImageViewBack = mRootView.findViewById(R.id.img_back);
        mMobileLyout = mRootView.findViewById(R.id.mobile_layout);
        mDThLayout = mRootView.findViewById(R.id.DTH_layout);
        mElectricityLayout = mRootView.findViewById(R.id.electricity_layout);
        mLandlineLayout = mRootView.findViewById(R.id.landline_layout);
        mBroadbandlayout = mRootView.findViewById(R.id.mBroadBand_layout);
        mDataCardLayout = mRootView.findViewById(R.id.Datacard_layout);
        mInsuranceLayout = mRootView.findViewById(R.id.Insurance_layout);
        mGasLayout = mRootView.findViewById(R.id.Gas_layout);
        mBusLayout = mRootView.findViewById(R.id.Bus_layout);


        mTextViewRechargeCount = mRootView.findViewById(R.id.txt_rechargeCount);
        mTextViewHotelCount = mRootView.findViewById(R.id.txt_hotelCount);
        mTextViewBusBookingCount = mRootView.findViewById(R.id.txt_busbookingCount);
        mTextViewFlightBookingCount = mRootView.findViewById(R.id.txt_flightBookingCount);
        myPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mMobileLyout.setOnClickListener(this);
        mDThLayout.setOnClickListener(this);
        mLandlineLayout.setOnClickListener(this);
        mElectricityLayout.setOnClickListener(this);
        mBroadbandlayout.setOnClickListener(this);
        mDataCardLayout.setOnClickListener(this);
        mInsuranceLayout.setOnClickListener(this);
        mGasLayout.setOnClickListener(this);
        mBusLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mMobileLyout) {
            launch(mContext, MobileRechargeActivity.class);
        } else if (v == mDThLayout) {
            launch(mContext, DTHRechargeActivity.class);
        } else if (v == mElectricityLayout) {
            launch(mContext, ElectricityRechargeActivity.class);
        } else if (v == mLandlineLayout) {
            launch(mContext, LandlineActivity.class);
        } else if (v == mBroadbandlayout) {
            launch(mContext, BroadbandRechargeActivity.class);
        } else if (v == mDataCardLayout) {
            launch(mContext, DataCardRechargeActivity.class);
        } else if (v == mInsuranceLayout) {
            launch(mContext, InsurancePayActivity.class);
        } else if (v == mGasLayout) {
            launch(mContext, GasBillPaymentActivity.class);
        } else if (v == mBusLayout) {
            launch(mContext, BusSearchActivity.class);
        }
    }

    public void getMyProfileCount() {

        RechargesService rechargeService = ApiClient.getClient().create(RechargesService.class);
        Call<MyProfile> call = rechargeService.getMyProfile("Token " + myPreference.getToken());
        call.enqueue(new Callback<MyProfile>() {


            @Override
            public void onResponse(@NonNull Call<MyProfile> call, @NonNull Response<MyProfile> response) {
                MyProfile myProfile = response.body();
                if (response.isSuccessful() && myProfile != null) {
                    mTextViewRechargeCount.setText(myProfile.getmRechargeCount());
                    mTextViewHotelCount.setText(myProfile.getmHotelCount());
                    mTextViewBusBookingCount.setText(myProfile.getmBusCount());
                    mTextViewFlightBookingCount.setText(myProfile.getFlightCount());

                }
            }

            @Override
            public void onFailure(@NonNull Call<MyProfile> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());

            }
        });


    }
}
