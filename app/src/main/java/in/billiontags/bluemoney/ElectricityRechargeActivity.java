package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.dialogs.Operatordialog;
import in.billiontags.bluemoney.model.Operator;
import in.billiontags.bluemoney.model.ValidateRechargeData;
import in.billiontags.bluemoney.presenter.ElectricityoperatorApi;
import in.billiontags.bluemoney.presenter.Operatorpresenter;
import in.billiontags.bluemoney.presenter.Operatorrequestview;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.WalletAmount.getWalletAmount;

public class ElectricityRechargeActivity extends AppCompatActivity implements View.OnClickListener, Operatorrequestview {


    public TextView mWalletAmount;

    ArrayList<Operator> mOperatorList = new ArrayList<>();
    Operatorpresenter moperatorpresenter;
    String Operatorid = "";
    String Operatorname = "";
    private Context mContext;
    private Button recharge_now;
    private ProgressDialog mProgressDialog;
    private ImageView mImageback;
    private TextInputLayout mTextLayoutmCardNumber, mTextLayoutOperator;
    private EditText mEditTextmCardNumber, mEditTextOperatorname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_electricity);
        initObjects();
        initCallbacks();


    }

    private void initObjects() {

        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mTextLayoutmCardNumber = findViewById(R.id.layout_AccountNumber);
        mTextLayoutOperator = findViewById(R.id.layout_Operator);
        mEditTextmCardNumber = findViewById(R.id.Edit_AccountNumber);
        mEditTextOperatorname = findViewById(R.id.Operator);
        recharge_now = findViewById(R.id.recharge_now);
        mWalletAmount = findViewById(R.id.amount_bar);
        mImageback = findViewById(R.id.img_back);

        recharge_now.setText(R.string.btn_pay_bill);

        moperatorpresenter = ElectricityoperatorApi.newInstance(ElectricityRechargeActivity.this);
        moperatorpresenter.Getting_trailer_list();
        getWalletAmount(mContext, mWalletAmount);
    }

    private void initCallbacks() {
        mEditTextOperatorname.setOnClickListener(this);
        recharge_now.setOnClickListener(this);
        mImageback.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void onClick(View v) {
        if (v == recharge_now) {
            processdata();
        } else if (v == mEditTextOperatorname) {
            mOperatorListclick();
        } else if (v == mImageback) {
            onBackPressed();
        }
    }


    public void mOperatorListclick() {
        Operatordialog operatordialog = new Operatordialog(this, ElectricityRechargeActivity.this);
        operatordialog.mOperatorListDialog(this, mOperatorList);
    }

    private void processdata() {
        String cardnum = mEditTextmCardNumber.getText().toString().trim();
        String select_operator1 = Operatorid;
        String operator = mEditTextOperatorname.getText().toString();
        if (validateInput(cardnum, operator)) {
            showProgressDialog("Validating....");
            getValidateRecharge(new ValidateRechargeData(cardnum, select_operator1, "0"), cardnum, select_operator1, "0");
        }
    }

    private boolean validateInput(String select_operator11, String cardnum) {
        if (TextUtils.isEmpty(select_operator11)) {
            mEditTextOperatorname.requestFocus();
            mTextLayoutOperator.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Operator"));
            return false;
        } else if (TextUtils.isEmpty(cardnum)) {
            mEditTextmCardNumber.requestFocus();
            mTextLayoutmCardNumber.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Account Number"));
            return false;
        }
        return true;
    }


    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void mOperatorlist(ArrayList<Operator> trckslist) {
        mOperatorList = trckslist;
    }


    @Override
    public void setOperatorName(String operatorid, String operatorname) {
        Operatorid = operatorid;
        Operatorname = operatorname;
        Log.e("Operatorid", "" + Operatorid);
        mEditTextOperatorname.setText(Operatorname);

    }

    private void getValidateRecharge(ValidateRechargeData validateRechargeData, final String mobilenumber, final String operator, final String Amount) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<ValidateRechargeData> call = authService.getRechargeValidation(validateRechargeData);
        call.enqueue(new Callback<ValidateRechargeData>() {

            @Override
            public void onResponse(@NonNull Call<ValidateRechargeData> call, @NonNull Response<ValidateRechargeData> response) {
                ValidateRechargeData validateRecharge = response.body();
                hideProgressDialog();
                if (response.isSuccessful() && validateRecharge != null) {
                    if (validateRecharge.getmIpayErrorCode().equals("TXN")) {
                        Intent i = new Intent(mContext, PaymentActivity.class);
                        i.putExtra("RechargeAmount", Amount);
                        i.putExtra("Operator", operator);
                        i.putExtra("mobile", mobilenumber);
                        startActivity(i);
                    } else if (validateRecharge.getmIpayErrorCode().equals("IAN")) {
                        ToastBuilder.build(mContext, validateRecharge.getmIpayErrorDesc());
                    }
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ValidateRechargeData> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
                hideProgressDialog();
            }
        });
    }

}
