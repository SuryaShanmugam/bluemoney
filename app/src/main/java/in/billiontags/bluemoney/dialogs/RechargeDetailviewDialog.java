package in.billiontags.bluemoney.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.RechargeDetailHistory;
import in.billiontags.bluemoney.utils.ErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RechargeDetailviewDialog implements View.OnClickListener {

    protected Context context;
    private Dialog dialog;
    private TextView mTextViewMobilenumber, mTextViewCategory, mTextViewOperator, mTextViewTxnId,
            mTextViewRechargeStatus, mTextViewHeaderCategoryname, mTextViewAmount, mTextViewPaymentInfo, mTextViewBluemoneyCash;
    private ImageView mImageRemove, mImageStatus;
    private MyPreference mPreference;

    public RechargeDetailviewDialog(Context context) {

        this.context = context;
        dialog = new Dialog(context);
    }

    public void mRechargeDialog(int id, boolean mTransStaus) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_rechargedetail);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        initObjects();
        initCallbacks();
        String mUrl = ApiClient.BASE_URL + "payments/retrieve/bookings/" + id + "/";
        getHistoryDetail(mUrl);

        if (mTransStaus) {
            mTextViewRechargeStatus.setText(R.string.recharge_successful);
            mImageStatus.setImageResource(R.drawable.ic_tick_sucess);

        } else {
            mTextViewRechargeStatus.setText(R.string.recharge_failure);
            mImageStatus.setImageResource(R.drawable.ic_remove_circle);
        }

        dialog.show();

    }

    private void initCallbacks() {
        mImageRemove.setOnClickListener(this);
    }

    private void initObjects() {
        mTextViewRechargeStatus = dialog.findViewById(R.id.recharge_statusheader);
        mTextViewHeaderCategoryname = dialog.findViewById(R.id.service_providername);
        mTextViewMobilenumber = dialog.findViewById(R.id.connection_number);
        mImageRemove = dialog.findViewById(R.id.img_remove);
        mTextViewCategory = dialog.findViewById(R.id.Category);
        mTextViewOperator = dialog.findViewById(R.id.Operator);
        mTextViewTxnId = dialog.findViewById(R.id.txn_id);
        mTextViewAmount = dialog.findViewById(R.id.amount);
        mTextViewPaymentInfo = dialog.findViewById(R.id.payment_info);
        mTextViewBluemoneyCash = dialog.findViewById(R.id.Bluemoney_Amount_info);
        mImageStatus = dialog.findViewById(R.id.img_status);
        mPreference = new MyPreference(context);

    }

    @Override
    public void onClick(View v) {
        if (v == mImageRemove) {
            dialog.dismiss();
        }
    }


    private void getHistoryDetail(String mUrl) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<RechargeDetailHistory> call = authService.getRechargedetail("Token " + mPreference.getToken(), mUrl);
        call.enqueue(new Callback<RechargeDetailHistory>() {
            @Override
            public void onResponse(@NonNull Call<RechargeDetailHistory> call, @NonNull Response<RechargeDetailHistory> response) {
                RechargeDetailHistory rechargeDetailHistory = response.body();
                if (response.isSuccessful() && rechargeDetailHistory != null) {
                    mTextViewHeaderCategoryname.setText(String.format("for %s %s", rechargeDetailHistory.getTagged_object().getmServiceProvider().getmServiceName(),
                            rechargeDetailHistory.getTagged_object().getmServiceProvider().getmCategory()));
                    mTextViewMobilenumber.setText(rechargeDetailHistory.getTagged_object().getmMobilenumber());
                    mTextViewCategory.setText(rechargeDetailHistory.getTagged_object().getmServiceProvider().getmCategory());
                    mTextViewOperator.setText(rechargeDetailHistory.getTagged_object().getmServiceProvider().getmServiceName());
                    mTextViewTxnId.setText(rechargeDetailHistory.getmTxnId());
                    mTextViewAmount.setText(String.valueOf("\u20b9 " + rechargeDetailHistory.getTagged_object().getmTrensferAmount()));
                    int cash = rechargeDetailHistory.getTagged_object().getmTrensferAmount() - rechargeDetailHistory.getmTxnAmount();
                    mTextViewBluemoneyCash.setText(String.valueOf("\u20b9 " + cash));

                } else {
                    ErrorHandler.processError(context, response.code(), response.errorBody());
                }

            }


            @Override
            public void onFailure(@NonNull Call<RechargeDetailHistory> call, @NonNull Throwable t) {


            }
        });
    }

}
