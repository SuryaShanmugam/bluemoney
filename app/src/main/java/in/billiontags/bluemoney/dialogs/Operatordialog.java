package in.billiontags.bluemoney.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.OperatorPlanAdapter;
import in.billiontags.bluemoney.model.Operator;
import in.billiontags.bluemoney.presenter.Operatorrequestview;


public class Operatordialog {

    protected Context context;
    private Dialog dialog;
    private OperatorPlanAdapter operatorPlanAdapter;
    private Operatorrequestview opertorrequestview;

    public Operatordialog(Activity context, Operatorrequestview tripview) {

        this.context = context;
        dialog = new Dialog(context);
        opertorrequestview = tripview;
    }

    public void mOperatorListDialog(final Activity mcontext, final ArrayList<Operator> mOperatorList) {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.operator_list_dialog);
        TextView yeshaul_truck_list_title = dialog.findViewById(R.id.yeshaul_truck_list_title);
        ListView yeshaul_truck_list_dialog_view = dialog.findViewById(R.id.yeshaul_truck_list_dialog_view);
        Button yeshaul_list_dialog_cancel = dialog.findViewById(R.id.yeshaul_truck_list_dialog_cancel);
        yeshaul_truck_list_title.setText("Select Operator");
        operatorPlanAdapter = new OperatorPlanAdapter(mcontext, mOperatorList);
        yeshaul_truck_list_dialog_view.setAdapter(operatorPlanAdapter);
        yeshaul_truck_list_dialog_view.setFastScrollEnabled(true);
        yeshaul_list_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        yeshaul_truck_list_dialog_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                opertorrequestview.setOperatorName(String.valueOf(mOperatorList.get(position).getmId()), mOperatorList.get(position).getmServiceName());

                dialog.dismiss();
            }
        });

        dialog.show();

    }


}
