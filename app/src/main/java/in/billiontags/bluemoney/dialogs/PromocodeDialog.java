package in.billiontags.bluemoney.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.adapter.PromocodeAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.callback.PromocodeCallback;
import in.billiontags.bluemoney.model.Promocode;
import in.billiontags.bluemoney.utils.ErrorHandler;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PromocodeDialog implements PromocodeCallback, View.OnClickListener {

    protected Context context;
    private Dialog dialog;
    private RecyclerView mRecyclerView;
    private ArrayList<Promocode> mPromocodeList = new ArrayList<>();
    private PromocodeAdapter promocodeAdapter;
    private LinearLayoutManager mLayoutManager;
    private EditText mPromocode;
    private TextView mTextViewApply, mTextviewNoOffers;
    private ImageView mImageRemove;
    private LinearLayout mRecyclerviewLayout;

    public PromocodeDialog(Activity context) {

        this.context = context;
        dialog = new Dialog(context);
    }

    public void promoDialog() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_promocode);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        initObjects();
        initCallbacks();
        initRecyclerView();

        getListPromocode();

        dialog.show();

    }

    private void initCallbacks() {
        mImageRemove.setOnClickListener(this);
    }

    private void initObjects() {
        mRecyclerView = dialog.findViewById(R.id.recyclerview);
        mPromocode = dialog.findViewById(R.id.edit_promocode);
        mTextViewApply = dialog.findViewById(R.id.txt_apply);
        mImageRemove = dialog.findViewById(R.id.img_remove);
        mTextviewNoOffers = dialog.findViewById(R.id.no_Offers);
        mRecyclerviewLayout = dialog.findViewById(R.id.recyclerviewLayout);
        promocodeAdapter = new PromocodeAdapter(context, mPromocodeList, this);
        mLayoutManager = new LinearLayoutManager(context);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(promocodeAdapter);

    }

    @Override
    public void onClick(View v) {
        if (v == mImageRemove) {
            dialog.dismiss();
        }
    }

    @Override
    public void mAmountItemClick(int id) {

    }

    public void getListPromocode() {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<ArrayList<Promocode>> call = authService.getPromocde();
        call.enqueue(new Callback<ArrayList<Promocode>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Promocode>> call, @NonNull Response<ArrayList<Promocode>> response) {
                List<Promocode> promocode = response.body();
                if (response.isSuccessful() && promocode != null) {
                    mPromocodeList.clear();
                    if (promocode.size() > 0) {
                        mRecyclerviewLayout.setVisibility(View.VISIBLE);
                        mTextviewNoOffers.setVisibility(View.GONE);
                        mPromocodeList.addAll(promocode);
                        promocodeAdapter.notifyItemInserted(promocode.size());
                        promocodeAdapter.notifyDataSetChanged();
                    } else {
                        mRecyclerviewLayout.setVisibility(View.GONE);
                        mTextviewNoOffers.setVisibility(View.VISIBLE);
                    }

                } else {
                    ErrorHandler.processError(context, response.code(), response.errorBody());
                }

            }


            @Override
            public void onFailure(@NonNull Call<ArrayList<Promocode>> call, @NonNull Throwable t) {


            }
        });
    }


}
