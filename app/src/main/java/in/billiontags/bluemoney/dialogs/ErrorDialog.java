package in.billiontags.bluemoney.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import in.billiontags.bluemoney.R;


public class ErrorDialog {

    protected Context context;
    private Dialog dialog;

    public ErrorDialog(Activity context) {

        this.context = context;
        dialog = new Dialog(context);
    }

    public void Error_dialog(final String message) {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.error_dialog_box);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        TextView error_dialog = dialog.findViewById(R.id.error_dialog);

        error_dialog.setText(message);
        TextView ok_dialog = dialog.findViewById(R.id.ok_dialog);

        ok_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }

}
