package in.billiontags.bluemoney;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.adapter.MyPagerAdapter;
import in.billiontags.bluemoney.fragments.IntroFragment;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WelcomeActivity extends AppCompatActivity {


    private ViewPager mPagerIntro;
    private PageIndicatorView mIndicatorView;
    private List<Fragment> mFragmentList;
    private MyPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initObjects();
        initViewPager();
        populateIntro();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void initObjects() {
        mPagerIntro = findViewById(R.id.pager_intro);
        mIndicatorView = findViewById(R.id.indicator);

        mFragmentList = new ArrayList<>();
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragmentList);


    }

    private void initViewPager() {
        mPagerIntro.setAdapter(mPagerAdapter);
        mIndicatorView.setViewPager(mPagerIntro);
    }

    private void populateIntro() {
        mFragmentList.add(
                IntroFragment.newInstance("Quick And Easy Recharge!!!",
                        "Recharge your prepaid mobile or DTH or Pay your postpaid bills "
                                + "with three simple steps from your app.", "Skip", R.drawable.walkthrough1));
        mFragmentList.add(IntroFragment.newInstance("Flexible Flight Booking!!!",
                "Find and book your flight with a few simple swipes."
                        + "down from now on with Blue Money.", "Skip", R.drawable.walthrough2));
        mFragmentList.add(
                IntroFragment.newInstance("Flight Booking now Easy!!!",
                        "All you have to do is choose your departure date and your arrival city."
                                + " from our platform.", "Get Started", R.drawable.walthrough3));
        mPagerAdapter.notifyDataSetChanged();
        mIndicatorView.setCount(mFragmentList.size());


    }

}
