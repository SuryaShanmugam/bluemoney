package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.adapter.BusHistoryDetailAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.BushistoryDetailCallback;
import in.billiontags.bluemoney.model.BusDetailHistory;
import in.billiontags.bluemoney.model.PassengerDetails;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BusHistoryDetailActivity extends AppCompatActivity implements BushistoryDetailCallback, View.OnClickListener {
    public Context mContext;
    MyPreference myPreference;


    public TextView mTextViewTicketPNRNo, mTextViewBookingId, mTextViewDateTime, mTextViewPicuplocation, mTextViewDropLocation,
            mTextViewNoPassengers, mTextViewTotalFare, mTextViewBoadingPoint, mTextViewBoardingTime, mTextViewDropingPoint,
            mTextViewDropingTime, mTextViewOperatorDetails, mTextViewBusContactNo, mTextViewPaymentInfo, mTextViewCancel;

    private RecyclerView mRecyclerview;
    public ProgressDialog mProgressDialog;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<PassengerDetails> mPassengerList;
    private BusHistoryDetailAdapter mAdapter;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_history_detail);

        initObjects();
        initCallbacks();
        initRecyclerView();
        getBusdetails();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }

    private void initRecyclerView() {
        mRecyclerview.setLayoutManager(mLayoutManager);
        mRecyclerview.setNestedScrollingEnabled(false);
        mRecyclerview.setHasFixedSize(true);
        mRecyclerview.setAdapter(mAdapter);

    }

    private void initObjects() {
        mContext = this;
        myPreference = new MyPreference(mContext);
        mImageBack = findViewById(R.id.img_back);

        mTextViewTicketPNRNo = findViewById(R.id.txt_ticketpnr);
        mTextViewBookingId = findViewById(R.id.txt_bookingid);
        mTextViewDateTime = findViewById(R.id.txt_datetime);
        mTextViewPicuplocation = findViewById(R.id.txt_pickup);
        mTextViewDropLocation = findViewById(R.id.txt_drop);
        mTextViewNoPassengers = findViewById(R.id.txt_passengers);
        mTextViewTotalFare = findViewById(R.id.txt_ticketfare);

        mTextViewBoadingPoint = findViewById(R.id.txt_boardingpoint);
        mTextViewBoardingTime = findViewById(R.id.txt_boardingtime);
        mTextViewDropingPoint = findViewById(R.id.txt_droppingpoint);
        mTextViewDropingTime = findViewById(R.id.txt_droppingtime);
        mTextViewOperatorDetails = findViewById(R.id.txt_operatordetails);
        mTextViewBusContactNo = findViewById(R.id.txt_buscontactno);
        mTextViewPaymentInfo = findViewById(R.id.txt_paymentinfo);
        mTextViewCancel = findViewById(R.id.txt_cancel);

        mRecyclerview = findViewById(R.id.recyclerview);

        mPassengerList = new ArrayList<>();
        mAdapter = new BusHistoryDetailAdapter(mContext, mPassengerList, this);
        mLayoutManager = new LinearLayoutManager(mContext);
    }

    @Override
    public void mOnItemClick(int id) {

    }

    @Override
    public void onClick(View v) {

        if (v == mImageBack) {
            onBackPressed();
        }

    }

    private void getBusdetails() {


        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<BusDetailHistory> call = categoryService.getbusdetail("Token " + myPreference.getToken());

        call.enqueue(new retrofit2.Callback<BusDetailHistory>() {

            @Override
            public void onResponse(@NonNull Call<BusDetailHistory> call, @NonNull Response<BusDetailHistory> response) {
                BusDetailHistory busDetailHistory = response.body();
                if (response.isSuccessful() && busDetailHistory != null) {
                    hideProgressDialog();
                    mPassengerList.clear();
                    mPassengerList.addAll(busDetailHistory.getTagged_object().getPassengers());
                    mAdapter.notifyDataSetChanged();


                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BusDetailHistory> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
