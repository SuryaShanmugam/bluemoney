package in.billiontags.bluemoney;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.adapter.BusAmenitiesAdapter;
import in.billiontags.bluemoney.app.Constant;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BusAmentiesDetailActivity extends AppCompatActivity implements View.OnClickListener {


    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<String> mCancellationTerms = new ArrayList<>();
    private ArrayList<String> mCancellationTermsnew = new ArrayList<>();
    private BusAmenitiesAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ImageView mImageClose;
    private TextView mNoTerms, mTextViewFacility, mTextViewBusType, mTextViewPartialCancellations;
    private String mbusTypemodel, mPartialcancellations, mbusfacility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_amenties_detail);
        initObjects();
        processBundle();
        initCallbacks();
        initRecyclerView();
    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
    }

    private void initObjects() {
        mContext = this;
        mRecyclerView = findViewById(R.id.recycler_cancellations);
        mImageClose = findViewById(R.id.img_back);
        mTextViewFacility = findViewById(R.id.txt_busfacility);
        mTextViewBusType = findViewById(R.id.txt_bustype);
        mTextViewPartialCancellations = findViewById(R.id.txt_paritical_cancellations);
        mNoTerms = findViewById(R.id.no_terms);
        mAdapter = new BusAmenitiesAdapter(mContext, mCancellationTerms);
        mLayoutManager = new LinearLayoutManager(mContext);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCancellationTermsnew.addAll(bundle.getStringArrayList(Constant.KEY_CANCELLATIONTERMS));
            if (mCancellationTermsnew.size() > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mNoTerms.setVisibility(View.GONE);
                mCancellationTerms.clear();
                mCancellationTerms.addAll(bundle.getStringArrayList(Constant.KEY_CANCELLATIONTERMS));
            } else {
                mNoTerms.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }

            mbusTypemodel = bundle.getString(Constant.KEY_BUSTYPEMODEL);
            mPartialcancellations = bundle.getString(Constant.KEY_PARTIALCANCELLATIONS);
            mbusfacility = bundle.getString(Constant.KEY_BUSFACILITY);
        }
        mSetData();
    }

    private void mSetData() {
        if (mbusfacility.equals("No")) {
            mTextViewFacility.setText("Non Ac");
        } else {
            mTextViewFacility.setText("Ac");
        }
        if (mPartialcancellations.equals("No")) {
            mTextViewPartialCancellations.setText("Partial Cancellations Not Allowed");
        } else {
            mTextViewPartialCancellations.setText("Partial Cancellations Allowed");
        }
        mTextViewBusType.setText(mbusTypemodel);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        }
    }
}
