package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.MyAddressCallback;


public class AddAddressHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewAddress, mTextViewMobile;

    public ImageView mImageEdit;
    MyAddressCallback mCallback;

    public AddAddressHolder(View itemView, MyAddressCallback callback) {
        super(itemView);
        mTextViewAddress = itemView.findViewById(R.id.Address);
        mTextViewMobile = itemView.findViewById(R.id.mobile_number);
        mImageEdit = itemView.findViewById(R.id.Edit_Option);

        mCallback = callback;
        mImageEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageEdit) {
            mCallback.itemOnClick(mImageEdit, getLayoutPosition());
        }

    }
}
