package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;


public class BusAmenitiesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewCancellationTerms;


    public BusAmenitiesHolder(View itemView) {
        super(itemView);
        mTextViewCancellationTerms = itemView.findViewById(R.id.txt_cancellationsterms);

    }

    @Override
    public void onClick(View v) {

    }
}
