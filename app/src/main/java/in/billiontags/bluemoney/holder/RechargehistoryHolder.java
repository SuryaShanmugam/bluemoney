package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.RechargeHistoryCallback;


public class RechargehistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mImageViewStatus;
    public TextView mTextViewStatus, mTextViewPrice, mTextViewPaymentMode, mTextViewMobile, mTextViewDate;
    private RechargeHistoryCallback mCallback;

    public RechargehistoryHolder(View itemView, RechargeHistoryCallback Callback) {
        super(itemView);
        mImageViewStatus = itemView.findViewById(R.id.img_status);
        mTextViewStatus = itemView.findViewById(R.id.txt_status);
        mTextViewPrice = itemView.findViewById(R.id.txt_price);
        mTextViewPaymentMode = itemView.findViewById(R.id.txt_payment_mode);
        mTextViewMobile = itemView.findViewById(R.id.txt_mobile);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mCallback = Callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == itemView) {
            mCallback.mOnItemClick(getLayoutPosition());
        }

    }
}
