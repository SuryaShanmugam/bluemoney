package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusSearchDetailCallback;


public class BusSearchDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewPrice, mTextViewDay, mTextViewdate,
            mTextViewTravelsName, mTextViewBusType, mTextViewSeats, mTextViewDuration, mTextViewDepatureTime, mTextViewRatingCount;
    public RatingBar mTravelerRating;
    private BusSearchDetailCallback mCallback;

    public BusSearchDetailHolder(View itemView, BusSearchDetailCallback Callback) {
        super(itemView);
        mTextViewPrice = itemView.findViewById(R.id.title_price);
        mTextViewDay = itemView.findViewById(R.id.title_date_label);
        mTextViewdate = itemView.findViewById(R.id.title_time_label);
        mTextViewTravelsName = itemView.findViewById(R.id.title_travelsname);
        mTextViewBusType = itemView.findViewById(R.id.title_bus_type);
        mTextViewSeats = itemView.findViewById(R.id.title_seat_count);
        mTextViewDuration = itemView.findViewById(R.id.title_durations);
        mTextViewDepatureTime = itemView.findViewById(R.id.title_depatureTime);
        mTravelerRating = itemView.findViewById(R.id.content_rating_Behaviour);
        mTextViewRatingCount = itemView.findViewById(R.id.rating_count);


        mCallback = Callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == itemView) {
            mCallback.mOnItemClick(v, getAdapterPosition());
        }
    }
}
