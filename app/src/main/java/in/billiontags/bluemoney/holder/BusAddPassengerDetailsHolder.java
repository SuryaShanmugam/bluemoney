package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import in.billiontags.bluemoney.R;


public class BusAddPassengerDetailsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView mTextViewPassengerName;
    public EditText mEditTextPassengerName, mEditTextPassengerAge;
    public RadioGroup grp_gender;
    public RadioButton mFemale, mMale;

    public BusAddPassengerDetailsHolder(View itemView) {
        super(itemView);
        mTextViewPassengerName = itemView.findViewById(R.id.txt_passengername);
        mEditTextPassengerName = itemView.findViewById(R.id.edit_name);
        mEditTextPassengerAge = itemView.findViewById(R.id.edit_age);
        grp_gender = itemView.findViewById(R.id.grp_gender);
        mFemale = itemView.findViewById(R.id.radio_female);
        mMale = itemView.findViewById(R.id.radio_male);

    }

    @Override
    public void onClick(View v) {


    }
}
