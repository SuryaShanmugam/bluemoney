package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.TicketCallback;


public class TicketHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewTicket;
    private TicketCallback mCallback;

    public TicketHolder(View itemView, TicketCallback callback) {
        super(itemView);
        mTextViewTicket = itemView.findViewById(R.id.txt_ticket);
        mCallback = callback;
        mTextViewTicket.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == mTextViewTicket) {
            mCallback.onTicketSelect(getLayoutPosition());
        }
    }
}
