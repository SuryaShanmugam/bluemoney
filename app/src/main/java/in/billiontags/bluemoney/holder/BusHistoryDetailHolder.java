package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BushistoryDetailCallback;


public class BusHistoryDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewSeatNo, mTextViewPassengerName, mTextViewSeatType, mTextViewGender, mTextViewAge, mTextViewAmount;
    private BushistoryDetailCallback mCallback;

    public BusHistoryDetailHolder(View itemView, BushistoryDetailCallback Callback) {
        super(itemView);

        mTextViewSeatNo = itemView.findViewById(R.id.txt_seatno);
        mTextViewPassengerName = itemView.findViewById(R.id.txt_passengername);
        mTextViewSeatType = itemView.findViewById(R.id.txt_seattype);
        mTextViewGender = itemView.findViewById(R.id.txt_gender);
        mTextViewAge = itemView.findViewById(R.id.txt_age);
        mTextViewAmount = itemView.findViewById(R.id.txt_fare);
        mCallback = Callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == itemView) {
            mCallback.mOnItemClick(getLayoutPosition());
        }

    }
}
