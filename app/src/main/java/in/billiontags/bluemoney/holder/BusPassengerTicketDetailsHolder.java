package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;


public class BusPassengerTicketDetailsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewName, mTextViewAge, mTextViewGender, mTextViewseatno, mTextViewFare;


    public BusPassengerTicketDetailsHolder(View itemView) {
        super(itemView);
        mTextViewName = itemView.findViewById(R.id.txt_passengername);
        mTextViewAge = itemView.findViewById(R.id.txt_age);
        mTextViewGender = itemView.findViewById(R.id.txt_gender);
        mTextViewseatno = itemView.findViewById(R.id.txt_seatno);
        mTextViewFare = itemView.findViewById(R.id.txt_fare);

    }

    @Override
    public void onClick(View v) {

    }
}
