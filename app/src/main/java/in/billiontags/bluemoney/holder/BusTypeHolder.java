package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusTypeCallback;

/**
 * Created by Bobby on 2/2/2018
 */

public class BusTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTextViewBusType;
    private BusTypeCallback mCallback;

    public BusTypeHolder(View itemView, BusTypeCallback callback) {
        super(itemView);
        mTextViewBusType = itemView.findViewById(R.id.txt_bus_type);
        mCallback = callback;
        mTextViewBusType.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewBusType) {
            mCallback.onBusTypeClick(getLayoutPosition());
        }

    }
}
