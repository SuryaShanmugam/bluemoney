package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;


public class WalletItemHolder extends RecyclerView.ViewHolder {

    public TextView mRewardTitle, mCreatedDate, mAmount;

    public WalletItemHolder(View itemView) {
        super(itemView);
        mRewardTitle = itemView.findViewById(R.id.reward_title);
        mCreatedDate = itemView.findViewById(R.id.txt_createdDate);
        mAmount = itemView.findViewById(R.id.Amount);
    }
}
