package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.PromocodeCallback;


public class PromocodeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView mPromocode, Amount_rate, mPromotitle, Validity, mDescription, mApply;
    PromocodeCallback mCallback;

    public PromocodeViewHolder(View itemView, PromocodeCallback callback) {
        super(itemView);
        mPromocode = itemView.findViewById(R.id.promocode);
        Amount_rate = itemView.findViewById(R.id.Amount_rate);
        mPromotitle = itemView.findViewById(R.id.promo_title);
        Validity = itemView.findViewById(R.id.Validity);
        mDescription = itemView.findViewById(R.id.description);
        mApply = itemView.findViewById(R.id.Apply_btn);
        mCallback = callback;
        Amount_rate.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        mCallback.mAmountItemClick(getLayoutPosition());
    }
}
