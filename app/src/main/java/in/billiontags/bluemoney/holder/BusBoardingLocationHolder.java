package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusBoardingLocationCallback;


public class BusBoardingLocationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView mTextViewboardinglocation, mTextViewboardingtime;
    LinearLayout mlocationlayout;
    private BusBoardingLocationCallback mCallback;

    public BusBoardingLocationHolder(View itemView, BusBoardingLocationCallback Callback) {
        super(itemView);
        mTextViewboardinglocation = itemView.findViewById(R.id.txt_boardinglocation);
        mTextViewboardingtime = itemView.findViewById(R.id.txt_boardingtime);
        mlocationlayout = itemView.findViewById(R.id.location_layout);
        mCallback = Callback;
        mlocationlayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mlocationlayout) {
            mCallback.mOnItemClick(getLayoutPosition());
        }

    }
}
