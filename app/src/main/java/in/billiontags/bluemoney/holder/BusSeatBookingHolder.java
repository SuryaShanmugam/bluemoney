package in.billiontags.bluemoney.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import in.billiontags.bluemoney.R;
import in.billiontags.bluemoney.callback.BusSeatBookingCallback;


public class BusSeatBookingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public ImageView mBusSeatUnAvailableImage, mBusSeatSelectedImage, mBusSeatFemale, mBusSeatGents, mBusSeatAvailable;
    private BusSeatBookingCallback mCallback;

    public BusSeatBookingHolder(View itemView, BusSeatBookingCallback Callback) {
        super(itemView);
        mBusSeatAvailable = itemView.findViewById(R.id.img_seat);
        mBusSeatSelectedImage = itemView.findViewById(R.id.img_seat_selected);
        mBusSeatFemale = itemView.findViewById(R.id.img_seat_female);
        mBusSeatGents = itemView.findViewById(R.id.img_seat_male);
        mBusSeatUnAvailableImage = itemView.findViewById(R.id.img_seat_booked);
        mCallback = Callback;
        mBusSeatAvailable.setOnClickListener(this);
        mBusSeatSelectedImage.setOnClickListener(this);
        mBusSeatFemale.setOnClickListener(this);
        mBusSeatGents.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mBusSeatAvailable) {
            mCallback.mOnItemClick(getLayoutPosition());
        } else if (v == mBusSeatSelectedImage) {
            mCallback.mOnItemClick(getLayoutPosition());
        } else if (v == mBusSeatFemale) {
            mCallback.mOnItemClick(getLayoutPosition());
        } else if (v == mBusSeatGents) {
            mCallback.mOnItemClick(getLayoutPosition());
        }

    }
}
