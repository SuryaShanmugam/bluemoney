package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.MyProfile;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int REQUEST_STORAGE_PERMISSION = 4;
    private static final int REQUEST_FEED_IMAGE = 4;
    public Context mContext;
    public String mName, mEmail, mGender, mDob, mProfilePic;
    protected View mRootView;
    File file;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    private EditText mEditTextName, mEditTextEmail, mEditTextDob;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private ImageView mImageBack;
    private Button mUpdateButton;
    private ImageView mImageProfileEdit;
    private CircleImageView mImageProfile;
    private String mImagePath;
    private Button mButtonMale, mButtonFemale;
    private int Year, Month, Day;
    private String mGenderSelection = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        initObjects();
        processBundle();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void initCallbacks() {
        mUpdateButton.setOnClickListener(this);
        mImageProfileEdit.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mButtonMale.setOnClickListener(this);
        mButtonFemale.setOnClickListener(this);
        mEditTextDob.setOnClickListener(this);
    }

    private void initObjects() {
        mContext = this;
        mEditTextName = findViewById(R.id.edit_name);
        mEditTextEmail = findViewById(R.id.edit_email);
        mEditTextDob = findViewById(R.id.edit_dob);
        mImageProfileEdit = findViewById(R.id.Profile_image_edit_icon);
        mImageProfile = findViewById(R.id.img_profileEdit);
        mImageBack = findViewById(R.id.img_back);
        mButtonMale = findViewById(R.id.txt_male);
        mButtonFemale = findViewById(R.id.txt_female);
        mUpdateButton = findViewById(R.id.btn_Continue);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);

        mButtonMale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_male, 0, 0);
        mButtonFemale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_female, 0, 0);
        mButtonFemale.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_txt));
        mButtonMale.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_txt));
        mButtonMale.setCompoundDrawablePadding(5);
        mButtonFemale.setCompoundDrawablePadding(5);

        calendar = Calendar.getInstance();
        Year = calendar.get(Calendar.YEAR);
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    private void processBundle() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mName = bundle.getString(Constant.USR_NAME);
            mEmail = bundle.getString(Constant.USR_EMAIL);
            mGender = bundle.getString(Constant.USR_GENDER);
            mDob = bundle.getString(Constant.USR_DOB);
            mProfilePic = bundle.getString(Constant.USR_PROFILEPIC);
            mEditTextName.setText(mName);
            mEditTextEmail.setText(mEmail);
            mEditTextDob.setText(mDob);
            Glide.with(mContext).load(mProfilePic)
                    .apply(new RequestOptions().placeholder(R.drawable.ic_man).error(R.drawable.ic_man))
                    .into(mImageProfile);

            if (mGender.equals("M")) {
                mButtonMale.setBackgroundResource(R.drawable.bg_button_borderless_dark);
                mButtonMale.setTextColor(ContextCompat.getColor(mContext, R.color.white));
                mButtonMale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_male_white, 0, 0);
                mButtonMale.setCompoundDrawablePadding(5);

            } else if (mGender.equals("F"))

                mButtonFemale.setBackgroundResource(R.drawable.bg_button_borderless1);
            mButtonFemale.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_txt));
            mButtonFemale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_female, 0, 0);
            mButtonMale.setCompoundDrawablePadding(5);

        }
    }

    @Override
    public void onClick(View v) {
        if (v == mUpdateButton) {
            processEditProfile();
        } else if (v == mImageBack) {
            onBackPressed();
        } else if (v == mImageProfileEdit) {
            processPickImage();
        } else if (v == mEditTextDob) {
            datePickerDialog = DatePickerDialog.newInstance(EditProfileActivity.this, Year, Month, Day);
            datePickerDialog.setThemeDark(false);
            datePickerDialog.showYearPickerFirst(false);
            datePickerDialog.setMaxDate(Calendar.getInstance());
            datePickerDialog.setAccentColor(Color.parseColor("#01ABF2"));
            datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
        } else if (v == mButtonMale) {
            mButtonMale.setBackgroundResource(R.drawable.bg_button_borderless_dark);
            mButtonMale.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            mButtonMale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_male_white, 0, 0);
            mButtonFemale.setBackgroundResource(R.drawable.bg_button_borderless1);
            mButtonFemale.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_txt));
            mButtonFemale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_female, 0, 0);
            mButtonMale.setCompoundDrawablePadding(5);
            mButtonFemale.setCompoundDrawablePadding(5);
            mGenderSelection = "M";
        } else if (v == mButtonFemale) {
            mGenderSelection = "F";
            mButtonFemale.setBackgroundResource(R.drawable.bg_button_borderless_dark);
            mButtonFemale.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            mButtonFemale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_female_white, 0, 0);

            mButtonMale.setBackgroundResource(R.drawable.bg_button_borderless1);
            mButtonMale.setTextColor(ContextCompat.getColor(mContext, R.color.secondary_txt));
            mButtonMale.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_male, 0, 0);
            mButtonMale.setCompoundDrawablePadding(5);
            mButtonFemale.setCompoundDrawablePadding(5);

        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        int month = monthOfYear + 1;
        Log.e("get", "" + month);
        if (month < 10 && dayOfMonth < 10)
            mEditTextDob.setText(year + "-" + (String.format("0%d", monthOfYear + 1)) + "-" + (String.format("0%d", dayOfMonth)));
        else if (month < 10 && dayOfMonth > 10)
            mEditTextDob.setText(year + "-" + (String.format("0%d", monthOfYear + 1)) + "-" + dayOfMonth);
        else
            mEditTextDob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);


    }

    private void processPickImage() {
        if (hasStoragePermission()) {
            pickImage();
        } else {
            requestStoragePermission(REQUEST_STORAGE_PERMISSION);
        }
    }

    private void pickImage() {
        EasyImage.configuration(mContext).setImagesFolderName(getString(R.string.app_name));
        EasyImage.openChooserWithGallery(this, "Select Image", REQUEST_FEED_IMAGE);
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(mContext,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestStoragePermission(int permission) {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission);
    }

    private void displayImage() {
        Glide.with(mContext).load(mImagePath).into(mImageProfile);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles,
                                       EasyImage.ImageSource source, int type) {
                mImagePath = imageFiles.get(0).getPath();
                Log.e("imagepathdataprofile", "" + mImagePath);
                file = new File(mImagePath);
                displayImage();

            }
        });
    }

    private void processEditProfile() {
        String firstName = mEditTextName.getText().toString().trim();
        String email = mEditTextEmail.getText().toString().trim();
        String dob = mEditTextDob.getText().toString();
        String gender;
        if (!mGenderSelection.isEmpty()) {
            gender = mGenderSelection;

        } else {
            gender = mGender;
        }

        showProgressDialog("Updating....");
        geteditprofile(firstName, email, dob, gender);
    }

    public void geteditprofile(final String first_Name, final String email, final String dob, final String gender) {

        RequestBody first_Name1 = RequestBody.create(MediaType.parse("form-data"), first_Name);
        RequestBody gender1 = RequestBody.create(MediaType.parse("form-data"), gender);
        RequestBody dob1 = RequestBody.create(MediaType.parse("form-data"), dob);
        RequestBody email1 = RequestBody.create(MediaType.parse("form-data"), email);
        if (mImagePath != null) {
            RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part imageFileBody = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
            RechargesService newsFeedService = ApiClient.getClient().create(RechargesService.class);
            Call<MyProfile> call = newsFeedService.profileeditImage("Token " + mPreference.getToken(), first_Name1, email1, gender1,
                    dob1, imageFileBody);
            call.enqueue(new retrofit2.Callback<MyProfile>() {

                @Override
                public void onResponse(@NonNull Call<MyProfile> call, @NonNull Response<MyProfile> response) {

                    MyProfile profileView = response.body();
                    if (response.isSuccessful() && profileView != null) {
                        hideProgressDialog();

//                        if(profileView.getUserprofile().getmProfilepic()!=null)
//                        {
//                            mPreference.setProfilePic(profileView.getUserprofile().getmProfilepic());
//                        }


                        onBackPressed();


                    } else {
                        hideProgressDialog();
                        ErrorHandler.processError(mContext, response.code(), response.errorBody());
                    }

                }

                @Override
                public void onFailure(@NonNull Call<MyProfile> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    ToastBuilder.build(mContext, t.getMessage());

                }
            });
        } else {
            RechargesService newsFeedService = ApiClient.getClient().create(RechargesService.class);
            Call<MyProfile> call = newsFeedService.profileedit("Token " + mPreference.getToken(), first_Name1, email1, gender1,
                    dob1);
            call.enqueue(new retrofit2.Callback<MyProfile>() {

                @Override
                public void onResponse(@NonNull Call<MyProfile> call, @NonNull Response<MyProfile> response) {
                    MyProfile profileView = response.body();
                    if (response.isSuccessful() && profileView != null) {
                        hideProgressDialog();
                        onBackPressed();

                    } else {
                        hideProgressDialog();
                        ErrorHandler.processError(mContext, response.code(), response.errorBody());
                    }

                }

                @Override
                public void onFailure(@NonNull Call<MyProfile> call, @NonNull Throwable t) {
                    hideProgressDialog();
                    ToastBuilder.build(mContext, t.getMessage());

                }
            });
        }

    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}

