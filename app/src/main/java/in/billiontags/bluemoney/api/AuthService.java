package in.billiontags.bluemoney.api;


import in.billiontags.bluemoney.model.ActivateAccount;
import in.billiontags.bluemoney.model.ForgotPass;
import in.billiontags.bluemoney.model.LoginPojo;
import in.billiontags.bluemoney.model.ResendPojo;
import in.billiontags.bluemoney.model.ResetPass;
import in.billiontags.bluemoney.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;


public interface AuthService {


    @POST("oauth/registration/")
    Call<User> registerUser(@Body User user);


    @POST("oauth/activate/account/")
    Call<User> activateAccount(@Body ActivateAccount activateAccount);

    @POST("oauth/login/")
    Call<User> loginUser(@Body LoginPojo login);

    @POST("oauth/logout/")
    Call<Void> logoutUser(@Header("Authorization") String token);


    @POST("oauth/forgot-password/")
    Call<ForgotPass> forgotPass(@Body ForgotPass forgotPass);

    @POST("oauth/reset-password/")
    Call<ResetPass> resetPass(@Body ResetPass resetPass);

    @PUT
    Call<ForgotPass> mResendOtp(@Url String url, @Body ResendPojo resendPojo);

}
