package in.billiontags.bluemoney.api;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import in.billiontags.bluemoney.model.BusBoardingPointDetails;
import in.billiontags.bluemoney.model.BusSearchResults;


public class CancellationDetailDeserializer implements JsonDeserializer<BusSearchResults> {
    @Override
    public BusSearchResults deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        BusSearchResults data = new Gson().fromJson(json, BusSearchResults.class);
        JsonObject jsonObject = json.getAsJsonObject();
        if (jsonObject.has("cancellationTerms")) {
            JsonElement element = jsonObject.get("cancellationTerms");
            if (element != null && element.isJsonArray()) {
                Type statusListType = new TypeToken<ArrayList<BusBoardingPointDetails>>() {
                }.getType();
                // data.setmCancellationTermsList((ArrayList<String>) new Gson().fromJson(element, statusListType));
            }
        }
        return data;
    }
}

