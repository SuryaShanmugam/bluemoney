package in.billiontags.bluemoney.api;

import java.util.ArrayList;

import in.billiontags.bluemoney.model.AddAddress;
import in.billiontags.bluemoney.model.AddNewAddress;
import in.billiontags.bluemoney.model.AddWalletMoney;
import in.billiontags.bluemoney.model.BluemoneyCashAmount;
import in.billiontags.bluemoney.model.BusDetailHistory;
import in.billiontags.bluemoney.model.BusOperatorsList;
import in.billiontags.bluemoney.model.BusSeatBooking;
import in.billiontags.bluemoney.model.BusSeatBookingProcess;
import in.billiontags.bluemoney.model.BusStations;
import in.billiontags.bluemoney.model.BusTravelsSearch;
import in.billiontags.bluemoney.model.Data;
import in.billiontags.bluemoney.model.FindOperator;
import in.billiontags.bluemoney.model.MyOrdersHistory;
import in.billiontags.bluemoney.model.MyProfile;
import in.billiontags.bluemoney.model.MyaddressList;
import in.billiontags.bluemoney.model.PaymentSuccess;
import in.billiontags.bluemoney.model.Promocode;
import in.billiontags.bluemoney.model.RechargeDetailHistory;
import in.billiontags.bluemoney.model.RechargeTransaction;
import in.billiontags.bluemoney.model.ServiceOperatorList;
import in.billiontags.bluemoney.model.ValidateRechargeData;
import in.billiontags.bluemoney.model.WalletAmount;
import in.billiontags.bluemoney.model.WalletTransaction;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Url;


public interface RechargesService {


    @GET("recharges/list/service-provider/")
    Call<ArrayList<ServiceOperatorList>> getOperators();

    @POST("recharges/validation/transaction/")
    Call<ValidateRechargeData> getRechargeValidation(@Body ValidateRechargeData validateRechargeData);

    @GET("wallet/amount/")
    Call<WalletAmount> getWalletAmount(@Header("Authorization") String token);

    @POST("recharges/proceed/transaction/")
    Call<RechargeTransaction> getRechargeTransaction(@Header("Authorization") String token, @Body RechargeTransaction rechargeTransaction);


    @POST("payments/payment-success")
    Call<Data> paymentSuccess(@Header("Authorization") String token, @Body PaymentSuccess paymentSuccess);

    @GET
    Call<BluemoneyCashAmount> getBluemoneycash(@Url String url);

    @GET("promotions/promotions/user_list/")
    Call<ArrayList<Promocode>> getPromocde();


    @POST("recharges/plan/")
    Call<FindOperator> getOperator(@Body FindOperator findOperator);


    @GET("wallet/my/wallet-history/")
    Call<ArrayList<WalletTransaction>> getWalletTransaction(@Header("Authorization") String token);

    @POST("wallet/money/transfer/")
    Call<AddWalletMoney> mAddWalletMoney(@Header("Authorization") String token, @Body AddWalletMoney addWalletMoney);


    @GET("users/my-profile/")
    Call<MyProfile> getMyProfile(@Header("Authorization") String token);


    @GET("users/list/address/")
    Call<ArrayList<MyaddressList>> getMyaddress(@Header("Authorization") String token);

    @POST("users/create/address/")
    Call<AddAddress> mAddAddress(@Header("Authorization") String token, @Body AddNewAddress addNewAddress);

    @PUT
    Call<AddNewAddress> mAddEditAddress(@Header("Authorization") String token, @Url String url, @Body AddNewAddress addNewAddress);

    @DELETE
    Call<AddNewAddress> mDeleteAddress(@Header("Authorization") String token, @Url String url);

    @POST
    Call<MyOrdersHistory> getRechargeHistory(@Header("Authorization") String token, @Url String url, @Body MyOrdersHistory myOrdersHistory);


    @Multipart
    @PUT("users/edit/my-profile/")
    Call<MyProfile> profileeditImage(@Header("Authorization") String token,
                                     @Part("first_name") RequestBody firstname,
                                     @Part("email") RequestBody email,
                                     @Part("gender") RequestBody gender,
                                     @Part("dob") RequestBody dob,

                                     @Part MultipartBody.Part profile_pic);

    @Multipart
    @PUT("users/edit/my-profile/")
    Call<MyProfile> profileedit(@Header("Authorization") String token,
                                @Part("first_name") RequestBody firstname,
                                @Part("email") RequestBody email,
                                @Part("gender") RequestBody gender,
                                @Part("dob") RequestBody dob);

    @GET
    Call<RechargeDetailHistory> getRechargedetail(@Header("Authorization") String token, @Url String url);


    @GET
    Call<BusDetailHistory> getbusdetail(@Header("Authorization") String token);

    @GET("travels/list/stations/")
    Call<ArrayList<BusStations>> getStations();

    @POST("travels/search/bus/")
    Call<BusOperatorsList> getBusSearchList(@Body BusTravelsSearch busTravelsSearch);

    @POST("travels/bus/details/")
    Call<BusSeatBooking> getBusSeatDetails(@Body BusSeatBookingProcess busSeatBookingProcess);
}
