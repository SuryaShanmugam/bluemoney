package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.AddAddress;
import in.billiontags.bluemoney.model.AddNewAddress;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddNewAddressActivity extends AppCompatActivity implements View.OnClickListener {

    public Context mContext;
    protected View mRootView;
    EditText mEditTextmMobile, mEditTextmStreet, mmEditTextmArea, mEditTextmCity,
            mEditTextmState, mEditTextmCountry, mEditTextmZipCode;
    private TextInputLayout mTextLayoutmMobile, mTextLayoutStreet, mTextLayoutmArea, mTextLayoutmCity,
            mTextLayoutmState, mTextLayoutmCountry, mTextLayoutmZipCode;
    private Button mSave;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    private ImageView mImageBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);

        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mSave.setOnClickListener(this);
        mImageBack.setOnClickListener(this);

    }

    private void initObjects() {
        mContext = this;
        mTextLayoutmMobile = findViewById(R.id.layout_Mobilenumber);
        mTextLayoutStreet = findViewById(R.id.layout_street);
        mTextLayoutmArea = findViewById(R.id.layout_Area);
        mTextLayoutmCity = findViewById(R.id.layout_City);
        mTextLayoutmState = findViewById(R.id.layout_State);
        mTextLayoutmCountry = findViewById(R.id.layout_ZipCode);
        mTextLayoutmZipCode = findViewById(R.id.layout_Country);

        mEditTextmMobile = findViewById(R.id.edit_mobile);
        mEditTextmStreet = findViewById(R.id.edit_Street);
        mmEditTextmArea = findViewById(R.id.edit_area);
        mEditTextmCity = findViewById(R.id.edit_city);
        mEditTextmState = findViewById(R.id.edit_State);
        mEditTextmCountry = findViewById(R.id.edit_Country);
        mEditTextmZipCode = findViewById(R.id.edit_zipcode);
        mSave = findViewById(R.id.btn_save);

        mImageBack = findViewById(R.id.img_back);
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);

    }

    @Override
    public void onClick(View v) {
        if (v == mSave) {
            processAddress();
        } else if (v == mImageBack) {
            onBackPressed();
        }

    }

    private void processAddress() {
        String mobile = mEditTextmMobile.getText().toString().trim();
        String street = mEditTextmStreet.getText().toString().trim();
        String area = mmEditTextmArea.getText().toString().trim();
        String city = mEditTextmCity.getText().toString().trim();
        String state = mEditTextmState.getText().toString().trim();
        String country = mEditTextmCountry.getText().toString().trim();
        String zipcode = mEditTextmZipCode.getText().toString().trim();
        if (validateInput(mobile, street, area, city, state, zipcode, country)) {
            showProgressDialog("Processing....");
            VerifyAddress(new AddNewAddress(mobile, street, area, city, state, zipcode, country, true));
        }
    }


    private boolean validateInput(String mobile, String street, String area, String city, String state, String zipcode, String country) {
        if (TextUtils.isEmpty(mobile)) {
            mEditTextmMobile.requestFocus();
            mTextLayoutmMobile.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Mobile"));
            return false;
        } else if (mobile.length() < 10) {
            mTextLayoutmMobile.setError(String.format(Locale.getDefault(), getString(R.string.error_length),
                    "Mobile Number", 10, "digits"));
            mEditTextmMobile.requestFocus();
            return false;
        } else if (TextUtils.isEmpty(street)) {
            mEditTextmStreet.requestFocus();
            mTextLayoutStreet.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Street"));
            return false;
        } else if (TextUtils.isEmpty(area)) {
            mmEditTextmArea.requestFocus();
            mTextLayoutmArea.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Area"));
            return false;
        } else if (TextUtils.isEmpty(city)) {
            mEditTextmCity.requestFocus();
            mTextLayoutmCity.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "City"));
            return false;
        } else if (TextUtils.isEmpty(state)) {
            mEditTextmState.requestFocus();
            mTextLayoutmState.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "State"));
            return false;
        } else if (TextUtils.isEmpty(zipcode)) {
            mEditTextmZipCode.requestFocus();
            mTextLayoutmZipCode.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Zipocode"));
            return false;
        } else if (TextUtils.isEmpty(country)) {
            mEditTextmCountry.requestFocus();
            mTextLayoutmCountry.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1), "Country"));
            return false;
        }
        return true;
    }

    private void VerifyAddress(AddNewAddress addNewAddress) {


        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<AddAddress> call = authService.mAddAddress("Token " + mPreference.getToken(), addNewAddress);
        call.enqueue(new Callback<AddAddress>() {
            @Override
            public void onResponse(@NonNull Call<AddAddress> call, @NonNull Response<AddAddress> response) {
                AddAddress newAddress = response.body();
                if (response.isSuccessful() && newAddress != null) {
                    hideProgressDialog();
                    onBackPressed();
                } else {
                    hideProgressDialog();

                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(@NonNull Call<AddAddress> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
