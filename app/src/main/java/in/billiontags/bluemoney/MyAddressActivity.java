package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.List;

import in.billiontags.bluemoney.adapter.MyAddressAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.callback.MyAddressCallback;
import in.billiontags.bluemoney.model.AddNewAddress;
import in.billiontags.bluemoney.model.MyaddressList;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launch;
import static in.billiontags.bluemoney.utils.Activity.launchWithBundle;

public class MyAddressActivity extends AppCompatActivity implements MyAddressCallback, SwipeRefreshLayout.OnRefreshListener, Runnable, View.OnClickListener {


    private static final String ADDR_ID = "mId";
    private static final String ADDR_STREET = "street";
    private static final String ADDR_AREA = "area";
    private static final String ADDR_CITY = "city";
    private static final String ADDR_STATE = "state";
    private static final String ADDR_COUNTRY = "country";
    private static final String ADDR_ZIPCODE = "zipcode";
    private static final String ADDR_MOBILE = "mobile";
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<MyaddressList> mMyAddressList;
    private MyAddressAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private FloatingActionButton mAddAddress;
    private ImageView mImageBack;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myaddress);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        showProgressDialog("Loading...");
        getListService();
    }

    private void initCallbacks() {
        mAddAddress.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.post(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        mPreference = new MyPreference(mContext);
        mRecyclerView = findViewById(R.id.recyclerview);
        mRefreshLayout = findViewById(R.id.refresh);
        mImageBack = findViewById(R.id.img_back);
        mAddAddress = findViewById(R.id.fab_addAddress);
        mProgressDialog = new ProgressDialog(mContext);
        mMyAddressList = new ArrayList<>();
        mAdapter = new MyAddressAdapter(mContext, mMyAddressList, this);
        mLayoutManager = new LinearLayoutManager(mContext);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        mRefreshLayout.post(this);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void run() {

    }

    @Override
    public void onRefresh() {
        getListService();
    }

    @Override
    public void itemOnClick(View v, int position) {
        showPopupMenu(v, position);

    }


    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));
        popup.show();
    }

    private void getListService() {
        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<ArrayList<MyaddressList>> call = categoryService.getMyaddress("Token " + mPreference.getToken());

        call.enqueue(new retrofit2.Callback<ArrayList<MyaddressList>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<MyaddressList>> call, @NonNull Response<ArrayList<MyaddressList>> response) {

                List<MyaddressList> CategoryListResponse = response.body();

                if (response.isSuccessful() && CategoryListResponse != null) {
                    hideProgressDialog();
                    mRefreshLayout.setRefreshing(false);
                    mMyAddressList.clear();
                    for (int i = 0; i < CategoryListResponse.size(); i++) {
                        if (CategoryListResponse.get(i).ismIsActive()) {
                            mMyAddressList.add(CategoryListResponse.get(i));
                            mAdapter.notifyDataSetChanged();
                        }
                    }


                } else {
                    hideProgressDialog();
                    mRefreshLayout.setRefreshing(false);
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<MyaddressList>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        getListService();
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        if (v == mAddAddress) {
            launch(mContext, AddNewAddressActivity.class);
        } else if (v == mImageBack) {
            onBackPressed();
        }

    }

    private void mDeleteAddress(String Url, final int position) {
        RechargesService authService = ApiClient.getClient().create(RechargesService.class);
        Call<AddNewAddress> call = authService.mDeleteAddress("Token " + mPreference.getToken(), Url);
        call.enqueue(new Callback<AddNewAddress>() {
            @Override
            public void onResponse(@NonNull Call<AddNewAddress> call, @NonNull Response<AddNewAddress> response) {
                AddNewAddress addnewaddress = response.body();
                if (response.isSuccessful() && addnewaddress != null) {
                    hideProgressDialog();
                    if (!addnewaddress.ismIsActive()) {
                        mAdapter.notifyDataSetChanged();
                        removeItem(position);
                    }
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(@NonNull Call<AddNewAddress> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());

            }
        });
    }

    private void removeItem(int position) {
        mMyAddressList.remove(position);
        mAdapter.notifyItemRemoved(position);
        mAdapter.notifyItemRangeChanged(position, mMyAddressList.size());
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        public MyMenuItemClickListener(int positon) {
            this.position = positon;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.edit:
                    Bundle bundle = new Bundle();
                    bundle.putInt(ADDR_ID, mMyAddressList.get(position).getmId());
                    bundle.putString(ADDR_STREET, mMyAddressList.get(position).getmAddress1());
                    bundle.putString(ADDR_AREA, mMyAddressList.get(position).getmAddress2());
                    bundle.putString(ADDR_CITY, mMyAddressList.get(position).getmCity());
                    bundle.putString(ADDR_STATE, mMyAddressList.get(position).getmState());
                    bundle.putString(ADDR_COUNTRY, mMyAddressList.get(position).getmCountry());
                    bundle.putString(ADDR_ZIPCODE, mMyAddressList.get(position).getmZipCode());
                    bundle.putString(ADDR_MOBILE, mMyAddressList.get(position).getmMobileNumber());
                    launchWithBundle(mContext, EditAddressActivity.class, bundle);
                    return true;
                case R.id.remove:
                    int mId = mMyAddressList.get(position).getmId();
                    String mUrl = ApiClient.BASE_URL + "users/delete/address/" + mId + "/";
                    mDeleteAddress(mUrl, position);


                    return true;
                default:
            }
            return false;
        }


    }

}
