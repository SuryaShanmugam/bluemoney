package in.billiontags.bluemoney;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.billiontags.bluemoney.adapter.BusSearchDetailAdapter;
import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.RechargesService;
import in.billiontags.bluemoney.app.Constant;
import in.billiontags.bluemoney.callback.BusSearchDetailCallback;
import in.billiontags.bluemoney.model.BusOperatorsList;
import in.billiontags.bluemoney.model.BusSearchResults;
import in.billiontags.bluemoney.model.BusTravelsSearch;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.app.Constant.KEY_NOOFTICKETS;
import static in.billiontags.bluemoney.utils.Activity.launchWithBundle;
import static in.billiontags.bluemoney.utils.Utils.getParsedDate;


public class BusSearchDetailActivity extends AppCompatActivity implements View.OnClickListener, BusSearchDetailCallback {


    private static final String KEY_SCHEDULECODE = "schedulecode";
    private static final String KEY_OPERATORCODE = "operatorcode";
    private static final String KEY_SOURCEPLACE = "source";
    private static final String KEY_DESTINATIONPLACE = "destination";
    private static final String KEY_TRAVELDATE = "traveldate";
    private static final String KEY_TRAVELSNAME = "travelername";
    private static final String KEY_BUSTYPE = "bustype";
    private static final String KEY_SOURCENAME = "sourcename";
    private static final String KEY_DESTINATIONNAME = "destinationname";
    private static final String KEY_CANCELLATIONTERMS = "cancellationterms";
    private static final String KEY_BUSFACILITY = "busfacility";
    private static final String KEY_BUSTYPEMODEL = "bustypemodel";
    private static final String KEY_PARTIALCANCELLATIONS = "partialcancellations";
    String mTravelDate, mSourceNameCode, mDestinationNameCode, mSourceName, mDestinationName, mCancelationsTerms;
    ArrayList<String> fare;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<BusSearchResults> mBusSearchResults;
    private BusSearchDetailAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ProgressDialog mProgressDialog;
    private TextView mTextViewSource, mTexViewDestination, mTextViewDate;
    private String mSourcePlaceCode, mDestinationPlaceCode, mTravelingDate;
    private int mNoOfTickets;
    private ArrayList<String> mCancellationTerms;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bussearch_detail);
        processBundle();
        initObjects();
        initCallbacks();
        initRecyclerView();

        showProgressDialog("Loading.....");

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }

    private void initObjects() {
        mContext = this;
        mRecyclerView = findViewById(R.id.recyclerview);
        mTextViewSource = findViewById(R.id.txt_source);
        mTexViewDestination = findViewById(R.id.txt_destination);
        mTextViewDate = findViewById(R.id.txt_date);
        mImageBack = findViewById(R.id.img_back);
        mProgressDialog = new ProgressDialog(mContext);
        mLayoutManager = new LinearLayoutManager(mContext);
        mBusSearchResults = new ArrayList<>();
        mCancellationTerms = new ArrayList<>();
        mAdapter = new BusSearchDetailAdapter(mContext, mBusSearchResults, this);
        mTextViewSource.setText(mSourceName);
        mTexViewDestination.setText(mDestinationName);
        mTextViewDate.setText(getParsedDate(mTravelDate));
    }

    @Override
    public void mOnItemClick(View view, int pos) {
        BusSearchResults busSearchResults = mBusSearchResults.get(pos);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_SCHEDULECODE, busSearchResults.getmScheduleCode());
        bundle.putString(KEY_OPERATORCODE, busSearchResults.getmOpertorCode());
        bundle.putString(KEY_TRAVELSNAME, busSearchResults.getmOperatorName());
        bundle.putString(KEY_BUSTYPE, busSearchResults.getmBustype());
        bundle.putString(KEY_SOURCEPLACE, mSourcePlaceCode);
        bundle.putString(KEY_DESTINATIONPLACE, mDestinationPlaceCode);
        bundle.putString(KEY_TRAVELDATE, mTravelingDate);
        bundle.putString(KEY_SOURCENAME, mSourceName);
        bundle.putString(KEY_DESTINATIONNAME, mDestinationName);
        bundle.putInt(KEY_NOOFTICKETS, mNoOfTickets);

        mCancellationTerms.clear();
        if (busSearchResults.getmCancellationTerms().isJsonArray()) {
            for (int i = 0; i < busSearchResults.getmCancellationTerms().getAsJsonArray().size(); i++) {
                mCancellationTerms.add(busSearchResults.getmCancellationTerms().getAsJsonArray().get(i).getAsString());
            }
        }
        bundle.putString(KEY_BUSFACILITY, busSearchResults.getmAcBus());
        bundle.putString(KEY_BUSTYPEMODEL, busSearchResults.getmBustype());
        bundle.putString(KEY_PARTIALCANCELLATIONS, busSearchResults.getmOperatorDetails().getmPartialCancelAllowed());
        bundle.putStringArrayList(KEY_CANCELLATIONTERMS, mCancellationTerms);
        launchWithBundle(mContext, BusSeatBookingctivity.class, bundle);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mTravelDate = bundle.getString(Constant.KEY_TRAVELDATE);
            mSourceNameCode = bundle.getString(Constant.KEY_SOURCEPLACE);
            mDestinationNameCode = bundle.getString(Constant.KEY_DESTINATIONPLACE);
            mSourceName = bundle.getString(Constant.KEY_SOURCENAME);
            mDestinationName = bundle.getString(Constant.KEY_DESTINATIONNAME);
            mNoOfTickets = bundle.getInt(KEY_NOOFTICKETS);
        }
        getListService(new BusTravelsSearch(mTravelDate, mSourceNameCode, mDestinationNameCode));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }
    }

    private void getListService(BusTravelsSearch busTravelsSearch) {

        RechargesService categoryService = ApiClient.getClient().create(RechargesService.class);
        Call<BusOperatorsList> call = categoryService.getBusSearchList(busTravelsSearch);
        call.enqueue(new retrofit2.Callback<BusOperatorsList>() {

            @Override
            public void onResponse(@NonNull Call<BusOperatorsList> call, @NonNull Response<BusOperatorsList> response) {
                BusOperatorsList busOperatorsList = response.body();
                if (response.isSuccessful() && busOperatorsList != null) {
                    mBusSearchResults.clear();
                    mBusSearchResults.addAll(busOperatorsList.getmSearchResults());
                    if (busOperatorsList.getmSearchResults().size() > 0) {
                        hideProgressDialog();
                        mSourcePlaceCode = busOperatorsList.getmFromStationcode();
                        mDestinationPlaceCode = busOperatorsList.getmToStationCode();
                        mTravelingDate = busOperatorsList.getmTravelDate();
                        mAdapter.notifyDataSetChanged();
                        for (int i = 0; i < busOperatorsList.getmSearchResults().size(); i++) {
                            busOperatorsList.getmSearchResults().get(i).setmTravelDate(busOperatorsList.getmTravelDate());
                        }
                    } else {
                        hideProgressDialog();
                        ToastBuilder.build(mContext, " No Buses Found");
                    }
                } else {
                    hideProgressDialog();
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BusOperatorsList> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}
