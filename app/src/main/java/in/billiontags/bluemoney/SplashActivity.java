package in.billiontags.bluemoney;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import in.billiontags.bluemoney.app.MyPreference;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launch;
import static in.billiontags.bluemoney.utils.Activity.launchClearStack;

public class SplashActivity extends AppCompatActivity {

    public Context mContext;
    MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = this;
        preference = new MyPreference(mContext);
        final Thread mythread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (Exception e) {

                } finally {
                    if (preference.getToken() != null) {
                        launchClearStack(mContext, HomeActivity.class);
                    } else {
                        launch(mContext, WelcomeActivity.class);
                    }
                }
            }
        };
        mythread.start();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onRestart() {
        super.onRestart();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
