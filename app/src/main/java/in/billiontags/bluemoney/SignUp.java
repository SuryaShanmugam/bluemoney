package in.billiontags.bluemoney;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import in.billiontags.bluemoney.api.ApiClient;
import in.billiontags.bluemoney.api.AuthService;
import in.billiontags.bluemoney.app.MyPreference;
import in.billiontags.bluemoney.model.User;
import in.billiontags.bluemoney.utils.ErrorHandler;
import in.billiontags.bluemoney.utils.ToastBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.billiontags.bluemoney.utils.Activity.launch;
import static in.billiontags.bluemoney.utils.Activity.launchClearStack;

public class SignUp extends AppCompatActivity implements View.OnClickListener, Runnable {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final int REQUEST_READ_SMS = 4;
    String phone;
    TextView redirect_login;
    TextInputLayout layout_signup_email;
    TextInputLayout layout_signup_mobile;
    TextInputLayout layout_signup_passwordconfirm;
    private Context mContext;
    private ImageView mImageViewBack;
    private EditText signup_email, signup_mobile, signup_passwordconfirm;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;
    private Button signUpbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initObjects();
        initCallbacks();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    @Override
    public void run() {

    }

    private void initObjects() {

        mImageViewBack = findViewById(R.id.img_back);
        signup_email = findViewById(R.id.signup_email);
        signup_mobile = findViewById(R.id.signup_mobile);
        redirect_login = findViewById(R.id.redirect_login);
        signup_passwordconfirm = findViewById(R.id.signup_passwordconfirm);
        layout_signup_email = findViewById(R.id.layout_signup_email);
        layout_signup_mobile = findViewById(R.id.layout_signup_mobile);
        layout_signup_passwordconfirm = findViewById(R.id.layout_signup_passwordconfirm);
        signUpbutton = findViewById(R.id.signUp);
        mContext = this;
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mImageViewBack.setOnClickListener(this);
        redirect_login.setOnClickListener(this);
        signUpbutton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == mImageViewBack) {
            onBackPressed();
        } else if (v == redirect_login) {
            launchClearStack(mContext, Login.class);

        } else if (v == signUpbutton) {
            processSignUp();
        }

    }

    private void processSignUp() {
        String email = signup_email.getText().toString().trim();
        phone = signup_mobile.getText().toString().trim();
        String pass = signup_passwordconfirm.getText().toString();

        if (validateInput(phone, pass)) {
            if (hasReadSmsPermission()) {
                mPreference.setPhone(phone);
                boolean type = false;
                String role = "3";
                showProgressDialog("Signing up..");
                registerUser(new User(phone, email, pass, type, role));

            } else {
                requestReadSmsPermission();
            }
        }
    }

    private boolean validateInput(String phone, String pass) {
        if (phone.isEmpty()) {
            signup_mobile.requestFocus();
            layout_signup_mobile.setError(String.format(Locale.getDefault(), getString(R.string.error_empty1),
                    "Mobile Number"));
            return false;
        } else if (phone.length() < 10) {
            signup_mobile.requestFocus();
            layout_signup_mobile.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "Phone Number", 10, "digits"));
            return false;
        } else if (pass.isEmpty()) {
            signup_passwordconfirm.requestFocus();
            layout_signup_passwordconfirm.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_empty1),
                            "Password"));
            return false;
        } else if (pass.length() < 6) {
            signup_passwordconfirm.requestFocus();
            layout_signup_passwordconfirm.setError(getString(R.string.error_pass_length));
            return false;
        } else {
            signup_mobile.setFocusable(false);
            signup_passwordconfirm.setFocusable(false);
        }
        return true;
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED;

    }

    private void requestReadSmsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_SMS}, REQUEST_READ_SMS);
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void registerUser(User user) {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<User> call = authService.registerUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                User userResponse = response.body();
                hideProgressDialog();
                if (response.isSuccessful() && userResponse != null) {
                    mPreference.setPhone(userResponse.getmUsername());
                    mPreference.setUserId(String.valueOf(userResponse.getmId()));
                    launch(mContext, ActivateAccountActivity.class);

                } else {
                    Log.e("error", "" + response.toString());
                    ErrorHandler.processError(mContext, response.code(), response.errorBody());
                }

            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                hideProgressDialog();
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    processSignUp();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}

